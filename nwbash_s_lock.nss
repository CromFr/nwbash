#include "nwbash_inc"



int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		NWBash_Info(
			  "Usage: lock <subcommand> [options]\n"
			+ "Subcommands:\n"
			+ "get     Print lock properties\n"
			+ "set     Change lock properties\n"
		);
		return 0;
	}
	else {
		string sSubcommand = ArrayGetString(args, 0);
		args = ArrayPopFront(args);
		nArgsLen--;

		if(sSubcommand == "get"){
			if(bNeedHelp){
				NWBash_Info(
					  "Usage: lock get [options]\n"
					+ "Print lock properties\n"
				);
				return 0;
			}

			object oTarget = NWBash_GetTarget();
			if(!GetIsObjectValid(oTarget)){
				NWBash_Err("Invalid target");
				return 1;
			}
			int nType = GetObjectType(oTarget);
			if(nType != OBJECT_TYPE_DOOR && nType != OBJECT_TYPE_PLACEABLE){
				NWBash_Err("Target is not a door or placeable");
				return 2;
			}

			NWBash_Info(NWBash_ObjectToUserString(oTarget)
				+ " locked=" + IntToString(GetLocked(oTarget))
				+ " unlockdc=" + IntToString(GetLockUnlockDC(oTarget))
				+ " lockable=" + IntToString(GetLockLockable(oTarget))
				+ " lockdc=" + IntToString(GetLockLockDC(oTarget))
				+ " keyreq=" + IntToString(GetLockKeyRequired(oTarget))
				+ " keytag='" + GetLockKeyTag(oTarget) + "'"
				+ " keymsg='" + GetKeyRequiredFeedbackMessage(oTarget) + "'"
			);
		}
		else if(sSubcommand == "set"){
			if(bNeedHelp){
				NWBash_Info(
					  "Usage: lock set <property>=<value>\n"
					+ "Change lock properties\n"
					+ "\n"
					+ "Properties:\n"
					+ "locked=<bool>    Locked with a lock that locks\n"
					+ "unlockdc=<int>   Unlock DC\n"
					+ "lockable=<bool>  Can be locked by players\n"
					+ "lockdc=<int>     Lock DC\n"
					+ "keyreq=<bool>    Key required to unlock\n"
					+ "keytag=<string>  Required key tag\n"
					+ "keymsg=<string>  Required key message"
				);
				return 0;
			}

			if(nArgsLen == 0){
				NWBash_Err("Bad number of arguments. See 'lock set --help'");
				return 10;
			}

			object oTarget = NWBash_GetTarget();
			if(!GetIsObjectValid(oTarget)){
				NWBash_Err("Invalid target");
				return 1;
			}
			int nType = GetObjectType(oTarget);
			if(nType != OBJECT_TYPE_DOOR && nType != OBJECT_TYPE_PLACEABLE){
				NWBash_Err("Target is not a door or placeable");
				return 2;
			}

			int i;
			for(i = 0 ; i < nArgsLen ; i++)
			{
				string sArg = ArrayGetString(args, i);
				int nEqPos = FindSubString(sArg, "=");
				string sProp = GetSubString(sArg, 0, nEqPos);
				string sValue = GetSubString(sArg, nEqPos + 1, -1);

				if(sProp == "locked"){
					int nValue = NWBash_StringToBool(sValue);
					SetLocked(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "unlockdc"){
					int nValue = StringToInt(sValue);
					SetLockUnlockDC(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "lockable"){
					int nValue = NWBash_StringToBool(sValue);
					SetLockLockable(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "lockdc"){
					int nValue = StringToInt(sValue);
					SetLockLockDC(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "keyreq"){
					int nValue = NWBash_StringToBool(sValue);
					SetLockKeyRequired(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "keytag"){
					SetLockKeyTag(oTarget, sValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + sValue);
				}
				else if(sProp == "keymsg"){
					SetKeyRequiredFeedbackMessage(oTarget, sValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + sValue);
				}
				else{
					NWBash_Err("Unknown property '" + sProp + "'. See lock set --help");
				}
			}

		}
	}

	return 0;
}
