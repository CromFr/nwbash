#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: feat <feat_id>\n"
			+ "feat_id must be an integer matching a feat ID in feats.2da\n"
			+ "To remove a feat, add a '-' before its ID (ex: -45)\n"
			+ "Multiple feat_id can be provided.\n"
			+ "Options can be:\n"
			+ "-h, --help  show this help"
		);
		return 0;
	}
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));

	object oTarget = NWBash_GetTarget();

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		NWBash_Err("Bad number of arguments. See 'feat --help'");
		return 1;
	}

	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 3;
	}
	if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE){
		NWBash_Err("Target is not a creature");
		return 4;
	}

	int i;
	for(i = 0 ; i < nArgsLen ; i++)
	{
		string sFeat = ArrayGetString(args, i);
		int bRemove = GetStringLeft(sFeat, 1) == "-";
		int nFeat = abs(StringToInt(sFeat));
		if(bRemove){
			FeatRemove(oTarget, nFeat);
			NWBash_Info("Removed feat " + GetStringByStrRef(StringToInt(Get2DAString("feat", "FEAT", nFeat))) + " (ID=" + IntToString(nFeat) + ") from " + NWBash_ObjectToUserString(oTarget));
		}
		else{
			FeatAdd(oTarget, nFeat, FALSE, TRUE);
			NWBash_Info("Given feat " + GetStringByStrRef(StringToInt(Get2DAString("feat", "FEAT", nFeat))) + " (ID=" + IntToString(nFeat) + ") to " + NWBash_ObjectToUserString(oTarget));
		}
	}

	return 0;
}
