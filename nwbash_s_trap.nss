#include "nwbash_inc"


int StringToFaction(string sFaction){
	sFaction = GetStringLowerCase(sFaction);
	if     (sFaction == "hostile")        return STANDARD_FACTION_HOSTILE;
	else if(sFaction == "commoner")       return STANDARD_FACTION_COMMONER;
	else if(sFaction == "merchant")       return STANDARD_FACTION_MERCHANT;
	else if(sFaction == "defender")       return STANDARD_FACTION_DEFENDER;
	return -1;
}

int StringToTrapType(string sType){
	if(NWBash_IsNumeric(sType)){
		return StringToInt(sType);
	} else{
		sType = GetStringLowerCase(sType);

		int i;
		for(i = 0 ; i < GetNum2DARows("traps.2da") ; i++)
		{
			if(sType == GetStringLowerCase(Get2DAString("traps.2da", "Label", i))){
				return i;
			}
		}
		return -1;
	}
}


int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		NWBash_Info(
			  "Usage: trap <subcommand> [options]\n"
			+ "Subcommands:\n"
			+ "get     Print trap properties\n"
			+ "set     Change trap properties\n"
			+ "create  Create a new trap on object or location"
		);
		return 0;
	}
	else {
		string sSubcommand = ArrayGetString(args, 0);
		args = ArrayPopFront(args);
		nArgsLen--;

		object oTarget = NWBash_GetTarget();
		if(sSubcommand == "get" || sSubcommand == "set"){
			if(!GetIsObjectValid(oTarget)){
				NWBash_Err("Invalid target");
				return 1;
			}
			int nType = GetObjectType(oTarget);
			if(nType != OBJECT_TYPE_DOOR && nType != OBJECT_TYPE_PLACEABLE && nType != OBJECT_TYPE_TRIGGER){
				NWBash_Err("Target is not a door, placeable or trigger");
				return 2;
			}
		}

		if(sSubcommand == "get"){
			if(bNeedHelp){
				NWBash_Info(
					  "Usage: trap get [options]\n"
					+ "Print trap properties\n"
				);
				return 0;
			}
			if(nArgsLen != 0){
				NWBash_Err("Bad number of arguments. See 'trap get --help'");
				return 10;
			}
			NWBash_Info(NWBash_ObjectToUserString(oTarget)
				+ " active=" + IntToString(GetTrapActive(oTarget))
				+ " oneshot=" + IntToString(GetTrapOneShot(oTarget))
				+ " detectable=" + IntToString(GetTrapDetectable(oTarget))
				+ " detectdc=" + IntToString(GetTrapDetectDC(oTarget))
				+ " disarmable=" + IntToString(GetTrapDisarmable(oTarget))
				+ " disarmdc=" + IntToString(GetTrapDisarmDC(oTarget))
				+ " recoverable=" + IntToString(GetTrapRecoverable(oTarget))
				+ " keytag='" + GetTrapKeyTag(oTarget) + "'"
				+ " visible=" + IntToString(GetTrapFlagged(oTarget))
				+ " basetype=" + IntToString(GetTrapBaseType(oTarget))
				+ " creator=" + NWBash_ObjectToUserString(GetTrapCreator(oTarget))
			);
		}
		else if(sSubcommand == "set"){
			if(bNeedHelp){
				NWBash_Info(
					  "Usage: trap set [options] <property>=<value> [...]\n"
					+ "Change trap properties\n"
					+ "\n"
					+ "Available properties:\n"
					+ "active=<bool>        Trap enabled\n"
					+ "oneshot=<bool>       Destroyed after triggered\n"
					+ "detectable=<bool>    Can be detected\n"
					+ "detectdc=<int>       Detection DC\n"
					+ "disarmable=<bool>    Can be disarmed\n"
					+ "disarmdc=<int>       Disarm DC\n"
					+ "recoverable=<bool>   Can be recovered / picked up\n"
					+ "detectedby=<object>  Make trap visible by object\n"
					+ "keytag=<string>      Require a key to disarm"
				);
				return 0;
			}
			if(nArgsLen == 0){
				NWBash_Err("Bad number of arguments. See 'trap set --help'");
				return 10;
			}
			int i;
			for(i = 0 ; i < nArgsLen ; i++)
			{
				string sArg = ArrayGetString(args, i);
				int nEqPos = FindSubString(sArg, "=");
				string sProp = GetSubString(sArg, 0, nEqPos);
				string sValue = GetSubString(sArg, nEqPos + 1, -1);

				if(sProp == "active"){
					int nValue = NWBash_StringToBool(sValue);
					SetTrapActive(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "oneshot"){
					int nValue = NWBash_StringToBool(sValue);
					SetTrapOneShot(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "detectable"){
					int nValue = NWBash_StringToBool(sValue);
					SetTrapDetectable(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "detectdc"){
					int nValue = StringToInt(sValue);
					SetTrapDetectDC(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "disarmable"){
					int nValue = NWBash_StringToBool(sValue);
					SetTrapDisarmable(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "disarmdc"){
					int nValue = StringToInt(sValue);
					SetTrapDisarmDC(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "recoverable"){
					int nValue = NWBash_StringToBool(sValue);
					SetTrapRecoverable(oTarget, nValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + IntToString(nValue));
				}
				else if(sProp == "detectedby"){
					object oValue = NWBash_ResolveObjectString(sValue);
					SetTrapDetectedBy(oTarget, oValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + NWBash_ObjectToUserString(oValue));
				}
				else if(sProp == "keytag"){
					SetTrapKeyTag(oTarget, sValue);
					NWBash_Info(NWBash_ObjectToUserString(oTarget) + ": set " + sProp + "=" + sValue);
				}
				else{
					NWBash_Err("Unknown property '" + sProp + "'. See trap set --help");
				}
			}
		}
		else if(sSubcommand == "create"){

			if(bNeedHelp){
				NWBash_Info(
					  "Usage: trap create [options] <trap_type>\n"
					+ "Create a new trap on object or at location\n"
					+ "\n"
					+ "Arguments:\n"
					+ "trap_type  Trap type ID or label, as defined in traps.2da"
					+ "\n"
					+ "Options:\n"
					+ "-t --trigger=SCR  Trap on triggered script\n"
					+ "-d --disarm=SCR   Trap on disarmed script\n"
					+ "-f --faction=FAC  Trap faction. Defaults to hostile. Available values: hostile commoner merchant defender\n"
					+ "-l --location     Create trap at the target location\n"
					+ "--loc-size=SIZE   Trap size when created at location. Defaults to 2.0\n"
					+ "--loc-tag=TAG     Trap tag when created at location\n"
					+ "--loc-export=VAR  Export trap object to env var when created at location"
				);
				return 0;
			}
			int bAtLocation = NWBash_GetBooleanArg(args, "l|location");
			string sOnTriggered = NWBash_GetStringArg(args, "trigger");
			string sOnDisarmed = NWBash_GetStringArg(args, "disarm");
			string sFaction = NWBash_GetStringArg(args, "f|faction");
			string sLocSize = NWBash_GetStringArg(args, "loc-size");
			string sLocTag = NWBash_GetStringArg(args, "loc-tag");
			string sLocExport = NWBash_GetStringArg(args, "loc-export");
			args = NWBash_RemoveBooleanArgs(args, Array("|l|location|"));
			args = NWBash_RemoveStringArgs(args, Array("|disarm|trigger|faction|loc-size|loc-tag|loc-export|"));
			if(ArrayGetLength(args) != 1){
				NWBash_Err("Bad number of arguments. See 'trap create --help'");
				return 11;
			}
			string sTrapType = ArrayGetString(args, 0);
			int nTrapType = StringToTrapType(sTrapType);
			if(nTrapType < 0){
				NWBash_Err("Invalid trap type '" + sTrapType + "'. See trap create --help");
				return 12;
			}
			string sTrapTypeLabel = Get2DAString("traps.2da", "Label", nTrapType);

			if(!GetIsObjectValid(oTarget)){
				NWBash_Err("Invalid target");
				return 13;
			}

			int nFaction = STANDARD_FACTION_HOSTILE;
			if(sFaction != ""){
				nFaction = StringToFaction(sFaction);
				if(nFaction < 0){
					NWBash_Err("Unknown faction '" + sFaction + "'. See trap create --help");
					return 14;
				}
			}

			float fLocSize = 2.0;
			if(sLocSize != ""){
				if(!NWBash_IsNumeric(sLocSize, TRUE)){
					NWBash_Err("Invalid --loc-size float value '" + sLocSize + "'");
					return 15;
				}
				fLocSize = StringToFloat(sLocSize);
			}

			if(bAtLocation) {
				object oTrap = CreateTrapAtLocation(nTrapType, GetLocation(oTarget), fLocSize, sLocTag, nFaction, sOnDisarmed, sOnTriggered);
				if(sLocExport != ""){
					NWBash_SetEnvVar(sLocExport, NWBash_ObjectToString(oTrap));
				}
				NWBash_Info("Created new " + sTrapTypeLabel + " trap " + NWBash_ObjectToUserString(oTrap) + " at location " + NWBash_LocationToString(GetLocation(oTrap)));
			}
			else {
				int nType = GetObjectType(oTarget);
				if(nType != OBJECT_TYPE_DOOR && nType != OBJECT_TYPE_PLACEABLE && nType != OBJECT_TYPE_TRIGGER){
					NWBash_Err("Target is not a door, placeable or trigger");
					return 16;
				}

				CreateTrapOnObject(nTrapType, oTarget, nFaction, sOnDisarmed, sOnTriggered);
				NWBash_Info("Created new " + sTrapTypeLabel + " trap on object " + NWBash_ObjectToUserString(oTarget));

			}
		}
		else{
			NWBash_Err("Unknown subcommand '" + sSubcommand + "'. See trap --help");
			return 3;
		}
	}

	return 0;
}
