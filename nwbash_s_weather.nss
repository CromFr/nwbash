#include "nwbash_inc"


int StringToWeatherType(string sWeatherType){
	sWeatherType = GetStringLowerCase(sWeatherType);
	if(sWeatherType == "rain")
		return WEATHER_TYPE_RAIN;
	else if(sWeatherType == "snow")
		return WEATHER_TYPE_SNOW;
	else if(sWeatherType == "lightning")
		return WEATHER_TYPE_LIGHTNING;
	else
		return WEATHER_TYPE_INVALID;
}
string WeatherTypeToString(int nWeatherType){
	switch(nWeatherType){
		case WEATHER_TYPE_RAIN: return "rain";
		case WEATHER_TYPE_SNOW: return "snow";
		case WEATHER_TYPE_LIGHTNING: return "lightning";
	}
	return "";
}


int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			  "Get or set weather conditions on the target area\n"
			+ "\n"
			+ "Usage: weather [options] <weather_type> [power]\n"
			+ "\n"
			+ "Arguments\n"
			+ "weather_type  Either 'rain', 'snow', 'lightning'\n"
			+ "power         Value between 0 and 5. -1 to reset to area default\n"
			+ "Options:\n"
			+ "-h, --help        Show this help\n"
		);
		return 0;
	}

	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));

	object oTarget = GetArea(NWBash_GetTarget());
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 3;
	}

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen < 1 || nArgsLen > 2){
		NWBash_Err("Bad number of arguments. See 'weather --help'");
		return 1;
	}

	if(nArgsLen == 1){
		string sWeatherType = ArrayGetString(args, 0);
		int nWeatherType = StringToWeatherType(sWeatherType);

		if(nWeatherType < 0){
			NWBash_Err("Invalid weather type: '" + sWeatherType + "'");
			return 2;
		}

		int nPower = GetWeather(oTarget, nWeatherType);
		NWBash_Info("Weather " + WeatherTypeToString(nWeatherType) + " power is " + IntToString(nPower) + " for area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(nArgsLen == 2){
		string sWeatherType = ArrayGetString(args, 0);
		int nWeatherType = StringToWeatherType(sWeatherType);
		if(nWeatherType < 0){
			NWBash_Err("Invalid weather type: '" + sWeatherType + "'");
			return 2;
		}
		int nPower = ArrayGetInt(args, 1);

		SetWeather(oTarget, nWeatherType, nPower);
		NWBash_Info("Set weather " + WeatherTypeToString(nWeatherType) + " power to " + IntToString(nPower) + " for area " + NWBash_ObjectToUserString(oTarget));
	}
	return 0;
}
