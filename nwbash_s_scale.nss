#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			  "Usage: scale [options] <scale_x> <scale_y> <scale_z>\n"
			+ "       scale [options]\n"
			+ "The first form allows to set the object scale, the second only prints the object scale.\n"
			+ "scale_* parameters can be '-' to keep the current scale on a specific axis\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help        Show this help\n"
			+ "-d, --dur         Duration of the scale in seconds. When provided the scale is set using EffectSetScale, instead of SetScale\n"
		);
		return 0;
	}

	string sDur = NWBash_GetStringArg(args, "d|dur");

	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));
	args = NWBash_RemoveStringArgs(args, Array("|d|dur|"));

	object oTarget = NWBash_GetTarget();

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen != 0 && nArgsLen != 3){
		NWBash_Err("Bad number of arguments. See 'scale --help'");
		return 1;
	}

	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 3;
	}

	vector vCurrScale = Vector(
		GetScale(oTarget, SCALE_X),
		GetScale(oTarget, SCALE_Y),
		GetScale(oTarget, SCALE_Z)
	);

	if(nArgsLen == 0){
		NWBash_Info("Scale of " + NWBash_ObjectToUserString(oTarget) + " is " + VectorToString(vCurrScale));
	}
	else{
		string sScaleX = ArrayGetString(args, 0);
		string sScaleY = ArrayGetString(args, 1);
		string sScaleZ = ArrayGetString(args, 2);

		vector vTargetScale = Vector(
			sScaleX == "-" ? vCurrScale.x : StringToFloat(sScaleX),
			sScaleY == "-" ? vCurrScale.y : StringToFloat(sScaleY),
			sScaleZ == "-" ? vCurrScale.z : StringToFloat(sScaleZ)
		);

		if(sDur != "")
			ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectSetScale(vTargetScale.x, vTargetScale.y, vTargetScale.z), oTarget, StringToFloat(sDur));
		else
			SetScale(oTarget, vTargetScale.x, vTargetScale.y, vTargetScale.z);
		NWBash_Info("Set scale of " + NWBash_ObjectToUserString(oTarget) + " from " + VectorToString(vCurrScale) + " to " + VectorToString(vTargetScale));
	}

	return 0;
}
