#include "nwbash_inc"

void Move(object oObj, location lDest, int nMode){
	switch(nMode){
		case 0:
			AssignCommand(oObj, JumpToLocation(lDest));
			NWBash_Info("Jump " + NWBash_ObjectToUserString(oObj) + " to " + GetName(GetAreaFromLocation(lDest)));
			break;
		case 1:
			AssignCommand(oObj, ActionMoveToLocation(lDest, FALSE));
			NWBash_Info("Walk " + NWBash_ObjectToUserString(oObj) + " to " + GetName(GetAreaFromLocation(lDest)));
			break;
		case 2:
			AssignCommand(oObj, ActionMoveToLocation(lDest, TRUE));
			NWBash_Info("Run " + NWBash_ObjectToUserString(oObj) + " to " + GetName(GetAreaFromLocation(lDest)));
			break;
	}
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: move [options] <dest_loc_spec>\n"
			+ "dest_loc_spec: Destination location value specification (see !help values)\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-r, --radius RANGE  Move all creatures around target\n"
			+ "-p, --players       Move only players, when used with --radius\n"
			+ "--self              Move yourself too\n"
			+ "-h, --help          show this help"
		);
		return 0;
	}
	int bOnlyPlayers = NWBash_GetBooleanArg(args, "p|players");
	int bMoveSelf = NWBash_GetBooleanArg(args, "self");
	int bRun = NWBash_GetBooleanArg(args, "run");
	int bWalk = NWBash_GetBooleanArg(args, "walk");
	string sRadius = NWBash_GetStringArg(args, "r|radius");
	args = NWBash_RemoveBooleanArgs(args, Array("|help|p|players|self|"));
	args = NWBash_RemoveStringArgs(args, Array("|r|radius|"));

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen != 1){
		NWBash_Err("Bad number of arguments. See 'move --help'");
		return 1;
	}

	location lDest = NWBash_ResolveLocationString(ArrayGetString(args, 0));
	if(!GetIsObjectValid(GetAreaFromLocation(lDest))){
		NWBash_Err("Cannot find destination");
		return 2;
	}
	if(bRun && bWalk){
		NWBash_Err("Cannot use both --run and --walk");
		return 3;
	}

	int nMode = 0;
	if(bWalk)
		nMode = 1;
	else if(bRun)
		nMode = 2;

	object oTarget = NWBash_GetTarget();
	if(sRadius != ""){
		float fRadius = StringToFloat(sRadius);
		location lTarget = GetLocation(oTarget);

		object oNear = GetFirstObjectInShape(SHAPE_SPHERE, fRadius, lTarget);
		while(GetIsObjectValid(oNear)){

			if((oNear == OBJECT_SELF && bMoveSelf) || (bOnlyPlayers ? GetIsPC(oNear) : TRUE)){
				Move(oNear, lDest, nMode);
			}
			oNear = GetNextObjectInShape(SHAPE_SPHERE, fRadius, lTarget);
		}
	}
	else{
		if(!GetIsObjectValid(oTarget)){
			NWBash_Err("Invalid target");
			return 4;
		}

		if(bOnlyPlayers && !GetIsPC(oTarget)){
			NWBash_Err("Target is not a player, and --player was used");
			return 5;
		}

		Move(oTarget, lDest, nMode);

		if(bMoveSelf && oTarget != OBJECT_SELF){
			Move(OBJECT_SELF, lDest, nMode);
		}
	}
	return 0;
}


