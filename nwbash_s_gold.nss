#include "nwbash_inc"

void GiveGold(object oTarget, int nGold){
	if(nGold < 0)
		TakeGoldFromCreature(-nGold, oTarget, TRUE);
	else
		GiveGoldToCreature(oTarget, nGold);
	NWBash_Info("Gave " + IntToString(nGold) + " gp to " + GetName(oTarget));
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: gold [options] [amount]\n"
			+ "Give gold or print the current gold owned by a creature\n"
			+ "If no amount is given, prints how much gold the creature has\n"
			+ "Options can be:\n"
			+ "-a, --area  Act on all players in this area\n"
			+ "-h, --help  show this help"
		);
		return 0;
	}
	int bArea = NWBash_GetBooleanArg(args, "a|area");
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|a|area|"));

	object oTarget = NWBash_GetTarget();

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen != 1){
		NWBash_Err("Bad number of arguments. See 'gold --help'");
		return 1;
	}

	int nGold = ArrayGetInt(args, 0);


	if(bArea){
		object oPC = GetFirstPC();
		while(GetIsObjectValid(oPC))
		{
			if(GetArea(oPC) == GetArea(oTarget)){
				if(nArgsLen == 0)
					NWBash_Info(NWBash_ObjectToUserString(oPC) + " has " + IntToString(GetGold(oPC)) + " gp");
				else
					GiveGold(oPC, nGold);
			}
			oPC = GetNextPC();
		}
	}
	else{
		if(!GetIsObjectValid(oTarget)){
			NWBash_Err("Invalid target");
			return 3;
		}
		if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE){
			NWBash_Err("Target is not a creature");
			return 4;
		}

		if(nArgsLen == 0)
			NWBash_Info(NWBash_ObjectToUserString(oTarget) + " has " + IntToString(GetGold(oTarget)) + " gp");
		else
			GiveGold(oTarget, nGold);
	}
	return 0;
}