#include "nwbash_inc"

int StringToObjectType(string sObjectType){
	sObjectType = GetStringUpperCase(sObjectType);
	if     (sObjectType == "CREATURE")       return OBJECT_TYPE_CREATURE;
	else if(sObjectType == "ITEM")           return OBJECT_TYPE_ITEM;
	else if(sObjectType == "TRIGGER")        return OBJECT_TYPE_TRIGGER;
	else if(sObjectType == "DOOR")           return OBJECT_TYPE_DOOR;
	else if(sObjectType == "AREA_OF_EFFECT") return OBJECT_TYPE_AREA_OF_EFFECT;
	else if(sObjectType == "WAYPOINT")       return OBJECT_TYPE_WAYPOINT;
	else if(sObjectType == "PLACEABLE")      return OBJECT_TYPE_PLACEABLE;
	else if(sObjectType == "STORE")          return OBJECT_TYPE_STORE;
	else if(sObjectType == "ENCOUNTER")      return OBJECT_TYPE_ENCOUNTER;
	else if(sObjectType == "LIGHT")          return OBJECT_TYPE_LIGHT;
	else if(sObjectType == "PLACED_EFFECT")  return OBJECT_TYPE_PLACED_EFFECT;
	else if(sObjectType == "ALL")            return OBJECT_TYPE_ALL;
	else if(sObjectType == "INVALID")        return OBJECT_TYPE_INVALID;
	return -1;
}
int StringToFaction(string sFaction){
	sFaction = GetStringUpperCase(sFaction);
	if     (sFaction == "HOSTILE")        return STANDARD_FACTION_HOSTILE;
	else if(sFaction == "COMMONER")       return STANDARD_FACTION_COMMONER;
	else if(sFaction == "MERCHANT")       return STANDARD_FACTION_MERCHANT;
	else if(sFaction == "DEFENDER")       return STANDARD_FACTION_DEFENDER;
	return -1;
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: create [options] <resref>\n"
			+ "Options can be:\n"
			+ "-t, --type TYPE     Object type. Available types are: creature, item, trigger, door, area_of_effect, waypoint, placeable, store, encounter, light, placed_effect\n"
			+ "--faction FAC       set faction. Available factions are: hostile, commoner, merchant, defender\n"
			+ "--stack NUMBER      Only valid for stackable items\n"
			+ "--tag TAG           Set object's tag\n"
			+ "-l, --location LOCSPEC  Create the object at the given location value spec\n"
			+ "-e, --export VAR    Save the created object reference into an environment variable\n"
			+ "-h, --help          show this help"
		);
		return 1;
	}
	string sType = NWBash_GetStringArg(args, "t|type");
	string sFaction = NWBash_GetStringArg(args, "faction");
	string sTag = NWBash_GetStringArg(args, "tag");
	string sStack = NWBash_GetStringArg(args, "stack");
	string sExport = NWBash_GetStringArg(args, "e|export");
	string sLoc = NWBash_GetStringArg(args, "l|location");
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));
	args = NWBash_RemoveStringArgs(args, Array("|t|type|faction|tag|stack|e|export|l|location|"));

	int nArgsLen = ArrayGetLength(args);

	string sResref = ArrayGetString(args, 0);

	int nType = OBJECT_TYPE_CREATURE;
	if(sType != ""){
		nType = StringToObjectType(sType);
		if(nType < 0){
			NWBash_Err("Unknown object type '" + sType + "'");
			return 2;
		}
	}

	object oTarget = NWBash_GetTarget();
	location lTarget;

	if(sLoc != "")
		lTarget = NWBash_ResolveLocationString(sLoc);
	else
		lTarget = GetLocation(oTarget);

	int nFaction;
	if(sFaction != ""){
		nFaction = StringToFaction(sFaction);
		if(nFaction < 0){
			NWBash_Err("Unknown faction '" + sFaction + "'");
			return 3;
		}
	}

	object oObj;
	if(sLoc == "" && nType == OBJECT_TYPE_ITEM){
		int nStack;
		if(sStack == "")
			nStack = 1;
		else
			nStack = StringToInt(sStack);
		if(nStack <= 0){
			NWBash_Err("Invalid stack number");
			return 4;
		}
		oObj = CreateItemOnObject(sResref, oTarget, nStack, sTag);
	}
	else
		oObj = CreateObject(nType, sResref, lTarget, FALSE, sTag);

	if(!GetIsObjectValid(oObj)){
		NWBash_Err("Object could not be created (bad resref?)");
		return 5;
	}

	if(sExport != ""){
		NWBash_SetEnvVar(sExport, NWBash_ObjectToString(oObj));
	}

	if(sFaction != ""){
		if(nType == OBJECT_TYPE_CREATURE)
			ChangeToStandardFaction(oObj, nFaction);
		else
			NWBash_Warn("Warning: --faction only works with creatures");
	}

	NWBash_Info("Created object " + NWBash_ObjectToUserString(oObj));
	return 0;
}
