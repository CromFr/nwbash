#include "nwbash_inc"

int GetSkillID(string sSkill){
	if(NWBash_IsNumeric(sSkill)){
		return StringToInt(sSkill);
	} else{
		sSkill = GetStringLowerCase(sSkill);

		int i;
		for(i = 0 ; i < GetNum2DARows("skills.2da") ; i++)
		{
			if(sSkill == GetStringLowerCase(Get2DAString("skills.2da", "Label", i))){
				return i;
			}
		}
		return -1;
	}
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: skill [options] <skill> [...]\n"
			+"       skill [options] <skill>=<rank> [...]\n"
			+ "The first form prints skill rank values, while the second change skill rank values.\n"
			+ "Multiple skill can be provided, in either forms.\n"
			+ "\n"
			+ "Arguments:\n"
			+ "skill  Skill ID or name as defined by the Label column in skills.2da\n"
			+ "rank   Base rank value. Start with + or - to modify current values\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help  show this help"
			+ "--track     Bind the skill change to the character level"
		);
		return 0;
	}
	int bTrack = NWBash_GetBooleanArg(args, "track");
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|track|"));

	object oTarget = NWBash_GetTarget();

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		NWBash_Err("Bad number of arguments. See 'skill --help'");
		return 1;
	}

	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 3;
	}
	if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE){
		NWBash_Err("Target is not a creature");
		return 4;
	}

	int i;
	for(i = 0 ; i < nArgsLen ; i++)
	{
		string sArg = ArrayGetString(args, i);
		int nEqPos = FindSubString(sArg, "=");
		string sSkill = GetSubString(sArg, 0, nEqPos);

		int nSkill = GetSkillID(sSkill);
		if(nSkill < 0){
			NWBash_Err("Invalid skill '" + sSkill + "'");
			continue;
		}

		string sSkillName = Get2DAString("skills.2da", "Label", nSkill);

		if(nEqPos < 0){
			// Print skill info
			int nBaseRank = GetSkillRank(nSkill, oTarget, TRUE);
			int nTotalRank = GetSkillRank(nSkill, oTarget, FALSE);
			NWBash_Info("Skill " + sSkillName + ": base=" + IntToString(nBaseRank) + " total=" + IntToString(nTotalRank) + " (on " + NWBash_ObjectToUserString(oTarget) + ")");
		}
		else{
			// Set the skill
			string sRank = GetSubString(sArg, nEqPos + 1, -1);
			string sMod = GetStringLeft(sRank, 1);
			if(sMod == "+" || sMod == "-"){
				sRank = GetSubString(sRank, 1, -1);
			} else{
				sMod = "";
			}

			if(!NWBash_IsNumeric(sRank)){
				NWBash_Err("Invalid skill rank value: '" + sRank + "'");
				continue;
			}
			int nRank = StringToInt(sRank);

			int nPrevRank = GetSkillRank(nSkill, oTarget, TRUE);

			if(sMod == "+")
				nRank = nPrevRank + nRank;
			else if(sMod == "-")
				nRank = nPrevRank - nRank;

			SetBaseSkillRank(oTarget, nSkill, nRank, bTrack);
			NWBash_Info("Skill " + sSkillName + ": Changed base from " + IntToString(nPrevRank) + " to " + IntToString(nRank) + " (on " + NWBash_ObjectToUserString(oTarget) + ")");
		}
	}

	return 0;
}
