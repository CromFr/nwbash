#include "nwbash_inc"

void ResetAnim(object oTarget){
	PlayCustomAnimation(oTarget, "%", FALSE);
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: vfx [options] <sef_name>\n"
			+ "Plays a SEF visual effect on the target or at a given location\n"
			+ "Options can be:\n"
			+ "-l, --location=LOCSPEC  Location where to play the VFX. Played on the target object if not provided\n"
			+ "-d, --duration=DUR      -1 for permanent, 0 or absent for instant, > 0 for temporary\n"
			+ "-h, --help              Show this help\n"
		);
		return 0;
	}
	float fDur = StringToFloat(NWBash_GetStringArg(args, "d|duration"));
	string sLocSpec = NWBash_GetStringArg(args, "l|location");
	string sBeamTarget = NWBash_GetStringArg(args, "beam-target");
	args = NWBash_RemoveStringArgs(args, Array("|d|duration|l|location|beam-target|"));
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));


	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen > 1){
		NWBash_Err("Bad number of arguments. See 'vfx --help'");
		return 1;
	}

	string sSEF = ArrayGetString(args, 0);

	object oBeamTarget;
	if(sBeamTarget != ""){
		oBeamTarget = NWBash_ResolveObjectString(sBeamTarget);
		if(!GetIsObjectValid(oBeamTarget)){
			NWBash_Err("Invalid beam target");
			return 2;
		}
	}

	int nDurType;
	if(fDur < 0.0)
		nDurType = DURATION_TYPE_PERMANENT;
	else if(fDur > 0.0)
		nDurType = DURATION_TYPE_TEMPORARY;
	else
		nDurType = DURATION_TYPE_INSTANT;

	if(sLocSpec != ""){
		string sTarget = ArrayGetString(args, 1);
		location lTarget = NWBash_ResolveLocationString(sTarget);
		if(!GetIsObjectValid(GetAreaFromLocation(lTarget))){
			NWBash_Err("Invalid loc_spec / location not found");
			return 3;
		}

		ApplyEffectAtLocation(nDurType, EffectNWN2SpecialEffectFile(sSEF, oBeamTarget), lTarget, nDurType == DURATION_TYPE_TEMPORARY ? fDur : 0.0);
		NWBash_Info("Applied SEF " + sSEF + " at location " + NWBash_LocationToString(lTarget) + (nDurType == DURATION_TYPE_TEMPORARY ? " for " + FloatToString(fDur, 0, 2) + " seconds." : ""));
	}
	else{
		object oTarget = NWBash_GetTarget();
		if(!GetIsObjectValid(oTarget)){
			NWBash_Err("Invalid target");
			return 4;
		}

		ApplyEffectToObject(nDurType, EffectNWN2SpecialEffectFile(sSEF, oBeamTarget), oTarget, nDurType == DURATION_TYPE_TEMPORARY ? fDur : 0.0);
		NWBash_Info("Applied SEF " + sSEF + " on object " + NWBash_ObjectToUserString(oTarget) + (nDurType == DURATION_TYPE_TEMPORARY ? " for " + FloatToString(fDur, 0, 2) + " seconds." : ""));
	}
	return 0;
}