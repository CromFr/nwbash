#include "nwbash_inc"

void ResetAnim(object oTarget){
	PlayCustomAnimation(oTarget, "%", FALSE);
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: anim [options] <animation_name>\n"
			+ "Play a custom animation with its name\n"
			+ "Options can be:\n"
			+ "-l, --loop          Loop animation\n"
			+ "-d, --duration=DUR  Reset to idle animation after this duration in seconds\n"
			+ "-h, --help          Show this help\n"
		);
		return 0;
	}
	int bLoop = NWBash_GetBooleanArg(args, "l|loop");
	string sDur = NWBash_GetStringArg(args, "d|duration");
	args = NWBash_RemoveStringArgs(args, Array("|d|duration|"));
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|l|loop|"));


	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen > 1){
		NWBash_Err("Bad number of arguments. See 'anim --help'");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 2;
	}

	string sAnim = ArrayGetString(args, 0);
	PlayCustomAnimation(oTarget, sAnim, bLoop);
	NWBash_Info("Playing animation " + sAnim + (bLoop ? " (looping)" : "") + " on object " + NWBash_ObjectToUserString(oTarget));

	if(sDur != ""){
		DelayCommand(StringToFloat(sDur), ResetAnim(oTarget));
	}
	return 0;
}