#include "nwbash_inc"


int StringToItemPropType(string sItemPropType){
	if(NWBash_IsNumeric(sItemPropType)){
		return StringToInt(sItemPropType);
	}
	else{
		sItemPropType = GetStringLowerCase(sItemPropType);
		int nRows = GetNum2DARows("itempropdef");
		int i;
		for(i = 0 ; i < nRows ; i++){
			if(GetStringLowerCase(Get2DAString("itempropdef", "Label", i)) == sItemPropType)
				return i;
		}
		return -1;
	}
}

// TODO: does not work with ITEM_PROPERTY_DAMAGE_REDUCTION
itemproperty BuildItemProperty(int nType, int nSubType = -1, int nCostValue = -1, int nParam1 = -1){
	switch(nType){
		case ITEM_PROPERTY_ABILITY_BONUS:                            return ItemPropertyAbilityBonus(nSubType, nCostValue);
		case ITEM_PROPERTY_AC_BONUS:                                 return ItemPropertyACBonus(nCostValue);
		case ITEM_PROPERTY_AC_BONUS_VS_ALIGNMENT_GROUP:              return ItemPropertyACBonusVsAlign(nSubType, nCostValue);
		case ITEM_PROPERTY_AC_BONUS_VS_DAMAGE_TYPE:                  return ItemPropertyACBonusVsDmgType(nSubType, nCostValue);
		case ITEM_PROPERTY_AC_BONUS_VS_RACIAL_GROUP:                 return ItemPropertyACBonusVsRace(nSubType, nCostValue);
		case ITEM_PROPERTY_AC_BONUS_VS_SPECIFIC_ALIGNMENT:           return ItemPropertyACBonusVsSAlign(nSubType, nCostValue);
		case ITEM_PROPERTY_ENHANCEMENT_BONUS:                        return ItemPropertyEnhancementBonus(nCostValue);
		case ITEM_PROPERTY_ENHANCEMENT_BONUS_VS_ALIGNMENT_GROUP:     return ItemPropertyEnhancementBonusVsAlign(nSubType, nCostValue);
		case ITEM_PROPERTY_ENHANCEMENT_BONUS_VS_RACIAL_GROUP:        return ItemPropertyEnhancementBonusVsRace(nSubType, nCostValue);
		case ITEM_PROPERTY_ENHANCEMENT_BONUS_VS_SPECIFIC_ALIGNEMENT: return ItemPropertyEnhancementBonusVsSAlign(nSubType, nCostValue);
		case ITEM_PROPERTY_DECREASED_ENHANCEMENT_MODIFIER:           return ItemPropertyEnhancementPenalty(nCostValue);
		case ITEM_PROPERTY_BASE_ITEM_WEIGHT_REDUCTION:               return ItemPropertyWeightReduction(nCostValue);
		case ITEM_PROPERTY_BONUS_FEAT:                               return ItemPropertyBonusFeat(nSubType);
		case ITEM_PROPERTY_BONUS_SPELL_SLOT_OF_LEVEL_N:              return ItemPropertyBonusLevelSpell(nSubType, nCostValue);
		// DELETED
		case ITEM_PROPERTY_CAST_SPELL:                               return ItemPropertyCastSpell(nSubType, nCostValue);
		case ITEM_PROPERTY_DAMAGE_BONUS:                             return ItemPropertyDamageBonus(nSubType, nCostValue);
		case ITEM_PROPERTY_DAMAGE_BONUS_VS_ALIGNMENT_GROUP:          return ItemPropertyDamageBonusVsAlign(nSubType, nParam1, nCostValue);
		case ITEM_PROPERTY_DAMAGE_BONUS_VS_RACIAL_GROUP:             return ItemPropertyDamageBonusVsRace(nSubType, nParam1, nCostValue);
		case ITEM_PROPERTY_DAMAGE_BONUS_VS_SPECIFIC_ALIGNMENT:       return ItemPropertyDamageBonusVsSAlign(nSubType, nParam1, nCostValue);
		case ITEM_PROPERTY_IMMUNITY_DAMAGE_TYPE:                     return ItemPropertyDamageImmunity(nSubType, nCostValue);
		case ITEM_PROPERTY_DECREASED_DAMAGE:                         return ItemPropertyDamagePenalty(nCostValue);
		// DELETED
		case ITEM_PROPERTY_DAMAGE_RESISTANCE:                        return ItemPropertyDamageResistance(nSubType, nCostValue);
		case ITEM_PROPERTY_DAMAGE_VULNERABILITY:                     return ItemPropertyDamageVulnerability(nSubType, nCostValue);
		// DELETED
		case ITEM_PROPERTY_DARKVISION:                               return ItemPropertyDarkvision();
		case ITEM_PROPERTY_DECREASED_ABILITY_SCORE:                  return ItemPropertyDecreaseAbility(nSubType, nCostValue);
		case ITEM_PROPERTY_DECREASED_AC:                             return ItemPropertyDecreaseAC(nSubType, nCostValue);
		case ITEM_PROPERTY_DECREASED_SKILL_MODIFIER:                 return ItemPropertyDecreaseSkill(nSubType, nCostValue);
		// DELETED
		// DELETED
		case ITEM_PROPERTY_ENHANCED_CONTAINER_REDUCED_WEIGHT:        return ItemPropertyContainerReducedWeight(nCostValue);
		case ITEM_PROPERTY_EXTRA_MELEE_DAMAGE_TYPE:                  return ItemPropertyExtraMeleeDamageType(nSubType);
		case ITEM_PROPERTY_EXTRA_RANGED_DAMAGE_TYPE:                 return ItemPropertyExtraRangeDamageType(nSubType);
		case ITEM_PROPERTY_HASTE:                                    return ItemPropertyHaste();
		case ITEM_PROPERTY_HOLY_AVENGER:                             return ItemPropertyHolyAvenger();
		case ITEM_PROPERTY_IMMUNITY_MISCELLANEOUS:                   return ItemPropertyImmunityMisc(nSubType);
		case ITEM_PROPERTY_IMPROVED_EVASION:                         return ItemPropertyImprovedEvasion();
		case ITEM_PROPERTY_SPELL_RESISTANCE:                         return ItemPropertyBonusSpellResistance(nCostValue);
		case ITEM_PROPERTY_SAVING_THROW_BONUS:                       return ItemPropertyBonusSavingThrowVsX(nSubType, nCostValue);
		case ITEM_PROPERTY_SAVING_THROW_BONUS_SPECIFIC:              return ItemPropertyBonusSavingThrow(nSubType, nCostValue);
		// DELETED
		case ITEM_PROPERTY_KEEN:                                     return ItemPropertyKeen();
		case ITEM_PROPERTY_LIGHT:                                    return ItemPropertyLight(nCostValue, nParam1);
		case ITEM_PROPERTY_MIGHTY:                                   return ItemPropertyMaxRangeStrengthMod(nCostValue);
		// DELETED
		case ITEM_PROPERTY_NO_DAMAGE:                                return ItemPropertyNoDamage();
		case ITEM_PROPERTY_ON_HIT_PROPERTIES:                        return ItemPropertyOnHitProps(nSubType, nCostValue, nParam1); // Not sure, needs testing
		case ITEM_PROPERTY_DECREASED_SAVING_THROWS:                  return ItemPropertyReducedSavingThrowVsX(nSubType, nCostValue);
		case ITEM_PROPERTY_DECREASED_SAVING_THROWS_SPECIFIC:         return ItemPropertyReducedSavingThrow(nSubType, nCostValue);
		case ITEM_PROPERTY_REGENERATION:                             return ItemPropertyRegeneration(nCostValue);
		case ITEM_PROPERTY_SKILL_BONUS:                              return ItemPropertySkillBonus(nSubType, nCostValue);
		case ITEM_PROPERTY_IMMUNITY_SPECIFIC_SPELL:                  return ItemPropertySpellImmunitySpecific(nCostValue);
		case ITEM_PROPERTY_IMMUNITY_SPELL_SCHOOL:                    return ItemPropertySpellImmunitySchool(nSubType);
		case ITEM_PROPERTY_THIEVES_TOOLS:                            return ItemPropertyThievesTools(nCostValue);
		case ITEM_PROPERTY_ATTACK_BONUS:                             return ItemPropertyAttackBonus(nCostValue);
		case ITEM_PROPERTY_ATTACK_BONUS_VS_ALIGNMENT_GROUP:          return ItemPropertyAttackBonusVsAlign(nSubType, nCostValue);
		case ITEM_PROPERTY_ATTACK_BONUS_VS_RACIAL_GROUP:             return ItemPropertyAttackBonusVsRace(nSubType, nCostValue);
		case ITEM_PROPERTY_ATTACK_BONUS_VS_SPECIFIC_ALIGNMENT:       return ItemPropertyAttackBonusVsSAlign(nSubType, nCostValue);
		case ITEM_PROPERTY_DECREASED_ATTACK_MODIFIER:                return ItemPropertyAttackPenalty(nCostValue);
		case ITEM_PROPERTY_UNLIMITED_AMMUNITION:                     return ItemPropertyUnlimitedAmmo(nCostValue); // Warning: There is a subtype value for arrow/bolt/bullet
		case ITEM_PROPERTY_USE_LIMITATION_ALIGNMENT_GROUP:           return ItemPropertyLimitUseByAlign(nSubType);
		case ITEM_PROPERTY_USE_LIMITATION_CLASS:                     return ItemPropertyLimitUseByClass(nSubType);
		case ITEM_PROPERTY_USE_LIMITATION_RACIAL_TYPE:               return ItemPropertyLimitUseByRace(nSubType);
		case ITEM_PROPERTY_USE_LIMITATION_SPECIFIC_ALIGNMENT:        return ItemPropertyLimitUseBySAlign(nSubType);
		case ITEM_PROPERTY_BONUS_HITPOINTS:                          return ItemPropertyBonusHitpoints(nCostValue);
		case ITEM_PROPERTY_REGENERATION_VAMPIRIC:                    return ItemPropertyVampiricRegeneration(nCostValue);
		// DELETED
		// DELETED
		case ITEM_PROPERTY_TRAP:                                     return ItemPropertyTrap(nSubType, nCostValue);
		case ITEM_PROPERTY_TRUE_SEEING:                              return ItemPropertyTrueSeeing();
		case ITEM_PROPERTY_ON_MONSTER_HIT:                           return ItemPropertyOnMonsterHitProperties(nSubType, nParam1); // Not sure, needs testing
		case ITEM_PROPERTY_TURN_RESISTANCE:                          return ItemPropertyTurnResistance(nCostValue);
		case ITEM_PROPERTY_MASSIVE_CRITICALS:                        return ItemPropertyMassiveCritical(nCostValue);
		case ITEM_PROPERTY_FREEDOM_OF_MOVEMENT:                      return ItemPropertyFreeAction();
		// DELETED
		case ITEM_PROPERTY_MONSTER_DAMAGE:                           return ItemPropertyMonsterDamage(nCostValue);
		case ITEM_PROPERTY_IMMUNITY_SPELLS_BY_LEVEL:                 return ItemPropertyImmunityToSpellLevel(nCostValue);
		case ITEM_PROPERTY_SPECIAL_WALK:                             return ItemPropertySpecialWalk(nSubType);
		case ITEM_PROPERTY_HEALERS_KIT:                              return ItemPropertyHealersKit(nCostValue);
		case ITEM_PROPERTY_WEIGHT_INCREASE:                          return ItemPropertyWeightIncrease(nParam1);
		case ITEM_PROPERTY_ONHITCASTSPELL:                           return ItemPropertyOnHitCastSpell(nSubType, nCostValue);
		case ITEM_PROPERTY_VISUALEFFECT:                             return ItemPropertyVisualEffect(nSubType);
		case ITEM_PROPERTY_ARCANE_SPELL_FAILURE:                     return ItemPropertyArcaneSpellFailure(nCostValue);
		// DELETED
		// DELETED
		// DELETED
		// DELETED
		// DELETED
		// case ITEM_PROPERTY_DAMAGE_REDUCTION: return ItemPropertyDamageReduction(nSubType, int nDRSubType, int nLimit=0, int nDRType=DR_TYPE_MAGICBONUS);
	}
	itemproperty ip;
	return ip;
}

string ItempropertyToString(itemproperty ip){
	return "(" + IntToString(GetItemPropertyType(ip)) + "," + IntToString(GetItemPropertySubType(ip)) + "," + IntToString(GetItemPropertyCostTableValue(ip)) + "," + IntToString(GetItemPropertyParam1Value(ip)) + ")";
}
// itemproperty StringToItemProperty(string sItemproperty){
// 	int nType = StringToInt(GetSubString(sItemproperty, 1, -1));
// 	int nOffset = FindSubString(sItemproperty, ",", 1) + 1;
// 	int nSubType = StringToInt(GetSubString(sItemproperty, nOffset, -1));
// 	nOffset = FindSubString(sItemproperty, ",", nOffset) + 1;
// 	int nCostValue = StringToInt(GetSubString(sItemproperty, nOffset, -1));
// 	nOffset = FindSubString(sItemproperty, ",", nOffset) + 1;
// 	int nParam1 = StringToInt(GetSubString(sItemproperty, nOffset, -1));

// 	return BuildItemProperty(nType, nSubType, nCostValue, nParam1);
// }

string GetItemPropertyGameString(itemproperty ip){

	string propName;
	string subType;
	string costValue;
	string paramValue;

	int propId = GetItemPropertyType(ip);
	propName = GetStringByStrRef(StringToInt(Get2DAString("itempropdef", "GameStrRef", propId)));

	string subTypeTable = Get2DAString("itempropdef", "SubTypeResRef", propId);
	if(subTypeTable != ""){
		int subTypeId = GetItemPropertySubType(ip);
		int nStrref = StringToInt(Get2DAString(subTypeTable, "Name", subTypeId));
		if(nStrref > 0)
			subType = GetStringByStrRef(nStrref);
	}
	string costValueTableId = Get2DAString("itempropdef", "CostTableResRef", propId);
	if(costValueTableId != ""){
		string costValueTable = Get2DAString("iprp_costtable", "Name", StringToInt(costValueTableId));
		int costId = GetItemPropertyCostTableValue(ip);
		int nStrref = StringToInt(Get2DAString(costValueTable, "Name", costId));
		if(nStrref > 0)
			costValue = GetStringByStrRef(nStrref);
	}

	string paramTableId = Get2DAString("itempropdef", "Param1ResRef", propId);
	if(paramTableId != ""){
		string paramTable = Get2DAString("iprp_paramtable", "TableResRef", StringToInt(paramTableId));
		int paramId = GetItemPropertyParam1Value(ip);
		int nStrref = StringToInt(Get2DAString(paramTable, "Name", paramId));
		if(nStrref > 0)
			paramValue = GetStringByStrRef(nStrref);
	}
	return propName
		+ (subType != ""? " " + subType : "")
		+ (costValue != ""? " " + costValue : "")
		+ (paramValue != ""? " " + paramValue : "");
}



int TargetCheck(object oTarget){
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 11;
	}
	if(GetObjectType(oTarget) != OBJECT_TYPE_ITEM){
		NWBash_Err("Target must be an item (current target is " + NWBash_ObjectToUserString(oTarget) + ")");
		return 12;
	}
	return 0;
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");

	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));
	int nArgsLen = ArrayGetLength(args);

	if(bNeedHelp && nArgsLen == 0){
		NWBash_Info(
			  "Usage: itemproperty list\n"
			+ "         # List itemproperties on an item\n"
			+ "       itemproperty add-id [options] <type> [subtype] [costvalue] [param1]\n"
			+ "         # Add a new item property\n"
			+ "       itemproperty remove <index>\n"
			+ "         # Remove an existing item property\n"
			+ "       itemproperty remove-ip <type> <subtype> <costvalue> <param1>\n"
			+ "         # Remove an existing item property matching the given item property\n"
			+ "       itemproperty find\n"
			+ "         # Helps find relevant item property arguments to build custom item properties\n"
		);
		return 0;
	}

	string sCommand = ArrayGetString(args, 0);
	args = ArrayPopFront(args);
	nArgsLen--;

	if(sCommand == "list"){
		if(bNeedHelp){
			NWBash_Info(
				  "List all item properties on an item\n"
				+ "\n"
				+ "Usage: itemproperty list"
			);
			return 0;
		}

		int nLen = ArrayGetLength(args);
		if(nLen != 0){
			NWBash_Err("Bad number of arguments. See 'itemproperty list --help'");
			return 10;
		}

		object oTarget = NWBash_GetTarget();
		int nRes = TargetCheck(oTarget);
		if(nRes != 0)
			return nRes;

		NWBash_Info("Item: " + NWBash_ObjectToUserString(oTarget));
		int i = 0;
		itemproperty ip = GetFirstItemProperty(oTarget);
		while(GetIsItemPropertyValid(ip))
		{
			NWBash_Info("- " + IntToString(i++) + ": " + GetItemPropertyGameString(ip) + " " + ItempropertyToString(ip));
			ip = GetNextItemProperty(oTarget);
		}
	}
	else if(sCommand == "remove"){
		if(bNeedHelp){
			NWBash_Info(
				  "Removes one or more itemproperty on an item, identified by their index (as shown using 'itemproperty list')\n"
				+ "\n"
				+ "Usage: itemproperty remove <index> [index...]"
			);
			return 0;
		}
		int nLen = ArrayGetLength(args);
		if(nLen < 1){
			NWBash_Err("Bad number of arguments. See 'itemproperty remove --help'");
			return 10;
		}

		object oTarget = NWBash_GetTarget();
		int nRes = TargetCheck(oTarget);
		if(nRes != 0)
			return nRes;

		int i = 0;
		itemproperty ip = GetFirstItemProperty(oTarget);
		while(GetIsItemPropertyValid(ip))
		{
			if(FindSubString(args.data, "|" + IntToString(i) + "|") >= 0){
				RemoveItemProperty(oTarget, ip);
				NWBash_Info("Removed itemproperty '" + GetItemPropertyGameString(ip) + "' " + ItempropertyToString(ip) + " on item " + NWBash_ObjectToUserString(oTarget));
			}
			i++;
			ip = GetNextItemProperty(oTarget);
		}
	}
	else if(sCommand == "remove-ip"){
		if(bNeedHelp){
			NWBash_Info(
				  "Removes one or more itemproperty on an item, identified by their properties\n"
				+ "Setting any argument to -1 will match any itemproperty value\n"
				+ "\n"
				+ "Usage: itemproperty remove-ip <type> <subtype> <costvalue> <param1>\n"
				+ "\n"
				+ "Examples:\n"
				+ "itemproperty remove-ip 11 0 6 0    # Remove all 50% weight reduction properties"
				+ "itemproperty remove-ip 16 -1 -1 -1 # Remove all damage bonus properties"
				+ "itemproperty remove-ip -1 -1 -1 -1 # Remove all item properties"
			);
			return 0;
		}
		int nLen = ArrayGetLength(args);
		if(nLen != 4){
			NWBash_Err("Bad number of arguments. See 'itemproperty remove-ip --help'");
			return 10;
		}

		object oTarget = NWBash_GetTarget();
		int nRes = TargetCheck(oTarget);
		if(nRes != 0)
			return nRes;

		int nMatchType = ArrayGetInt(args, 0);
		int nMatchSubType = ArrayGetInt(args, 1);
		int nMatchCostValue = ArrayGetInt(args, 2);
		int nMatchParam1 = ArrayGetInt(args, 3);

		// Override filters if there's no subtype / cost table / param1 table
		if(nMatchType >= 0){
			if(Get2DAString("itempropdef", "SubTypeResRef", nMatchType) == "")
				nMatchSubType = -1;
			if(Get2DAString("itempropdef", "CostTableResRef", nMatchType) == "")
				nMatchCostValue = -1;
			if(Get2DAString("itempropdef", "Param1ResRef", nMatchType) == "")
				nMatchParam1 = -1;
		}

		itemproperty ip = GetFirstItemProperty(oTarget);
		while(GetIsItemPropertyValid(ip))
		{
			int nType = GetItemPropertyType(ip);
			int nSubType = GetItemPropertySubType(ip);
			int nCostValue = GetItemPropertyCostTableValue(ip);
			int nParam1 = GetItemPropertyParam1Value(ip);

			if((nMatchType >= 0 ? nType == nMatchType : TRUE)
			&& (nMatchSubType >= 0 ? nSubType == nMatchSubType : TRUE)
			&& (nMatchCostValue >= 0 ? nCostValue == nMatchCostValue : TRUE)
			&& (nMatchParam1 >= 0 ? nParam1 == nMatchParam1 : TRUE)
			){
				RemoveItemProperty(oTarget, ip);
				NWBash_Info("Removed itemproperty '" + GetItemPropertyGameString(ip) + "' " + ItempropertyToString(ip) + " on item " + NWBash_ObjectToUserString(oTarget));
			}

			ip = GetNextItemProperty(oTarget);
		}
	}
	else if(sCommand == "add-id"){
		if(bNeedHelp){
			NWBash_Info(
				  "Adds a new item property on an item"
				+ "\n"
				+ "Usage: itemproperty add-id [options] <type> [subtype] [costvalue] [param1]\n"
				+ "\n"
				+ "Options:\n"
				+ "-d, --duration=DUR  Add a temporary item property that will last DUR seconds. Permanent by default\n"
			);
			return 0;
		}
		string sDur = NWBash_GetStringArg(args, "d|duration");
		args = NWBash_RemoveStringArgs(args, Array("|d|duration|"));

		int nLen = ArrayGetLength(args);
		if(nLen < 1 || nLen > 4){
			NWBash_Err("Bad number of arguments. See 'itemproperty add-id --help'");
			return 10;
		}

		object oTarget = NWBash_GetTarget();
		int nRes = TargetCheck(oTarget);
		if(nRes != 0)
			return nRes;

		if(sDur != "" && !NWBash_IsNumeric(sDur, TRUE)){
			NWBash_Err("duration must be a floating point");
			return 15;
		}
		float fDur = StringToFloat(sDur);

		int nType = ArrayGetInt(args, 0);
		int nSubType = ArrayGetInt(args, 1);
		int nCostValue = ArrayGetInt(args, 2);
		int nParam1 = ArrayGetInt(args, 3);

		itemproperty ip = BuildItemProperty(nType, nSubType, nCostValue, nParam1);

		AddItemProperty(fDur > 0.0 ? DURATION_TYPE_TEMPORARY : DURATION_TYPE_PERMANENT, ip, oTarget, fDur);
		NWBash_Info("Added itemproperty '" + GetItemPropertyGameString(ip) + "' " + ItempropertyToString(ip) + " on item " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sCommand == "find"){
		if(bNeedHelp){
			NWBash_Info(
				  "Helps find relevant item property arguments to build custom item properties\n"
				+ "\n"
				+ "Usage: itemproperty find\n"
				+ "         # List all existing itemproperty types\n"
				+ "       itemproperty find <type>\n"
				+ "         # List all itemproperty sybtypes costvalues and param1s for the given type\n"
				+ "       itemproperty find <type> <subtype> <costvalue> <param1>\n"
				+ "         # Verify an itemproperty is valid\n"
				+ "\n"
				+ "Examples:\n"
				+ "itemproperty find          # Lists all item property types\n"
				+ "itemproperty find 11       # Lists cost_values for 'WeightReduction'\n"
				+ "itemproperty find 11 0 6 0 # Check itemproperty is valid and is 50% weight reduction\n"
				+ "itemproperty find 17 5 7 8 # Check itemproperty is valid and is +1d6 divine dmg vs Evil\n"
			);
			return 0;
		}

		int nLen = ArrayGetLength(args);
		if(nLen == 0){
			NWBash_Info("ItemProperty <b>type</b>:");
			int nRows = GetNum2DARows("itempropdef");
			int i;
			for(i = 0 ; i < nRows ; i++){
				string sNameStrref = Get2DAString("itempropdef", "Name", i);
				if(sNameStrref != ""){
					string sLabel = Get2DAString("itempropdef", "Label", i);
					string sName = GetStringByStrRef(StringToInt(sNameStrref));

					NWBash_Info("- " + IntToString(i) + " (" + sLabel + "): " + sName);
				}
			}
		}
		if(nLen == 1){
			int nType = ArrayGetInt(args, 0);

			string sNameStrref = Get2DAString("itempropdef", "Name", nType);
			if(sNameStrref == ""){
				NWBash_Err("Invalid item property type " + IntToString(nType));
				return 11;
			}

			string sLabel = Get2DAString("itempropdef", "Label", nType);
			string sName = GetStringByStrRef(StringToInt(sNameStrref));

			string sSubTypeTable = Get2DAString("itempropdef", "SubTypeResRef", nType);
			if(sSubTypeTable != ""){
				NWBash_Info("Itemproperty <b>subtype</b>:");

				int nRows = GetNum2DARows(sSubTypeTable);
				int i;
				for(i = 0 ; i < nRows ; i++){
					string sNameStrref = Get2DAString(sSubTypeTable, "Name", i);
					if(sNameStrref != ""){
						string sLabel = Get2DAString(sSubTypeTable, "Label", i);
						string sName = GetStringByStrRef(StringToInt(sNameStrref));

						NWBash_Info("- " + IntToString(i) + " (" + sLabel + "): " + sName);
					}
				}
			}

			string sCostTableID = Get2DAString("itempropdef", "CostTableResRef", nType);
			if(sCostTableID != ""){
				string sCostTable = Get2DAString("iprp_costtable", "Name", StringToInt(sCostTableID));

				NWBash_Info("Itemproperty <b>cost_value</b>:");

				int nRows = GetNum2DARows(sCostTable);
				int i;
				for(i = 0 ; i < nRows ; i++){
					string sNameStrref = Get2DAString(sCostTable, "Name", i);
					if(sNameStrref != ""){
						string sLabel = Get2DAString(sCostTable, "Label", i);
						string sName = GetStringByStrRef(StringToInt(sNameStrref));

						NWBash_Info("- " + IntToString(i) + " (" + sLabel + "): " + sName);
					}
				}
			}

			string sParamTableID = Get2DAString("itempropdef", "Param1ResRef", nType);
			if(sParamTableID != ""){
				string sParamTable = Get2DAString("iprp_paramtable", "TableResRef", StringToInt(sParamTableID));

				NWBash_Info("Itemproperty <b>param1</b>:");

				int nRows = GetNum2DARows(sParamTable);
				int i;
				for(i = 0 ; i < nRows ; i++){
					string sNameStrref = Get2DAString(sParamTable, "Name", i);
					if(sNameStrref != ""){
						string sLabel = Get2DAString(sParamTable, "Label", i);
						string sName = GetStringByStrRef(StringToInt(sNameStrref));

						NWBash_Info("- " + IntToString(i) + " (" + sLabel + "): " + sName);
					}
				}
			}
		}
		else if(nLen == 4){
			int nType = ArrayGetInt(args, 0);
			int nSubType = ArrayGetInt(args, 1);
			int nCostValue = ArrayGetInt(args, 2);
			int nParam1 = ArrayGetInt(args, 3);

			itemproperty ip = BuildItemProperty(nType, nSubType, nCostValue, nParam1);
			if(GetIsItemPropertyValid(ip)){
				NWBash_Info("Valid item property: " + GetItemPropertyGameString(ip) + " " + ItempropertyToString(ip));
			}
			else{
				NWBash_Info("Invalid item property: (" + IntToString(nType) + "," + IntToString(nSubType) + "," + IntToString(nCostValue) + "," + IntToString(nParam1) + ")");
				return 1;
			}

		}
		else{
			NWBash_Err("Bad number of arguments. See 'itemproperty find --help'");
			return 10;
		}
	}
	else{
		NWBash_Err("Unknown subcommand " + sCommand);
		return 4;
	}
	return 0;
}
