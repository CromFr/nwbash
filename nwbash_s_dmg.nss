#include "nwbash_inc"

int StringToDamageType(string sDmgType){
	sDmgType = GetStringLowerCase(sDmgType);
	if     (sDmgType == "all")         return DAMAGE_TYPE_ALL;
	else if(sDmgType == "bludgeoning") return DAMAGE_TYPE_BLUDGEONING;
	else if(sDmgType == "piercing")    return DAMAGE_TYPE_PIERCING;
	else if(sDmgType == "slashing")    return DAMAGE_TYPE_SLASHING;
	else if(sDmgType == "magical")     return DAMAGE_TYPE_MAGICAL;
	else if(sDmgType == "acid")        return DAMAGE_TYPE_ACID;
	else if(sDmgType == "cold")        return DAMAGE_TYPE_COLD;
	else if(sDmgType == "divine")      return DAMAGE_TYPE_DIVINE;
	else if(sDmgType == "electrical")  return DAMAGE_TYPE_ELECTRICAL;
	else if(sDmgType == "fire")        return DAMAGE_TYPE_FIRE;
	else if(sDmgType == "negative")    return DAMAGE_TYPE_NEGATIVE;
	else if(sDmgType == "positive")    return DAMAGE_TYPE_POSITIVE;
	else if(sDmgType == "sonic")       return DAMAGE_TYPE_SONIC;
	return -1;
}

// Allows to deal more than 10000 dmg. Damage reduction will not work correctly (it will stack for every chunk of 10K damage).
effect EffectMassiveDamage(int nDamageAmount, int nDamageType=DAMAGE_TYPE_MAGICAL, int nDamagePower=DAMAGE_POWER_NORMAL, int nIgnoreResistances=FALSE){
	effect e = EffectDamage(nDamageAmount % 10000, nDamageType, nDamagePower, nIgnoreResistances);
	int i;
	int nChunks = nDamageAmount / 10000;
	for(i = 0 ; i < nChunks ; i++)
		e = EffectLinkEffects(e, EffectDamage(10000, nDamageType, nDamagePower, nIgnoreResistances));
	return e;
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info("Usage: dmg <amount> [type]\ntype: bludgeoning, piercing, slashing, magical, acid, cold, divine, electrical, fire, negative, positive, sonic");
		return 0;
	}
	int argsLen = ArrayGetLength(args);
	if(argsLen != 1 && argsLen != 2){
		NWBash_Err("Bad number of arguments. See !dmg --help");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 2;
	}

	int nAmount = ArrayGetInt(args, 0);
	string sType = ArrayGetString(args, 1);
	int nType = DAMAGE_TYPE_MAGICAL;
	if(sType != ""){
		nType = StringToDamageType(sType);
		if(nType < 0){
			NWBash_Err("Invalid damage type");
			return 3;
		}
	}

	ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectMassiveDamage(nAmount, nType, DAMAGE_POWER_NORMAL), oTarget);
	NWBash_Info("Hit " + NWBash_ObjectToUserString(oTarget) + " with " + IntToString(nAmount) + " damage");
	return 0;
}