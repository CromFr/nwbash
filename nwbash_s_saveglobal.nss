#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: global [options] <action> <save_name>\n"
			+ "\n"
			+ "action     'save' or 'load'\n"
			+ "save_name  Save file name\n"
		);
		return 0;
	}
	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen != 2){
		NWBash_Err("Bad number of arguments. See locvar --help");
		return 1;
	}

	string sAction = ArrayGetString(args, 0);
	string sSaveName = ArrayGetString(args, 1);

	if(sAction == "save"){
		SaveGlobalVariables(sSaveName);
		NWBash_Info("Saved global variables to '" + sSaveName + "'");
	}
	else if(sAction == "load"){
		LoadGlobalVariables(sSaveName);
		NWBash_Info("Loaded global variables from '" + sSaveName + "'");
	}
	else{
		NWBash_Err("Invalid action '" + sAction + "'");
		return 3;
	}

	return 0;
}