// Used for testing NWBash internal functions. Useless if you don't modify nwbash_inc.nss

#include "nwbash_inc"


void _NWBash_Enforce(int result, int id){
	if(!result){
		SendMessageToPC(OBJECT_SELF, "<c=red>    Enforce " + IntToString(id) + " failed");
	}
}
void _NWBash_EnforceSEq(string a, string b, int id){
	if(a != b){
		SendMessageToPC(OBJECT_SELF, "<c=red>    Enforce " + IntToString(id) + " failed: '" + a + "' != '" + b + "'");
	}
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(NWBash_PreprocessLine("_ -a 1 -b2 --ccc 3 --ddd=4 -e --fff='Hello World' --ggg --hhh 'Yolo World'").argsArray);
	_NWBash_EnforceSEq(args.data, "|-a|1|-b2|--ccc|3|--ddd=4|-e|--fff=Hello World|--ggg|--hhh|Yolo World|", 0);

	_NWBash_EnforceSEq(NWBash_GetStringArg(args, "a|"), "1", 1);
	_NWBash_EnforceSEq(NWBash_GetStringArg(args, "b|"), "2", 2);
	_NWBash_EnforceSEq(NWBash_GetStringArg(args, "ccc"), "3", 3);
	_NWBash_EnforceSEq(NWBash_GetStringArg(args, "ddd"), "4", 4);
	_NWBash_Enforce(NWBash_GetBooleanArg(args, "e|"), 5);
	_NWBash_EnforceSEq(NWBash_GetStringArg(args, "fff"), "Hello World", 6);
	_NWBash_Enforce(NWBash_GetBooleanArg(args, "ggg"), 7);
	_NWBash_EnforceSEq(NWBash_GetStringArg(args, "hhh"), "Yolo World", 8);

	_NWBash_EnforceSEq(NWBash_GetStringArg(args, "b|bbb"), "2", 9);

	args = NWBash_RemoveStringArgs(args, Array("|a|aaa|c|fff|"));
	_NWBash_Enforce(ArrayGetLength(args) == 8, 10);

	NWBash_SetEnvVar("TEST", "42");
	struct NWBash_PreprocessedLine line = NWBash_PreprocessLine("$TEST aaa$TEST aaa${TEST}aaa");
	_NWBash_EnforceSEq(line.envArray, "|", 11);
	_NWBash_EnforceSEq(line.cmd, "42", 12);
	_NWBash_EnforceSEq(line.argsArray, "|aaa42|aaa42aaa|", 13);
	// _NWBash_EnforceSEq(NWBash_PreprocessLine("$TEST aaa$TEST aaa${TEST}aaa"), "42 aaa42 aaa42aaa", 11);

	_NWBash_Enforce(NWBash_ResolveObjectString("__SELF__") == OBJECT_SELF, 20);
	_NWBash_Enforce(NWBash_ResolveObjectString("__MODULE__") == GetModule(), 21);
	_NWBash_Enforce(NWBash_ResolveObjectString("__AREA__") == GetArea(OBJECT_SELF), 22);
	object oDummy = GetObjectByTag("dummy");
	_NWBash_Enforce(NWBash_ResolveObjectString("t:dummy") == oDummy, 23);
	_NWBash_Enforce(NWBash_ResolveObjectString("0x" + ObjectToString(oDummy)) == oDummy, 24);
	_NWBash_Enforce(NWBash_ResolveObjectString("0d" + IntToString(ObjectToInt(oDummy))) == oDummy, 25);


	_NWBash_Enforce(NWBash_RunCmd("alias", Array("|s=speak -h|")) == 0, 30);
	line = NWBash_PreprocessLine("s");
	_NWBash_EnforceSEq(line.cmd, "speak", 31);
	_NWBash_EnforceSEq(line.argsArray, "|-h|", 32);
	line = NWBash_PreprocessLine("s 'hello world'");
	_NWBash_EnforceSEq(line.cmd, "speak", 33);
	_NWBash_EnforceSEq(line.argsArray, "|-h|hello world|", 34);


	NWBash_RunScript("TARGET=t:dummy speak SUCCESS !");

	return 0;
}