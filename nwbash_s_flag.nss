#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: flag <flag_set>\n"
			+ "flag_set: flag to set with its value. Multiple flag_set can be provided. ex: 'plot=1'\n"
			+ "\n"
			+ "Available flags:\n"
			+ "- plot: un-damageable\n"
			+ "- immortal: cannot fall under 1HP (creatures only)\n"
			+ "- useable: Can be clicked (placeables only)\n"
			+ "- cursed: Cannot be transfered (items only)\n"
			+ "- stolen: Has been stolen (items only)\n"
			+ "- droppable: Cannot be dropped on death (items only)\n"
			+ "- infinite: Infinite consumable (items only)\n"
			+ "- pickpocketable: Can be stolen (items only)\n"
			+ "- identified: Is identified (items only)\n"
			+ "- destroyable: Can be destroyed / raised / selected when dead. This flag takes multiple values: destroyable=1,0,1\n"
			+ "- recoverable: The trap can be recovered (trap triggers only)\n"
			+ "- commandable: Can change its action queue (creatures only)\n"
			+ "- scripthidden: Disable AI (creatures only)\n"
			+ "- comppossblocked: Companion possession is forbidden (creatures only)\n"
			+ "- orientondialog: Orient itself when talked to (creatures only)\n"
			+ "- collision: Collide with other creatures (creatures only)\n"
			+ "- bumpable: Can be bumped when walked into (creatures only)\n"
			+ "- cantalktonpc: Can talk to non-player characters\n"
			+ "- locked: Door/placeable lock state\n"
			+ "- trapped: Door/placeable/trigger trap active\n"
		);
		return 0;
	}
	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		NWBash_Err("Bad number of arguments. See !flag --help");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 2;
	}

	int i;
	for(i = 0 ; i < nArgsLen ; i++){
		string sFlag = ArrayGetString(args, i);
		int nEq = FindSubString(sFlag, "=");
		if(nEq < 0){
			NWBash_Err("Bad syntax: must be <flag>=<value>");
			return 4;
		}

		string sFlagName = GetStringLowerCase(GetStringLeft(sFlag, nEq));
		string sValue = GetStringLowerCase(GetSubString(sFlag, nEq + 1, -1));
		int bFlagValue = NWBash_StringToBool(sValue);

		if(sFlagName == "plot")
			SetPlotFlag(oTarget, bFlagValue);
		else if(sFlagName == "immortal"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE)
				NWBash_Warn("useable flag only works on placeables");
			SetImmortal(oTarget, bFlagValue);
		}
		else if(sFlagName == "useable"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_PLACEABLE)
				NWBash_Warn("useable flag only works on placeables");
			SetUseableFlag(oTarget, bFlagValue);
		}
		else if(sFlagName == "cursed"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_ITEM)
				NWBash_Warn("cursed flag only works on items");
			SetItemCursedFlag(oTarget, bFlagValue);
		}
		else if(sFlagName == "stolen"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_ITEM)
				NWBash_Warn("stolen flag only works on items");
			SetStolenFlag(oTarget, bFlagValue);
		}
		else if(sFlagName == "droppable"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_ITEM)
				NWBash_Warn("droppable flag only works on items");
			SetDroppableFlag(oTarget, bFlagValue);
		}
		else if(sFlagName == "infinite"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_ITEM)
				NWBash_Warn("infinite flag only works on items");
			SetInfiniteFlag(oTarget, bFlagValue);
		}
		else if(sFlagName == "pickpocketable"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_ITEM)
				NWBash_Warn("pickpocketable flag only works on items");
			SetPickpocketableFlag(oTarget, bFlagValue);
		}
		else if(sFlagName == "identified"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_ITEM)
				NWBash_Warn("identified flag only works on creatures");
			SetIdentified(oTarget, bFlagValue);
		}
		else if(sFlagName == "destroyable"){
			int n0 = 0;
			int n1 = FindSubString(sValue, ",", n0);
			int bDestroyable = StringToInt(GetSubString(sValue, n0, n1 - n0));
			n0 = n1;
			n1 = FindSubString(sValue, ",", n0);
			int bRaiseable = StringToInt(GetSubString(sValue, n0, n1 - n0));
			n0 = n1;
			n1 = FindSubString(sValue, ",", n0);
			int bSelectableWhenDead = StringToInt(GetSubString(sValue, n0, n1 - n0));

			AssignCommand(oTarget, SetIsDestroyable(bDestroyable, bRaiseable, bSelectableWhenDead));
			NWBash_Info("Set flag " + sFlagName + "=" + IntToString(bDestroyable) + "," + IntToString(bRaiseable) + "," + IntToString(bSelectableWhenDead) + " on " + NWBash_ObjectToUserString(oTarget));
			continue;
		}
		else if(sFlagName == "recoverable"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_TRIGGER)
				NWBash_Warn("recoverable flag only works on trap triggers");
			SetTrapRecoverable(oTarget, bFlagValue);
		}
		else if(sFlagName == "commandable"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE)
				NWBash_Warn("commandable flag only works on creatures");
			SetCommandable(bFlagValue, oTarget);
		}
		else if(sFlagName == "scripthidden"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE)
				NWBash_Warn("scripthidden flag only works on creatures");
			SetScriptHidden(oTarget, bFlagValue);
		}
		else if(sFlagName == "comppossblocked"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE)
				NWBash_Warn("comppossblocked flag only works on creatures");
			SetIsCompanionPossessionBlocked(oTarget, bFlagValue);
		}
		else if(sFlagName == "orientondialog"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE)
				NWBash_Warn("orientondialog flag only works on creatures");
			SetOrientOnDialog(oTarget, bFlagValue);
		}
		else if(sFlagName == "collision"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE)
				NWBash_Warn("collision flag only works on creatures");
			SetCollision(oTarget, bFlagValue);
		}
		else if(sFlagName == "bumpable"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE)
				NWBash_Warn("bumpable flag only works on creatures");
			SetBumpState(oTarget, bFlagValue ? BUMPSTATE_BUMPABLE : BUMPSTATE_UNBUMPABLE);
		}
		else if(sFlagName == "cantalktonpc"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE)
				NWBash_Warn("cantalktonpc flag only works on creatures");
			SetCanTalkToNonPlayerOwnedCreatures(oTarget, bFlagValue);
		}
		else if(sFlagName == "locked"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_DOOR && GetObjectType(oTarget) != OBJECT_TYPE_PLACEABLE)
				NWBash_Warn("locked flag only works on doors and placeables");
			SetLocked(oTarget, bFlagValue);
		}
		else if(sFlagName == "trapped"){
			if(GetObjectType(oTarget) != OBJECT_TYPE_DOOR && GetObjectType(oTarget) != OBJECT_TYPE_PLACEABLE && GetObjectType(oTarget) != OBJECT_TYPE_TRIGGER)
				NWBash_Warn("trapped flag only works on doors, placeables and triggers");
			SetTrapActive(oTarget, bFlagValue);
		}
		else{
			NWBash_Err("Unknown flag '" + sFlagName + "'");
			return 3;
		}

		NWBash_Info("Set flag " + sFlagName + "=" + IntToString(bFlagValue) + " on " + NWBash_ObjectToUserString(oTarget));
	}

	return 0;
}