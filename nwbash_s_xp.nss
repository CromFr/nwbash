#include "nwbash_inc"

void GiveXP(object oTarget, int nXP, int bSet){
	if(bSet){
		int nPrevXP = GetXP(oTarget);
		SetXP(oTarget, nXP);
		NWBash_Info("Set XP to " + IntToString(nXP) + " (from " + IntToString(nPrevXP) + ") to " + GetName(oTarget));
	}
	else{
		if(nXP < 0)
			SetXP(oTarget, GetXP(oTarget) + nXP);
		else
			GiveXPToCreature(oTarget, nXP);
		NWBash_Info("Gave " + IntToString(nXP) + " XP to " + GetName(oTarget));
	}
}

int StartingConditional(string sArgsData){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: xp [options] [amount]\n"
			+ "Give XP or print the current XP of a creature\n"
			+ "If no amount is given, prints how much XP the creature has\n"
			+ "Options can be:\n"
			+ "-a, --area  Act on all players in this area\n"
			+ "--set       Set the amount of XP to the given value\n"
			+ "-h, --help  show this help"
		);
		return 0;
	}
	int bArea = NWBash_GetBooleanArg(args, "a|area");
	int bSet = NWBash_GetBooleanArg(args, "set");
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|a|area|set|"));

	object oTarget = NWBash_GetTarget();

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen > 1){
		NWBash_Err("Bad number of arguments. See 'xp --help'");
		return 1;
	}

	int nXP = ArrayGetInt(args, 0);


	if(bArea){
		object oPC = GetFirstPC();
		while(GetIsObjectValid(oPC))
		{
			if(GetArea(oPC) == GetArea(oTarget)){
				if(nArgsLen == 0)
					NWBash_Info(NWBash_ObjectToUserString(oPC) + " has " + IntToString(GetXP(oPC)) + " XP");
				else
					GiveXP(oPC, nXP, bSet);
			}
			oPC = GetNextPC();
		}
	}
	else{
		if(!GetIsObjectValid(oTarget)){
			NWBash_Err("Invalid target");
			return 3;
		}
		if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE){
			NWBash_Err("Target is not a creature");
			return 4;
		}

		if(nArgsLen == 0)
			NWBash_Info(NWBash_ObjectToUserString(oTarget) + " has " + IntToString(GetXP(oTarget)) + " XP");
		else
			GiveXP(oTarget, nXP, bSet);
	}
	return 0;
}