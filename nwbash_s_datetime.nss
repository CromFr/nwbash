#include "nwbash_inc"


struct DateTime {
	int year, month, day, hours, minutes, seconds, msecs;
};
struct DateTime GetNow(){
	struct DateTime ret;
	ret.year = GetCalendarYear();
	ret.month = GetCalendarMonth();
	ret.day = GetCalendarDay();
	ret.hours = GetTimeHour();
	ret.minutes = GetTimeMinute();
	ret.seconds = GetTimeSecond();
	ret.msecs = GetTimeMillisecond();
	return ret;
}
string DateTimeToString(struct DateTime dt){
	string ret;
	ret += (dt.msecs < 1000 ? "0" : "") + (dt.msecs < 100 ? "0" : "") + (dt.msecs < 10 ? "0" : "") + IntToString(dt.year);
	ret += "-" + (dt.month < 10 ? "0" : "") + IntToString(dt.month);
	ret += "-" + (dt.day < 10 ? "0" : "") + IntToString(dt.day);
	ret += "T" + (dt.hours < 10 ? "0" : "") + IntToString(dt.hours);
	ret += ":" + (dt.minutes < 10 ? "0" : "") + IntToString(dt.minutes);
	ret += ":" + (dt.seconds < 10 ? "0" : "") + IntToString(dt.seconds);
	ret += "." + (dt.msecs < 100 ? "0" : "") + (dt.msecs < 10 ? "0" : "") + IntToString(dt.msecs);
	return ret;
}

int GetIsIntString(string s){
	int nLen = GetStringLength(s);
	if(nLen == 0)
		return FALSE;
	int i;
	for(i = 0 ; i < nLen ; i++){
		int c = CharToASCII(GetSubString(s, i, 1));
		if(c < 0x30 || c > 0x39)
			return FALSE;
	}
	return TRUE;
}

int CheckDate(string sDate){
	// YYYY-MM-DD
	int yearLen = FindSubString(sDate, "-") + 1;
	return yearLen > 0
		&& GetStringLength(sDate) >= (6 + yearLen)
		&& GetIsIntString(GetSubString(sDate, 0, yearLen))
		&& GetSubString(sDate, yearLen, 1) == "-"
		&& GetIsIntString(GetSubString(sDate, yearLen + 1, 2))
		&& GetSubString(sDate, yearLen + 3, 1) == "-"
		&& GetIsIntString(GetSubString(sDate, yearLen + 4, 2));
}
struct DateTime FillDate(struct DateTime dt, string sDate){
	// YYYY-MM-DD
	int yearLen = FindSubString(sDate, "-") + 1;

	dt.year = StringToInt(GetSubString(sDate, 0, yearLen));
	dt.month = StringToInt(GetSubString(sDate, yearLen + 1, 2));
	dt.day = StringToInt(GetSubString(sDate, yearLen + 4, 2));
	return dt;
}
int CheckTime(string sTime){
	// HH:MM:SS.sss
	return GetStringLength(sTime) >= 8
		&& GetIsIntString(GetSubString(sTime, 0, 2))
		&& GetSubString(sTime, 2, 1) == ":"
		&& GetIsIntString(GetSubString(sTime, 3, 2))
		&& GetSubString(sTime, 5, 1) == ":"
		&& GetIsIntString(GetSubString(sTime, 6, 2))
		&& (GetStringLength(sTime) <= 8 || (GetSubString(sTime, 8, 1) == "." && GetIsIntString(GetSubString(sTime, 9, 3))));
}
struct DateTime FillTime(struct DateTime dt, string sTime){
	// HH:MM:SS.sss
	dt.hours = StringToInt(GetSubString(sTime, 0, 2));
	dt.minutes = StringToInt(GetSubString(sTime, 3, 2));
	dt.seconds = StringToInt(GetSubString(sTime, 6, 2));
	if(GetStringLength(sTime) > 8)
		dt.msecs = StringToInt(GetSubString(sTime, 9, 3));
	return dt;
}

void InvalidFormat(){
	NWBash_Err("Invalid date-time format. Must be either: YYYY-MM-DDThh:mm:ss.sss, YYYY-MM-DD or hh:mm:ss.sss");
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			  "Usage: datetime [options] [<datetime>]\n"
			+ "       datetime [options] \n"
			+ "If <datetime> is provided, sets the current date and time on the module. Otherwise prints the current date-time.\n"
			+ "<datetime> can be one of the following forms:\n"
			+ "- YYYY-MM-DDThh:mm:ss.sss  date + time, ex: 2021-01-30T15:30:00.243\n"
			+ "- YYYY-MM-DD               date only, ex: 2021-01-30\n"
			+ "- hh:mm:ss.sss             time only, ex: 15:30:00.243\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help        Show this help\n"
		);
		return 0;
	}

	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		NWBash_Info("Current date-time is: " + DateTimeToString(GetNow()));
	}
	else if(nArgsLen == 1){
		string sDateTime = ArrayGetString(args, 0);
		int nLen = GetStringLength(sDateTime);

		struct DateTime datetime = GetNow();
		string sDateTimeFrom = DateTimeToString(datetime);

		if(CheckDate(sDateTime)){
			// YYYY-MM-DDTHH:MM:SS.sss
			datetime = FillDate(datetime, sDateTime);

			int nTPos = FindSubString(sDateTime, "T");

			if(nTPos >= 0){
				string sTime = GetSubString(sDateTime, nTPos + 1, -1);
				if(!CheckTime(sTime)){
					InvalidFormat();
					return 3;
				}
				datetime = FillTime(datetime, sTime);
			}
		}
		else if(CheckTime(sDateTime)){
			// HH:MM:SS.sss
			datetime = FillTime(datetime, sDateTime);
		}
		else{
			InvalidFormat();
			return 3;
		}

		string sDateTimeTo = DateTimeToString(datetime);
		if(StringCompare(sDateTimeTo, sDateTimeFrom) < 0){
			NWBash_Err("Time can only be advanced forward. Cannot set date-time from " + sDateTimeFrom + " to " + sDateTimeTo);
			return 2;
		}

		SetTime(datetime.hours, datetime.minutes, datetime.seconds, datetime.msecs);
		SetCalendar(datetime.year, datetime.month, datetime.day);

		NWBash_Info("Set date from " + sDateTimeFrom + " to " + sDateTimeTo);
	}
	else{
		NWBash_Err("Bad number of arguments. Must be 0 or 1");
		return 1;
	}


	return 0;
}
