
# Get help

To get help, write in the chat:
<c=#0A3263>!help</c>

There is also:
<c=#0A3263>!help builtin</c>
<c=#0A3263>!help values</c>

Most command print their specific help with the <c=#0A3263>-h</c> or <c=#0A3263>--help</c>
<c=#0A3263>!alias --help</c>
<c=#0A3263>!exec --help</c>




# Targeting

The <c=#0A3263>speak</c> command makes the target speak

You can target this dummy in different ways:

1/ By selecting the dummy
Perform a short right click on the dummy, and execute the command:
<c=#0A3263>!speak "Hello world"</c>

2/ By temporarily setting the TARGET environment variable
Environment variables are variables internal to NWBash, that you can set using <c=#0A3263>!export VARNAME=value</c>, and view using <c=#0A3263>!env</c>.

TARGET is a special environment variable that is used by most commands to do actions on a subject.
If the TARGET environment variable is <b>not</b> set, the script target will be the selected object, or if nothing is selected it will be the player executing the command.
If the TARGET environment variable is set, the script target will be the object pointed by its value.

This dummy has the tag "dummy", you can make him speak with:

<c=#0A3263>!TARGET=t:dummy speak "Good morning"</c>

Setting the env. variable before the command will make it effective only for the duration of this command.

<c=#0A3263>t:dummy</c> is what is called an object value specification. See <c=#0A3263>!help values</c> for more information.


3/ By exporting the TARGET environment variable

Exporting an environment variable will keep its value set until the server reboots or yuou manually reset it. Env. variables only affect your character (they are stored a local variables on your character).

<c=#0A3263>!export TARGET=t:dummy</c>
<c=#0A3263>!speak "Greetings traveler !"</c>
<c=#0A3263>!speak "What can I get you?"</c>

To unset an environment variable:
<c=#0A3263>!unexport TARGET</c>



# Spawning

Spawn a creature using:
<c=#0A3263>!create c_human --export=PUPPET</c=#0A3263>
This command creates a human and stores its reference into the PUPPET environment variable.
This command can create any kind of object. See <c=#0A3263>!create --help</c=#0A3263> for more !

Storing the created zombie in an env variable is handy for doing further actions on it:
<c=#0A3263>!TARGET=$PUPPET anim --loop dance01</c>
<c=#0A3263>!TARGET=$PUPPET vfx sp_holy_cast</c>

    








