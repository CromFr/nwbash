#include "nwbash_inc"


int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: rmeffects [options]\n"
			+ "Options can be:\n"
			+ "--spells    Only remove spell effects\n"
			+ "-h, --help  Show this help"
		);
		return 0;
	}
	int bSpell = NWBash_GetBooleanArg(args, "spells");


	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 1;
	}

	if(bSpell){
		effect e = GetFirstEffect(oTarget);
		while(GetIsEffectValid(e)){
			if(GetEffectSpellId(e) >= 0)
				RemoveEffect(oTarget, e);
			e = GetNextEffect(oTarget);
		}
		NWBash_Info("Removed spell effects on " + NWBash_ObjectToUserString(oTarget));
	}
	else{
		effect e = GetFirstEffect(oTarget);
		while(GetIsEffectValid(e)){
			RemoveEffect(oTarget, e);
			e = GetNextEffect(oTarget);
		}
		NWBash_Info("Removed all effects on " + NWBash_ObjectToUserString(oTarget));
	}

	return 0;
}