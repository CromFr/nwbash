#include "nwbash_inc"



string GetScriptList(object oTarget){
	switch(GetObjectType(oTarget)){
		case OBJECT_TYPE_CREATURE:
			return "ON_HEARTBEAT:          " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_HEARTBEAT) + "\n"
			     + "ON_NOTICE:             " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_NOTICE) + "\n"
			     + "ON_SPELLCASTAT:        " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_SPELLCASTAT) + "\n"
			     + "ON_MELEE_ATTACKED:     " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_MELEE_ATTACKED) + "\n"
			     + "ON_DAMAGED:            " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_DAMAGED) + "\n"
			     + "ON_DISTURBED:          " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_DISTURBED) + "\n"
			     + "ON_END_COMBATROUND:    " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_END_COMBATROUND) + "\n"
			     + "ON_DIALOGUE:           " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_DIALOGUE) + "\n"
			     + "ON_SPAWN_IN:           " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_SPAWN_IN) + "\n"
			     + "ON_RESTED:             " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_RESTED) + "\n"
			     + "ON_DEATH:              " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_DEATH) + "\n"
			     + "ON_USER_DEFINED_EVENT: " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_USER_DEFINED_EVENT) + "\n"
			     + "ON_BLOCKED_BY_DOOR:    " + GetEventHandler(oTarget, CREATURE_SCRIPT_ON_BLOCKED_BY_DOOR);
		case OBJECT_TYPE_TRIGGER:
			return "ON_HEARTBEAT:          " + GetEventHandler(oTarget, SCRIPT_TRIGGER_ON_HEARTBEAT) + "\n"
				 + "ON_OBJECT_ENTER:       " + GetEventHandler(oTarget, SCRIPT_TRIGGER_ON_OBJECT_ENTER) + "\n"
				 + "ON_OBJECT_EXIT:        " + GetEventHandler(oTarget, SCRIPT_TRIGGER_ON_OBJECT_EXIT) + "\n"
				 + "ON_USER_DEFINED_EVENT: " + GetEventHandler(oTarget, SCRIPT_TRIGGER_ON_USER_DEFINED_EVENT) + "\n"
				 + "ON_TRAPTRIGGERED:      " + GetEventHandler(oTarget, SCRIPT_TRIGGER_ON_TRAPTRIGGERED) + "\n"
				 + "ON_DISARMED:           " + GetEventHandler(oTarget, SCRIPT_TRIGGER_ON_DISARMED) + "\n"
				 + "ON_CLICKED:            " + GetEventHandler(oTarget, SCRIPT_TRIGGER_ON_CLICKED);
		case OBJECT_TYPE_DOOR:
			return "ON_OPEN:           " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_OPEN) + "\n"
				 + "ON_CLOSE:          " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_CLOSE) + "\n"
				 + "ON_DAMAGE:         " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_DAMAGE) + "\n"
				 + "ON_DEATH:          " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_DEATH) + "\n"
				 + "ON_DISARM:         " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_DISARM) + "\n"
				 + "ON_HEARTBEAT:      " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_HEARTBEAT) + "\n"
				 + "ON_LOCK:           " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_LOCK) + "\n"
				 + "ON_MELEE_ATTACKED: " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_MELEE_ATTACKED) + "\n"
				 + "ON_SPELLCASTAT:    " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_SPELLCASTAT) + "\n"
				 + "ON_TRAPTRIGGERED:  " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_TRAPTRIGGERED) + "\n"
				 + "ON_UNLOCK:         " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_UNLOCK) + "\n"
				 + "ON_USERDEFINED:    " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_USERDEFINED) + "\n"
				 + "ON_CLICKED:        " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_CLICKED) + "\n"
				 + "ON_DIALOGUE:       " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_DIALOGUE) + "\n"
				 + "ON_FAIL_TO_OPEN:   " + GetEventHandler(oTarget, SCRIPT_DOOR_ON_FAIL_TO_OPEN);
		case OBJECT_TYPE_ENCOUNTER:
			return "ON_OBJECT_ENTER:        " + GetEventHandler(oTarget, SCRIPT_ENCOUNTER_ON_OBJECT_ENTER) + "\n"
				 + "ON_OBJECT_EXIT:         " + GetEventHandler(oTarget, SCRIPT_ENCOUNTER_ON_OBJECT_EXIT) + "\n"
				 + "ON_HEARTBEAT:           " + GetEventHandler(oTarget, SCRIPT_ENCOUNTER_ON_HEARTBEAT) + "\n"
				 + "ON_ENCOUNTER_EXHAUSTED: " + GetEventHandler(oTarget, SCRIPT_ENCOUNTER_ON_ENCOUNTER_EXHAUSTED) + "\n"
				 + "ON_USER_DEFINED_EVENT:  " + GetEventHandler(oTarget, SCRIPT_ENCOUNTER_ON_USER_DEFINED_EVENT);
		case OBJECT_TYPE_AREA_OF_EFFECT:
			return "HEARTBEAT:          " + GetEventHandler(oTarget, SCRIPT_AOE_ON_HEARTBEAT) + "\n"
				 + "USER_DEFINED_EVENT: " + GetEventHandler(oTarget, SCRIPT_AOE_ON_USER_DEFINED_EVENT) + "\n"
				 + "OBJECT_ENTER:       " + GetEventHandler(oTarget, SCRIPT_AOE_ON_OBJECT_ENTER) + "\n"
				 + "OBJECT_EXIT:        " + GetEventHandler(oTarget, SCRIPT_AOE_ON_OBJECT_EXIT);
		case OBJECT_TYPE_STORE:
			return "ON_OPEN:  " + GetEventHandler(oTarget, SCRIPT_STORE_ON_OPEN) + "\n"
				  + "ON_CLOSE: " + GetEventHandler(oTarget, SCRIPT_STORE_ON_CLOSE);
		case OBJECT_TYPE_ITEM:
		case OBJECT_TYPE_LIGHT:
		case OBJECT_TYPE_WAYPOINT:
		case OBJECT_TYPE_PLACED_EFFECT:
			return "No scripts";
		default:
			if(GetArea(oTarget) == oTarget){
				return "ON_HEARTBEAT:          " + GetEventHandler(oTarget, SCRIPT_AREA_ON_HEARTBEAT) + "\n"
					 + "ON_USER_DEFINED_EVENT: " + GetEventHandler(oTarget, SCRIPT_AREA_ON_USER_DEFINED_EVENT) + "\n"
					 + "ON_ENTER:              " + GetEventHandler(oTarget, SCRIPT_AREA_ON_ENTER) + "\n"
					 + "ON_EXIT:               " + GetEventHandler(oTarget, SCRIPT_AREA_ON_EXIT) + "\n"
					 + "ON_CLIENT_ENTER:       " + GetEventHandler(oTarget, SCRIPT_AREA_ON_CLIENT_ENTER);
			}
			else if(oTarget == GetModule()){
				return "ON_CLOSED:             " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_CLOSED) + "\n"
					 + "ON_DAMAGED:            " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_DAMAGED) + "\n"
					 + "ON_DEATH:              " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_DEATH) + "\n"
					 + "ON_DISARM:             " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_DISARM) + "\n"
					 + "ON_HEARTBEAT:          " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_HEARTBEAT) + "\n"
					 + "ON_INVENTORYDISTURBED: " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_INVENTORYDISTURBED) + "\n"
					 + "ON_LOCK:               " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_LOCK) + "\n"
					 + "ON_MELEEATTACKED:      " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_MELEEATTACKED) + "\n"
					 + "ON_OPEN:               " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_OPEN) + "\n"
					 + "ON_SPELLCASTAT:        " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_SPELLCASTAT) + "\n"
					 + "ON_TRAPTRIGGERED:      " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_TRAPTRIGGERED) + "\n"
					 + "ON_UNLOCK:             " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_UNLOCK) + "\n"
					 + "ON_USED:               " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_USED) + "\n"
					 + "ON_USER_DEFINED_EVENT: " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_USER_DEFINED_EVENT) + "\n"
					 + "ON_DIALOGUE:           " + GetEventHandler(oTarget, SCRIPT_PLACEABLE_ON_DIALOGUE);
			}
	}
	return "Unknown object type";
}


int StringToEventID(object oTarget, string sEvent){
	sEvent = GetStringUpperCase(sEvent);

	int nEventID;
	switch(GetObjectType(oTarget)){
		case OBJECT_TYPE_CREATURE:
			if(     sEvent == "ON_HEARTBEAT")          return CREATURE_SCRIPT_ON_HEARTBEAT;
			else if(sEvent == "ON_NOTICE")             return CREATURE_SCRIPT_ON_NOTICE;
			else if(sEvent == "ON_SPELLCASTAT")        return CREATURE_SCRIPT_ON_SPELLCASTAT;
			else if(sEvent == "ON_MELEE_ATTACKED")     return CREATURE_SCRIPT_ON_MELEE_ATTACKED;
			else if(sEvent == "ON_DAMAGED")            return CREATURE_SCRIPT_ON_DAMAGED;
			else if(sEvent == "ON_DISTURBED")          return CREATURE_SCRIPT_ON_DISTURBED;
			else if(sEvent == "ON_END_COMBATROUND")    return CREATURE_SCRIPT_ON_END_COMBATROUND;
			else if(sEvent == "ON_DIALOGUE")           return CREATURE_SCRIPT_ON_DIALOGUE;
			else if(sEvent == "ON_SPAWN_IN")           return CREATURE_SCRIPT_ON_SPAWN_IN;
			else if(sEvent == "ON_RESTED")             return CREATURE_SCRIPT_ON_RESTED;
			else if(sEvent == "ON_DEATH")              return CREATURE_SCRIPT_ON_DEATH;
			else if(sEvent == "ON_USER_DEFINED_EVENT") return CREATURE_SCRIPT_ON_USER_DEFINED_EVENT;
			else if(sEvent == "ON_BLOCKED_BY_DOOR")    return CREATURE_SCRIPT_ON_BLOCKED_BY_DOOR;
			else return -1;
		case OBJECT_TYPE_TRIGGER:
			if(     sEvent == "ON_HEARTBEAT")          return SCRIPT_TRIGGER_ON_HEARTBEAT;
			else if(sEvent == "ON_OBJECT_ENTER")       return SCRIPT_TRIGGER_ON_OBJECT_ENTER;
			else if(sEvent == "ON_OBJECT_EXIT")        return SCRIPT_TRIGGER_ON_OBJECT_EXIT;
			else if(sEvent == "ON_USER_DEFINED_EVENT") return SCRIPT_TRIGGER_ON_USER_DEFINED_EVENT;
			else if(sEvent == "ON_TRAPTRIGGERED")      return SCRIPT_TRIGGER_ON_TRAPTRIGGERED;
			else if(sEvent == "ON_DISARMED")           return SCRIPT_TRIGGER_ON_DISARMED;
			else if(sEvent == "ON_CLICKED")            return SCRIPT_TRIGGER_ON_CLICKED;
			else return -1;
		case OBJECT_TYPE_DOOR:
			if(     sEvent == "ON_OPEN")           return SCRIPT_DOOR_ON_OPEN;
			else if(sEvent == "ON_CLOSE")          return SCRIPT_DOOR_ON_CLOSE;
			else if(sEvent == "ON_DAMAGE")         return SCRIPT_DOOR_ON_DAMAGE;
			else if(sEvent == "ON_DEATH")          return SCRIPT_DOOR_ON_DEATH;
			else if(sEvent == "ON_DISARM")         return SCRIPT_DOOR_ON_DISARM;
			else if(sEvent == "ON_HEARTBEAT")      return SCRIPT_DOOR_ON_HEARTBEAT;
			else if(sEvent == "ON_LOCK")           return SCRIPT_DOOR_ON_LOCK;
			else if(sEvent == "ON_MELEE_ATTACKED") return SCRIPT_DOOR_ON_MELEE_ATTACKED;
			else if(sEvent == "ON_SPELLCASTAT")    return SCRIPT_DOOR_ON_SPELLCASTAT;
			else if(sEvent == "ON_TRAPTRIGGERED")  return SCRIPT_DOOR_ON_TRAPTRIGGERED;
			else if(sEvent == "ON_UNLOCK")         return SCRIPT_DOOR_ON_UNLOCK;
			else if(sEvent == "ON_USERDEFINED")    return SCRIPT_DOOR_ON_USERDEFINED;
			else if(sEvent == "ON_CLICKED")        return SCRIPT_DOOR_ON_CLICKED;
			else if(sEvent == "ON_DIALOGUE")       return SCRIPT_DOOR_ON_DIALOGUE;
			else if(sEvent == "ON_FAIL_TO_OPEN")   return SCRIPT_DOOR_ON_FAIL_TO_OPEN;
			else return -1;
		case OBJECT_TYPE_ENCOUNTER:
			if(     sEvent == "ON_OBJECT_ENTER")        return SCRIPT_ENCOUNTER_ON_OBJECT_ENTER;
			else if(sEvent == "ON_OBJECT_EXIT")         return SCRIPT_ENCOUNTER_ON_OBJECT_EXIT;
			else if(sEvent == "ON_HEARTBEAT")           return SCRIPT_ENCOUNTER_ON_HEARTBEAT;
			else if(sEvent == "ON_ENCOUNTER_EXHAUSTED") return SCRIPT_ENCOUNTER_ON_ENCOUNTER_EXHAUSTED;
			else if(sEvent == "ON_USER_DEFINED_EVENT")  return SCRIPT_ENCOUNTER_ON_USER_DEFINED_EVENT;
			else return -1;
		case OBJECT_TYPE_AREA_OF_EFFECT:
			if(     sEvent == "HEARTBEAT")          return SCRIPT_AOE_ON_HEARTBEAT;
			else if(sEvent == "USER_DEFINED_EVENT") return SCRIPT_AOE_ON_USER_DEFINED_EVENT;
			else if(sEvent == "OBJECT_ENTER")       return SCRIPT_AOE_ON_OBJECT_ENTER;
			else if(sEvent == "OBJECT_EXIT")        return SCRIPT_AOE_ON_OBJECT_EXIT;
			else return -1;
		case OBJECT_TYPE_STORE:
			if(     sEvent == "ON_OPEN")  return SCRIPT_STORE_ON_OPEN;
			else if(sEvent == "ON_CLOSE") return SCRIPT_STORE_ON_CLOSE;
			else return -1;
		case OBJECT_TYPE_ITEM:
		case OBJECT_TYPE_LIGHT:
		case OBJECT_TYPE_WAYPOINT:
		case OBJECT_TYPE_PLACED_EFFECT:
			return -1;
		default:
			if(GetArea(oTarget) == oTarget){
				if(     sEvent == "ON_HEARTBEAT")          return SCRIPT_AREA_ON_HEARTBEAT;
				else if(sEvent == "ON_USER_DEFINED_EVENT") return SCRIPT_AREA_ON_USER_DEFINED_EVENT;
				else if(sEvent == "ON_ENTER")              return SCRIPT_AREA_ON_ENTER;
				else if(sEvent == "ON_EXIT")               return SCRIPT_AREA_ON_EXIT;
				else if(sEvent == "ON_CLIENT_ENTER")       return SCRIPT_AREA_ON_CLIENT_ENTER;
				else return -1;
			}
			else if(oTarget == GetModule()){
				if(     sEvent == "ON_CLOSED")             return SCRIPT_PLACEABLE_ON_CLOSED;
				else if(sEvent == "ON_DAMAGED")            return SCRIPT_PLACEABLE_ON_DAMAGED;
				else if(sEvent == "ON_DEATH")              return SCRIPT_PLACEABLE_ON_DEATH;
				else if(sEvent == "ON_DISARM")             return SCRIPT_PLACEABLE_ON_DISARM;
				else if(sEvent == "ON_HEARTBEAT")          return SCRIPT_PLACEABLE_ON_HEARTBEAT;
				else if(sEvent == "ON_INVENTORYDISTURBED") return SCRIPT_PLACEABLE_ON_INVENTORYDISTURBED;
				else if(sEvent == "ON_LOCK")               return SCRIPT_PLACEABLE_ON_LOCK;
				else if(sEvent == "ON_MELEEATTACKED")      return SCRIPT_PLACEABLE_ON_MELEEATTACKED;
				else if(sEvent == "ON_OPEN")               return SCRIPT_PLACEABLE_ON_OPEN;
				else if(sEvent == "ON_SPELLCASTAT")        return SCRIPT_PLACEABLE_ON_SPELLCASTAT;
				else if(sEvent == "ON_TRAPTRIGGERED")      return SCRIPT_PLACEABLE_ON_TRAPTRIGGERED;
				else if(sEvent == "ON_UNLOCK")             return SCRIPT_PLACEABLE_ON_UNLOCK;
				else if(sEvent == "ON_USED")               return SCRIPT_PLACEABLE_ON_USED;
				else if(sEvent == "ON_USER_DEFINED_EVENT") return SCRIPT_PLACEABLE_ON_USER_DEFINED_EVENT;
				else if(sEvent == "ON_DIALOGUE")           return SCRIPT_PLACEABLE_ON_DIALOGUE;
				else return -1;
			}
	}
	return -1;
}





int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			  "Usage: scriptset [options]\n"
			+ "       scriptset [options] <event>=<script> [<event>=<script> ...]\n"
			+ "\n"
			+ "The first form prints all scripts for the target object.\n"
			+ "The second form sets one or more scripts for the target object\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help        Show this help\n"
		);
		return 0;
	}

	string sDur = NWBash_GetStringArg(args, "d|dur");

	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));

	object oTarget = NWBash_GetTarget();

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		NWBash_Info("Scripts on object " + NWBash_ObjectToUserString(oTarget) + "\n" + GetScriptList(oTarget));
	}
	else{
		int i;
		for(i = 0 ; i < nArgsLen ; i++)
		{
			string sAssignment = ArrayGetString(args, i);
			int nEqPos = FindSubString(sAssignment, "=");
			if(nEqPos < 0){
				NWBash_Err("No equal sign in script assignment: " + sAssignment);
				return 10;
			}

			string sEvent = GetSubString(sAssignment, 0, nEqPos);
			string sScript = GetSubString(sAssignment, nEqPos + 1, -1);

			int nEventID = StringToEventID(oTarget, sEvent);
			if(nEventID < 0){
				NWBash_Err("Unknown event name '" + sEvent + "' for object " + NWBash_ObjectToUserString(oTarget));
				return 11;
			}

			SetEventHandler(oTarget, nEventID, sScript);
			NWBash_Info("Set script " + sScript + " for event " + sEvent + " on object " + NWBash_ObjectToUserString(oTarget));
		}
	}

	return 0;
}
