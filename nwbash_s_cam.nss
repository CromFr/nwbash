#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: cam <campaign_name> <var_type> <var_name> [value]\n"
			+ "var_type: can be 'int', 'float', 'string', 'vector', 'location'\n"
			+ "\n"
			+ "Examples:\n"
			+ "cam quests int save_the_world    # Display the value of the 'save_the_world' integer of the 'quests' database\n"
			+ "cam quests int save_the_world 42 # Set the value of the 'save_the_world' integer of the 'quests' database to 42\n"
		);
		return 0;
	}
	int argsLen = ArrayGetLength(args);
	if(argsLen != 3 && argsLen != 4){
		NWBash_Err("Bad number of arguments. See cam --help");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target: " + NWBash_ObjectToUserString(oTarget));
		return 2;
	}

	string sCampaign = ArrayGetString(args, 0);
	string sType = GetStringLowerCase(ArrayGetString(args, 1));
	string sVarName = ArrayGetString(args, 2);
	string sValue = ArrayGetString(args, 3);

	if(argsLen == 3){
		// Get value
		if(sType == "int")           sValue = IntToString(GetCampaignInt(sCampaign, sVarName, oTarget));
		else if(sType == "float")    sValue = FloatToString(GetCampaignFloat(sCampaign, sVarName, oTarget));
		else if(sType == "string")   sValue = GetCampaignString(sCampaign, sVarName, oTarget);
		else if(sType == "vector")   sValue = NWBash_VectorToString(GetCampaignVector(sCampaign, sVarName, oTarget));
		else if(sType == "location") sValue = NWBash_LocationToString(GetCampaignLocation(sCampaign, sVarName, oTarget));
		else{
			NWBash_Err("'" + sType + "' is not a valid campaign variable type");
			return 3;
		}
		NWBash_Info("["+(GetIsPC(oTarget) ? (GetPCPlayerName(oTarget) + "." + GetBicFileName(oTarget)) : "Module")+"]: " + sCampaign + "." + sVarName + "=" + sValue);
	}
	else if(argsLen == 4){
		// Set value
		if(sType == "int")           SetCampaignInt(sCampaign, sVarName, StringToInt(sValue), oTarget);
		else if(sType == "float")    SetCampaignFloat(sCampaign, sVarName, StringToFloat(sValue), oTarget);
		else if(sType == "string")   SetCampaignString(sCampaign, sVarName, sValue, oTarget);
		else if(sType == "vector")   SetCampaignVector(sCampaign, sVarName, NWBash_StringToVector(sValue), oTarget);
		else if(sType == "location") SetCampaignLocation(sCampaign, sVarName, NWBash_StringToLocation(sValue), oTarget);
		else{
			NWBash_Err("'" + sType + "' is not a valid campaign variable type");
			return 4;
		}
		NWBash_Info("["+(GetIsObjectValid(oTarget) ? GetName(oTarget) : "Module")+"]: <b>Set</b> " + sCampaign + "." + sVarName + "=" + sValue);
	}
	return 0;
}