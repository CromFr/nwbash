// Include this in order to get the NWBASH_CMD_PREFIX_CHAR symbol
#include "nwbash_custom"

int StartingConditional(object oSender, object oTarget, int nChannel, string sMessage)
{
	// You may want to allow the commands to be executed by non-DM players on a testing server
	if(GetIsPC(oSender)
	&& (GetIsSinglePlayer() || GetLocalInt(GetModule(), "bAllowNWBash") || GetIsDM(oSender) || GetIsDMPossessed(oSender))
	&& (GetStringLeft(sMessage, 1) == NWBASH_CMD_PREFIX_CHAR || GetLocalInt(oSender, "nwbash_cmdmode"))){
		ClearScriptParams();
		AddScriptParameterString(sMessage);
		AddScriptParameterInt(nChannel);
		ExecuteScriptEnhanced("nwbash_exec", oSender, TRUE);
		return FALSE;
	}
	return TRUE;
}

