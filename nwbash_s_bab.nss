#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: bab [options] [value]\n"
			+ "Get or set the base attack bonus of a creature\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help  show this help"
		);
		return 0;
	}


	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 1;
	}

	if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE){
		NWBash_Err("Target is not a creature");
		return 2;
	}

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		int nValue = GetBaseAttackBonus(oTarget);
		NWBash_Info("Base attack bonus:  " + IntToString(nValue) + " (on " + NWBash_ObjectToUserString(oTarget) + ")");
	}
	else if(nArgsLen == 1) {
		string sValue = ArrayGetString(args, 0);
		if(!NWBash_IsNumeric(sValue)){
			NWBash_Err("Invalid ability value: '" + sValue + "'");
			return 3;
		}
		int nValue = StringToInt(sValue);

		SetBaseAttackBonus(nValue, oTarget);
		NWBash_Info("Set base attack bonus to  " + IntToString(nValue) + " (on " + NWBash_ObjectToUserString(oTarget) + ")");
	}
	else{
		NWBash_Err("Bad number of arguments. See 'bab --help'");
		return 1;
	}

	return 0;
}
