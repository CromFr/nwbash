# NWBash

NWBash is a basic implementation of
[Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) in NWScript for the
game Neverwinter Nights 2, that allows you to run commands by writing them to
the in-game chat box.

Its goal is to provide a scripting tool for quickly performing actions in
NWN2, that would be complicated or tedious to do using graphical interfaces.
With NWBash you can automate some role-playing events as a DM, or setup
specific conditions for quickly testing a quest / system.

NWBash is best paired with [Skywing Client
Extension](https://neverwintervault.org/project/nwn2/other/nwn2-client-extension),
as it allows you to copy/paste commands directly in the chat box.

NWBash can be used for both single and multi-player modules.


## Features

NWBash supports the following features (with some limitations):
- Comments
- Single / double quotes and character escaping
- Environment variables (see `env`, `export`, `unexport`)
- Command aliases (see `alias`, `unalias`)

NWBash does not support:
- `stdin` / `stdout` / `stderr`
- Piping and stream redirections
- Background tasks (`command &`)
- Command logic (`&&`, `||`)
- Conditional / loop blocks (`while`, `for`, `if`, `case`, ...)
- Brace operations (`{1..10}`, `${VAR:-def}`, ...)
- And many more advanced features... 


# Demo

Download this repository as zip (or clone it with git) and extract the NWBash folder into your `Documents/Neverwinter Nights 2/modules/` directory. Start the game, start a new module and select NWBash. The demo is setup so all players are given access to NWBash commands.

# Installation

1. Copy all the files starting with `nwbash_` into your module directory
2. Edit your module OnChat script (or create it) to add the content from `n2_mod_chat.nss`, more specifically:
    - The `#include "nwbash_custom"` atop of the file
    - The big `if(...){...}` block. On single player modules, the player has access to NWBash commands. In multiplayer only DMs have access NWBash commands (and you can set the local variable bAllowNWBash on the module to 1 to allow unrestricted access to NWBash) 
3. Compile your module scripts.


# In-game scripting

## Examples

### Create a mini cutscene with two creatures

```bash
!create c_zombie --faction=defender --tag=zombie # Create a zombie
!create c_human --tag=victim # Create a victim
!sleep 3 # wait 3 seconds

!TARGET=t:zombie speak "Braiiiiiins..." # Make the zombie speak
!sleep 2

!export TARGET=t:victim # Next commands will target the zombie
!speak "AAAAhhhh !" # Make the victim speak
!sleep 0.5
!kill # kill the victim

!sleep 2
!TARGET=t:zombie speak "Yummy..." # Make the zombie speak

!unexport TARGET # Clear target variable
```

### Reward nearby players

```bash
!speak "Well done adventurers ! *hands over a pouch of gold*" # Make the DM / selected creature speak
!gold 1000 --area # Give 1000 gp to all players in this area
!xp 1000 --area # Give 1000 xp to all players in this area
```

### Give feat, advance quest and jump to the given NPC

```bash
!feat 1337 # Give the feat ID 1337
!cam quests int respawn_stone 10 # Same as SetCampaignInt("quests", "respawn_stone", 10, OBJECT_SELF)
!jump aribeth # Jump to the NPC to test the quest dialog
```

### Execute any script

The `exec` command allows you to execute any script on the current target (defined by the env var TARGET):

```bash
!exec ga_attack_target s:OWNER s:victim i:0 # Make the target attack the object with the tag 'victim'
```


# Easily extensible

You can add custom commands by writing your own scripts: when you execute a nwbash command, it tries to execute the script `nwbash_s_<command>`.

```c
#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChatChannel){
    struct array args = Array(sArgsData);

    // Handle help request
    int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
    if(bNeedHelp){
        NWBash_Info("Your custom command help");
        return 0;
    }

    // Randle command line arguments
    // NWBash_GetStringArg returns values set as -rvalue, -r value, --radius value, --radius=value
    string sRadius = NWBash_GetStringArg(args, "r|radius");
    int bKillPlayers = NWBash_GetBooleanArg(args, "p|players");

    // Remove command-line arguments
    args = NWBash_RemoveStringArgs(args, Array("|r|radius|"));
    args = NWBash_RemoveBooleanArgs(args, Array("|p|players|"));

    int nRemainingArgsLen = ArrayGetLength(args);
    int i;
    for(i = 0 ; i < nRemainingArgsLen ; i++){
    	string sArg = ArrayGetString(args, i);
    	// Do something with the arg?
    }

    // Get the currently script-selected target
    object oTarget = NWBash_GetTarget();

    // Get environment variable
    string sCustomVar = NWBash_GetEnvVar("CUSTOM");


    // do something...


    // Return status code
    // 0: success
    // -1: command not found (you should never return -1)
    // other: custom error integer 
    return 0;
}
```

## Contributing

Writing nwbash commands for every single NWN2 feature is a daunting task, so I included here only some of the most useful commands. If you happen to write new commands that are not specific to your server / module, please reach to me through Discord (Crom#5130) or send a merge request on Gitlab with your code !
