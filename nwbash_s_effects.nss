#include "nwbash_inc"



string EffectTypeToString(int nType){
	switch(nType)
	{
		case EFFECT_TYPE_INVALIDEFFECT:              return "INVALIDEFFECT(" + IntToString(EFFECT_TYPE_INVALIDEFFECT) + ")";
		case EFFECT_TYPE_DAMAGE_RESISTANCE:          return "DAMAGE_RESISTANCE(" + IntToString(EFFECT_TYPE_DAMAGE_RESISTANCE) + ")";
		// case EFFECT_TYPE_ABILITY_BONUS:              return "ABILITY_BONUS(" + IntToString(EFFECT_TYPE_ABILITY_BONUS) + ")";
		case EFFECT_TYPE_REGENERATE:                 return "REGENERATE(" + IntToString(EFFECT_TYPE_REGENERATE) + ")";
		// case EFFECT_TYPE_SAVING_THROW_BONUS:         return "SAVING_THROW_BONUS(" + IntToString(EFFECT_TYPE_SAVING_THROW_BONUS) + ")";
		// case EFFECT_TYPE_MODIFY_AC:                  return "MODIFY_AC(" + IntToString(EFFECT_TYPE_MODIFY_AC) + ")";
		// case EFFECT_TYPE_ATTACK_BONUS:               return "ATTACK_BONUS(" + IntToString(EFFECT_TYPE_ATTACK_BONUS) + ")";
		case EFFECT_TYPE_DAMAGE_REDUCTION:           return "DAMAGE_REDUCTION(" + IntToString(EFFECT_TYPE_DAMAGE_REDUCTION) + ")";
		// case EFFECT_TYPE_DAMAGE_BONUS:               return "DAMAGE_BONUS(" + IntToString(EFFECT_TYPE_DAMAGE_BONUS) + ")";
		case EFFECT_TYPE_TEMPORARY_HITPOINTS:        return "TEMPORARY_HITPOINTS(" + IntToString(EFFECT_TYPE_TEMPORARY_HITPOINTS) + ")";
		// case EFFECT_TYPE_DAMAGE_IMMUNITY:            return "DAMAGE_IMMUNITY(" + IntToString(EFFECT_TYPE_DAMAGE_IMMUNITY) + ")";
		case EFFECT_TYPE_ENTANGLE:                   return "ENTANGLE(" + IntToString(EFFECT_TYPE_ENTANGLE) + ")";
		case EFFECT_TYPE_INVULNERABLE:               return "INVULNERABLE(" + IntToString(EFFECT_TYPE_INVULNERABLE) + ")";
		case EFFECT_TYPE_DEAF:                       return "DEAF(" + IntToString(EFFECT_TYPE_DEAF) + ")";
		case EFFECT_TYPE_RESURRECTION:               return "RESURRECTION(" + IntToString(EFFECT_TYPE_RESURRECTION) + ")";
		case EFFECT_TYPE_IMMUNITY:                   return "IMMUNITY(" + IntToString(EFFECT_TYPE_IMMUNITY) + ")";
		// case EFFECT_TYPE_BLIND:                      return "BLIND(" + IntToString(EFFECT_TYPE_BLIND) + ")";
		case EFFECT_TYPE_ENEMY_ATTACK_BONUS:         return "ENEMY_ATTACK_BONUS(" + IntToString(EFFECT_TYPE_ENEMY_ATTACK_BONUS) + ")";
		case EFFECT_TYPE_ARCANE_SPELL_FAILURE:       return "ARCANE_SPELL_FAILURE(" + IntToString(EFFECT_TYPE_ARCANE_SPELL_FAILURE) + ")";
		// case EFFECT_TYPE_MOVEMENT_SPEED:             return "MOVEMENT_SPEED(" + IntToString(EFFECT_TYPE_MOVEMENT_SPEED) + ")";
		case EFFECT_TYPE_AREA_OF_EFFECT:             return "AREA_OF_EFFECT(" + IntToString(EFFECT_TYPE_AREA_OF_EFFECT) + ")";
		case EFFECT_TYPE_BEAM:                       return "BEAM(" + IntToString(EFFECT_TYPE_BEAM) + ")";
		// case EFFECT_TYPE_SPELL_RESISTANCE:           return "SPELL_RESISTANCE(" + IntToString(EFFECT_TYPE_SPELL_RESISTANCE) + ")";
		case EFFECT_TYPE_CHARMED:                    return "CHARMED(" + IntToString(EFFECT_TYPE_CHARMED) + ")";
		case EFFECT_TYPE_CONFUSED:                   return "CONFUSED(" + IntToString(EFFECT_TYPE_CONFUSED) + ")";
		case EFFECT_TYPE_FRIGHTENED:                 return "FRIGHTENED(" + IntToString(EFFECT_TYPE_FRIGHTENED) + ")";
		case EFFECT_TYPE_DOMINATED:                  return "DOMINATED(" + IntToString(EFFECT_TYPE_DOMINATED) + ")";
		case EFFECT_TYPE_PARALYZE:                   return "PARALYZE(" + IntToString(EFFECT_TYPE_PARALYZE) + ")";
		case EFFECT_TYPE_DAZED:                      return "DAZED(" + IntToString(EFFECT_TYPE_DAZED) + ")";
		case EFFECT_TYPE_STUNNED:                    return "STUNNED(" + IntToString(EFFECT_TYPE_STUNNED) + ")";
		case EFFECT_TYPE_SLEEP:                      return "SLEEP(" + IntToString(EFFECT_TYPE_SLEEP) + ")";
		case EFFECT_TYPE_POISON:                     return "POISON(" + IntToString(EFFECT_TYPE_POISON) + ")";
		case EFFECT_TYPE_DISEASE:                    return "DISEASE(" + IntToString(EFFECT_TYPE_DISEASE) + ")";
		case EFFECT_TYPE_CURSE:                      return "CURSE(" + IntToString(EFFECT_TYPE_CURSE) + ")";
		case EFFECT_TYPE_SILENCE:                    return "SILENCE(" + IntToString(EFFECT_TYPE_SILENCE) + ")";
		case EFFECT_TYPE_TURNED:                     return "TURNED(" + IntToString(EFFECT_TYPE_TURNED) + ")";
		case EFFECT_TYPE_HASTE:                      return "HASTE(" + IntToString(EFFECT_TYPE_HASTE) + ")";
		case EFFECT_TYPE_SLOW:                       return "SLOW(" + IntToString(EFFECT_TYPE_SLOW) + ")";
		case EFFECT_TYPE_ABILITY_INCREASE:           return "ABILITY_INCREASE(" + IntToString(EFFECT_TYPE_ABILITY_INCREASE) + ")";
		case EFFECT_TYPE_ABILITY_DECREASE:           return "ABILITY_DECREASE(" + IntToString(EFFECT_TYPE_ABILITY_DECREASE) + ")";
		case EFFECT_TYPE_ATTACK_INCREASE:            return "ATTACK_INCREASE(" + IntToString(EFFECT_TYPE_ATTACK_INCREASE) + ")";
		case EFFECT_TYPE_ATTACK_DECREASE:            return "ATTACK_DECREASE(" + IntToString(EFFECT_TYPE_ATTACK_DECREASE) + ")";
		case EFFECT_TYPE_DAMAGE_INCREASE:            return "DAMAGE_INCREASE(" + IntToString(EFFECT_TYPE_DAMAGE_INCREASE) + ")";
		case EFFECT_TYPE_DAMAGE_DECREASE:            return "DAMAGE_DECREASE(" + IntToString(EFFECT_TYPE_DAMAGE_DECREASE) + ")";
		case EFFECT_TYPE_DAMAGE_IMMUNITY_INCREASE:   return "DAMAGE_IMMUNITY_INCREASE(" + IntToString(EFFECT_TYPE_DAMAGE_IMMUNITY_INCREASE) + ")";
		case EFFECT_TYPE_DAMAGE_IMMUNITY_DECREASE:   return "DAMAGE_IMMUNITY_DECREASE(" + IntToString(EFFECT_TYPE_DAMAGE_IMMUNITY_DECREASE) + ")";
		case EFFECT_TYPE_AC_INCREASE:                return "AC_INCREASE(" + IntToString(EFFECT_TYPE_AC_INCREASE) + ")";
		case EFFECT_TYPE_AC_DECREASE:                return "AC_DECREASE(" + IntToString(EFFECT_TYPE_AC_DECREASE) + ")";
		case EFFECT_TYPE_MOVEMENT_SPEED_INCREASE:    return "MOVEMENT_SPEED_INCREASE(" + IntToString(EFFECT_TYPE_MOVEMENT_SPEED_INCREASE) + ")";
		case EFFECT_TYPE_MOVEMENT_SPEED_DECREASE:    return "MOVEMENT_SPEED_DECREASE(" + IntToString(EFFECT_TYPE_MOVEMENT_SPEED_DECREASE) + ")";
		case EFFECT_TYPE_SAVING_THROW_INCREASE:      return "SAVING_THROW_INCREASE(" + IntToString(EFFECT_TYPE_SAVING_THROW_INCREASE) + ")";
		case EFFECT_TYPE_SAVING_THROW_DECREASE:      return "SAVING_THROW_DECREASE(" + IntToString(EFFECT_TYPE_SAVING_THROW_DECREASE) + ")";
		case EFFECT_TYPE_SPELL_RESISTANCE_INCREASE:  return "SPELL_RESISTANCE_INCREASE(" + IntToString(EFFECT_TYPE_SPELL_RESISTANCE_INCREASE) + ")";
		case EFFECT_TYPE_SPELL_RESISTANCE_DECREASE:  return "SPELL_RESISTANCE_DECREASE(" + IntToString(EFFECT_TYPE_SPELL_RESISTANCE_DECREASE) + ")";
		case EFFECT_TYPE_SKILL_INCREASE:             return "SKILL_INCREASE(" + IntToString(EFFECT_TYPE_SKILL_INCREASE) + ")";
		case EFFECT_TYPE_SKILL_DECREASE:             return "SKILL_DECREASE(" + IntToString(EFFECT_TYPE_SKILL_DECREASE) + ")";
		case EFFECT_TYPE_INVISIBILITY:               return "INVISIBILITY(" + IntToString(EFFECT_TYPE_INVISIBILITY) + ")";
		case EFFECT_TYPE_GREATERINVISIBILITY:        return "GREATERINVISIBILITY(" + IntToString(EFFECT_TYPE_GREATERINVISIBILITY) + ")";
		case EFFECT_TYPE_DARKNESS:                   return "DARKNESS(" + IntToString(EFFECT_TYPE_DARKNESS) + ")";
		case EFFECT_TYPE_DISPELMAGICALL:             return "DISPELMAGICALL(" + IntToString(EFFECT_TYPE_DISPELMAGICALL) + ")";
		case EFFECT_TYPE_ELEMENTALSHIELD:            return "ELEMENTALSHIELD(" + IntToString(EFFECT_TYPE_ELEMENTALSHIELD) + ")";
		case EFFECT_TYPE_NEGATIVELEVEL:              return "NEGATIVELEVEL(" + IntToString(EFFECT_TYPE_NEGATIVELEVEL) + ")";
		case EFFECT_TYPE_POLYMORPH:                  return "POLYMORPH(" + IntToString(EFFECT_TYPE_POLYMORPH) + ")";
		case EFFECT_TYPE_SANCTUARY:                  return "SANCTUARY(" + IntToString(EFFECT_TYPE_SANCTUARY) + ")";
		case EFFECT_TYPE_TRUESEEING:                 return "TRUESEEING(" + IntToString(EFFECT_TYPE_TRUESEEING) + ")";
		case EFFECT_TYPE_SEEINVISIBLE:               return "SEEINVISIBLE(" + IntToString(EFFECT_TYPE_SEEINVISIBLE) + ")";
		case EFFECT_TYPE_TIMESTOP:                   return "TIMESTOP(" + IntToString(EFFECT_TYPE_TIMESTOP) + ")";
		case EFFECT_TYPE_BLINDNESS:                  return "BLINDNESS(" + IntToString(EFFECT_TYPE_BLINDNESS) + ")";
		case EFFECT_TYPE_SPELLLEVELABSORPTION:       return "SPELLLEVELABSORPTION(" + IntToString(EFFECT_TYPE_SPELLLEVELABSORPTION) + ")";
		case EFFECT_TYPE_DISPELMAGICBEST:            return "DISPELMAGICBEST(" + IntToString(EFFECT_TYPE_DISPELMAGICBEST) + ")";
		case EFFECT_TYPE_ULTRAVISION:                return "ULTRAVISION(" + IntToString(EFFECT_TYPE_ULTRAVISION) + ")";
		case EFFECT_TYPE_MISS_CHANCE:                return "MISS_CHANCE(" + IntToString(EFFECT_TYPE_MISS_CHANCE) + ")";
		case EFFECT_TYPE_CONCEALMENT:                return "CONCEALMENT(" + IntToString(EFFECT_TYPE_CONCEALMENT) + ")";
		case EFFECT_TYPE_SPELL_IMMUNITY:             return "SPELL_IMMUNITY(" + IntToString(EFFECT_TYPE_SPELL_IMMUNITY) + ")";
		case EFFECT_TYPE_VISUALEFFECT:               return "VISUALEFFECT(" + IntToString(EFFECT_TYPE_VISUALEFFECT) + ")";
		case EFFECT_TYPE_DISAPPEARAPPEAR:            return "DISAPPEARAPPEAR(" + IntToString(EFFECT_TYPE_DISAPPEARAPPEAR) + ")";
		case EFFECT_TYPE_SWARM:                      return "SWARM(" + IntToString(EFFECT_TYPE_SWARM) + ")";
		case EFFECT_TYPE_TURN_RESISTANCE_DECREASE:   return "TURN_RESISTANCE_DECREASE(" + IntToString(EFFECT_TYPE_TURN_RESISTANCE_DECREASE) + ")";
		case EFFECT_TYPE_TURN_RESISTANCE_INCREASE:   return "TURN_RESISTANCE_INCREASE(" + IntToString(EFFECT_TYPE_TURN_RESISTANCE_INCREASE) + ")";
		case EFFECT_TYPE_PETRIFY:                    return "PETRIFY(" + IntToString(EFFECT_TYPE_PETRIFY) + ")";
		case EFFECT_TYPE_CUTSCENE_PARALYZE:          return "CUTSCENE_PARALYZE(" + IntToString(EFFECT_TYPE_CUTSCENE_PARALYZE) + ")";
		case EFFECT_TYPE_ETHEREAL:                   return "ETHEREAL(" + IntToString(EFFECT_TYPE_ETHEREAL) + ")";
		case EFFECT_TYPE_SPELL_FAILURE:              return "SPELL_FAILURE(" + IntToString(EFFECT_TYPE_SPELL_FAILURE) + ")";
		case EFFECT_TYPE_CUTSCENEGHOST:              return "CUTSCENEGHOST(" + IntToString(EFFECT_TYPE_CUTSCENEGHOST) + ")";
		case EFFECT_TYPE_CUTSCENEIMMOBILIZE:         return "CUTSCENEIMMOBILIZE(" + IntToString(EFFECT_TYPE_CUTSCENEIMMOBILIZE) + ")";
		case EFFECT_TYPE_BARDSONG_SINGING:           return "BARDSONG_SINGING(" + IntToString(EFFECT_TYPE_BARDSONG_SINGING) + ")";
		case EFFECT_TYPE_HIDEOUS_BLOW:               return "HIDEOUS_BLOW(" + IntToString(EFFECT_TYPE_HIDEOUS_BLOW) + ")";
		case EFFECT_TYPE_NWN2_DEX_ACMOD_DISABLE:     return "NWN2_DEX_ACMOD_DISABLE(" + IntToString(EFFECT_TYPE_NWN2_DEX_ACMOD_DISABLE) + ")";
		case EFFECT_TYPE_DETECTUNDEAD:               return "DETECTUNDEAD(" + IntToString(EFFECT_TYPE_DETECTUNDEAD) + ")";
		case EFFECT_TYPE_SHAREDDAMAGE:               return "SHAREDDAMAGE(" + IntToString(EFFECT_TYPE_SHAREDDAMAGE) + ")";
		case EFFECT_TYPE_ASSAYRESISTANCE:            return "ASSAYRESISTANCE(" + IntToString(EFFECT_TYPE_ASSAYRESISTANCE) + ")";
		case EFFECT_TYPE_DAMAGEOVERTIME:             return "DAMAGEOVERTIME(" + IntToString(EFFECT_TYPE_DAMAGEOVERTIME) + ")";
		case EFFECT_TYPE_ABSORBDAMAGE:               return "ABSORBDAMAGE(" + IntToString(EFFECT_TYPE_ABSORBDAMAGE) + ")";
		case EFFECT_TYPE_AMORPENALTYINC:             return "AMORPENALTYINC(" + IntToString(EFFECT_TYPE_AMORPENALTYINC) + ")";
		case EFFECT_TYPE_DISINTEGRATE:               return "DISINTEGRATE(" + IntToString(EFFECT_TYPE_DISINTEGRATE) + ")";
		case EFFECT_TYPE_HEAL_ON_ZERO_HP:            return "HEAL_ON_ZERO_HP(" + IntToString(EFFECT_TYPE_HEAL_ON_ZERO_HP) + ")";
		case EFFECT_TYPE_BREAK_ENCHANTMENT:          return "BREAK_ENCHANTMENT(" + IntToString(EFFECT_TYPE_BREAK_ENCHANTMENT) + ")";
		case EFFECT_TYPE_MESMERIZE:                  return "MESMERIZE(" + IntToString(EFFECT_TYPE_MESMERIZE) + ")";
		case EFFECT_TYPE_ON_DISPEL:                  return "ON_DISPEL(" + IntToString(EFFECT_TYPE_ON_DISPEL) + ")";
		case EFFECT_TYPE_BONUS_HITPOINTS:            return "BONUS_HITPOINTS(" + IntToString(EFFECT_TYPE_BONUS_HITPOINTS) + ")";
		case EFFECT_TYPE_JARRING:                    return "JARRING(" + IntToString(EFFECT_TYPE_JARRING) + ")";
		case EFFECT_TYPE_MAX_DAMAGE:                 return "MAX_DAMAGE(" + IntToString(EFFECT_TYPE_MAX_DAMAGE) + ")";
		case EFFECT_TYPE_WOUNDING:                   return "WOUNDING(" + IntToString(EFFECT_TYPE_WOUNDING) + ")";
		case EFFECT_TYPE_WILDSHAPE:                  return "WILDSHAPE(" + IntToString(EFFECT_TYPE_WILDSHAPE) + ")";
		case EFFECT_TYPE_EFFECT_ICON:                return "EFFECT_ICON(" + IntToString(EFFECT_TYPE_EFFECT_ICON) + ")";
		case EFFECT_TYPE_RESCUE:                     return "RESCUE(" + IntToString(EFFECT_TYPE_RESCUE) + ")";
		case EFFECT_TYPE_DETECT_SPIRITS:             return "DETECT_SPIRITS(" + IntToString(EFFECT_TYPE_DETECT_SPIRITS) + ")";
		case EFFECT_TYPE_DAMAGE_REDUCTION_NEGATED:   return "DAMAGE_REDUCTION_NEGATED(" + IntToString(EFFECT_TYPE_DAMAGE_REDUCTION_NEGATED) + ")";
		case EFFECT_TYPE_CONCEALMENT_NEGATED:        return "CONCEALMENT_NEGATED(" + IntToString(EFFECT_TYPE_CONCEALMENT_NEGATED) + ")";
		case EFFECT_TYPE_INSANE:                     return "INSANE(" + IntToString(EFFECT_TYPE_INSANE) + ")";
		case EFFECT_TYPE_HITPOINT_CHANGE_WHEN_DYING: return "HITPOINT_CHANGE_WHEN_DYING(" + IntToString(EFFECT_TYPE_HITPOINT_CHANGE_WHEN_DYING) + ")";
	}
	return "UNKNOWN(" + IntToString(nType) + ")";
}
string DurationTypeToString(int nType){
	switch(nType)
	{
		case DURATION_TYPE_INSTANT: return "INSTANT";
		case DURATION_TYPE_TEMPORARY: return "TEMPORARY";
		case DURATION_TYPE_PERMANENT: return "PERMANENT";
	}
	return "INVALID";
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			  "Usage: effects [options]\n"
			+ "Prints all effects on the target.\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help        Show this help\n"
		);
		return 0;
	}

	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));
	// args = NWBash_RemoveStringArgs(args, Array("|"));

	object oTarget = NWBash_GetTarget();

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen != 0 && nArgsLen != 3){
		NWBash_Err("Bad number of arguments. See 'effects --help'");
		return 1;
	}

	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 3;
	}

	int i = 0;
	effect e = GetFirstEffect(oTarget);
	while(GetIsEffectValid(e))
	{
		NWBash_Info(
			IntToString(i++) + ":"
			+ " type=" + EffectTypeToString(GetEffectType(e))
			+ " subtype=" + IntToString(GetEffectSubType(e))
			+ " duration=" + DurationTypeToString(GetEffectDurationType(e))
			+ " creator=" + NWBash_ObjectToUserString(GetEffectCreator(e))
			+ " spellID=" + IntToString(GetEffectSpellId(e))
			+ " int=" + IntToString(GetEffectInteger(e, 0)) + "," + IntToString(GetEffectInteger(e, 1)) + "," + IntToString(GetEffectInteger(e, 2)) + "," + IntToString(GetEffectInteger(e, 3)) + "," + IntToString(GetEffectInteger(e, 4))
		);
		e = GetNextEffect(oTarget);
	}

	return 0;
}
