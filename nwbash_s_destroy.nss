#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: destroy [options]\n"
			+ "Options can be:\n"
			+ "--force     Destroy immortals and plot creatures\n"
			+ "-h, --help  Show this help");
		return 0;
	}
	int bForce = NWBash_GetBooleanArg(args, "f|force");

	int argsLen = ArrayGetLength(args);
	if(argsLen != 0){
		NWBash_Err("Bad number of arguments / unknown options. See destroy --help");
		return 1;
	}

	object oTarget = NWBash_GetTarget();


	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 2;
	}

	if(bForce)
		AssignCommand(oTarget, SetIsDestroyable(TRUE));

	DestroyObject(oTarget);
	NWBash_Info("Destroyed " + NWBash_ObjectToUserString(oTarget));
	return 0;
}


