#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: global [options] <var_type> <var_name> [value]\n"
			+ "If the value is provided, will set the global variable value.\n"
			+ "var_type: can be 'int', 'float', 'string', 'bool'\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-e, --export=VAR  Sets an environment variable with the value of the variable\n"
			+ "\n"
			+ "Examples:\n"
			+ "global int myvar      # Get current value of myvar\n"
			+ "global int myvar 5    # Set myvar=5"
		);
		return 0;
	}
	string sVar = NWBash_GetStringArg(args, "e|export");
	args = NWBash_RemoveStringArgs(args, Array("|e|export|"));
	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen < 2 || nArgsLen > 3){
		NWBash_Err("Bad number of arguments. See global --help");
		return 1;
	}

	if(nArgsLen == 2){
		// Get / delete value
		string sType = GetStringLowerCase(ArrayGetString(args, 0));
		string sName = ArrayGetString(args, 1);

		string sValue;
		if(sType == "int")
			sValue = IntToString(GetGlobalInt(sName));
		else if(sType == "float")
			sValue = FloatToString(GetGlobalFloat(sName), 0, 6);
		else if(sType == "string")
			sValue = GetGlobalString(sName);
		else if(sType == "bool")
			sValue = IntToString(GetGlobalBool(sName));
		else{
			NWBash_Err("Invalid variable type '" + sType + "'");
			return 3;
		}
		NWBash_Info("Get " + sType + " " + sName + " = " + sValue);
		if(sVar != "")
			NWBash_SetEnvVar(sVar, sValue);
	}
	else{
		// Set value
		string sType = GetStringLowerCase(ArrayGetString(args, 0));
		string sName = ArrayGetString(args, 1);
		string sValue = ArrayGetString(args, 2);
		if(sType == "int"){
			if(!NWBash_IsNumeric(sValue, FALSE)){
				NWBash_Err("Value '" + sValue + "' is not an int");
				return 3;
			}
			SetGlobalInt(sName, StringToInt(sValue));
			sValue = IntToString(GetGlobalInt(sName));
		}
		else if(sType == "float"){
			if(!NWBash_IsNumeric(sValue, TRUE)){
				NWBash_Err("Value '" + sValue + "' is not a float");
				return 3;
			}
			SetGlobalFloat(sName, StringToFloat(sValue));
			sValue = FloatToString(GetGlobalFloat(sName), 0, 6);
		}
		else if(sType == "string"){
			SetGlobalString(sName, sValue);
			sValue = GetGlobalString(sName);
		}
		else if(sType == "bool"){
			SetGlobalBool(sName, NWBash_StringToBool(sValue));
			sValue = IntToString(GetGlobalBool(sName));
		}
		else{
			NWBash_Err("Invalid variable type '" + sType + "'");
			return 3;
		}

		NWBash_Info("Set " + sType + " " + sName + " = " + sValue);

		if(sVar != "")
			NWBash_SetEnvVar(sVar, sValue);
	}
	return 0;
}