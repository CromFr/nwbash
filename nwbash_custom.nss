// Include here module-specific stuff


// Prefix for triggering commands
const string NWBASH_CMD_PREFIX_CHAR = "!";

// Edit this function to add your custom commands list (shown when executing !help)
string NWBash_HelpCommandList(){
	return ""
		+ "Generic:\n"
		+ "- <b>create</b>     Spawn an object (creature or other)\n"
		+ "- <b>destroy</b>    Destroy the target\n"
		+ "- <b>export-obj</b> Get a specific object relative to the target, and store its value in an environment variable\n"
		+ "- <b>flag</b>       Set object flags (plot, cursed, immortal, locked, trapped, ...)\n"
		+ "- <b>scriptset</b>  Get or set callback scripts on any object\n"
		+ "\n"
		+ "Creature-related:\n"
		+ "- <b>anim</b>       Make the target play a custom animation\n"
		+ "- <b>dmg</b>        Inflict damage to the target\n"
		+ "- <b>faction</b>    Change the target creature's faction\n"
		+ "- <b>feat</b>       Give / remove a feat on creature\n"
		+ "- <b>gold</b>       Give / remove gold to a creature\n"
		+ "- <b>kill</b>       Kill target creature or in a radius\n"
		+ "- <b>move</b>       Move or jump the target to a location\n"
		+ "- <b>sound</b>      Make the target play a sound\n"
		+ "- <b>speak</b>      Make a creature / object speak\n"
		+ "- <b>xp</b>         Give / remove XP to a creature or a group of players\n"
		+ "- <b>level</b>      Give / remove levels to a creature\n"
		+ "- <b>bab</b>        Get / set base attack bonus\n"
		+ "- <b>ability</b>    Get / set base ability scores\n"
		+ "- <b>skill</b>      Get / set skill ranks\n"
		+ "\n"
		+ "Item-related:\n"
		+ "- <b>itemproperty</b> List, add or remove item properties\n"
		+ "\n"
		+ "Effects:\n"
		+ "- <b>vfx</b>        Apply a visual effect on the target or at a given location\n"
		+ "- <b>effects</b>    List all effect on an object\n"
		+ "- <b>rmeffects</b>  Remove all effects on an object\n"
		+ "- <b>scale</b>      Change the scale of an object\n"
		+ "- <b>music</b>      Manage area musics and ambient sounds\n"
		+ "- <b>weather</b>    Get / set weather conditions\n"
		+ "\n"
		+ "Other:\n"
		+ "- <b>lock</b>       Change placeable and door lock status\n"
		+ "- <b>trap</b>       Change placeable and door trap status\n"
		+ "\n"
		+ "Variables:\n"
		+ "- <b>cam</b>        View / modify local variables on the target\n"
		+ "- <b>locvar</b>     View / modify local variables\n"
	;
}
