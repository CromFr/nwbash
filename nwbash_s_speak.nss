
#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChatChannel){
	struct array args = Array(sArgsData);
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info("Makes the target creature / object speak some text\n"
			+"Usage: speak <text>\n"
		);
		return 0;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 1;
	}

	if(nChatChannel == TALKVOLUME_WHISPER || nChatChannel == TALKVOLUME_SILENT_TALK)
		nChatChannel = TALKVOLUME_TALK;

	string sText;
	int nArgsLen = ArrayGetLength(args);
	int i;
	for(i = 0 ; i < nArgsLen ; i++){
		sText += (i > 0 ? " " : "") + ArrayGetString(args, i);
	}

	AssignCommand(oTarget, SpeakString(sText, nChatChannel));
	return 0;
}
