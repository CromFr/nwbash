#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: locvar [options] [<var_type> <var_name> [value]]\n"
			+ "If no argument is provided, will list all local variables on the target.\n"
			+ "If the value is provided, will set the local variable value.\n"
			+ "var_type: can be 'int', 'float', 'string', 'object', 'location'\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-d, --delete      Delete the local variable (just specify <var_type> and <var_name>)\n"
			+ "-e, --export=VAR  Sets an environment variable with the value of the variable\n"
			+ "\n"
			+ "Examples:\n"
			+ "locvar                # Print all local variables\n"
			+ "locvar int myvar      # Get current value of myvar\n"
			+ "locvar int myvar 5    # Set myvar=5"
		);
		return 0;
	}
	int bDelete = NWBash_GetBooleanArg(args, "d|delete");
	string sVar = NWBash_GetStringArg(args, "e|export");
	args = NWBash_RemoveBooleanArgs(args, Array("|d|delete|"));
	args = NWBash_RemoveStringArgs(args, Array("|e|export|"));
	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen != 0 && nArgsLen != 2 && nArgsLen != 3){
		NWBash_Err("Bad number of arguments. See locvar --help");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 2;
	}


	if(nArgsLen == 0){
		// Print all local vars
		int nVarCnt = GetVariableCount(oTarget);
		int i;
		for(i = 0 ; i < nVarCnt ; i++){
			string sValue;
			string sType;
			switch(GetVariableType(oTarget, i)){
				case VARIABLE_TYPE_INT:      sType = "int";      sValue = IntToString(GetVariableValueInt(oTarget, i)); break;
				case VARIABLE_TYPE_FLOAT:    sType = "float";    sValue = FloatToString(GetVariableValueFloat(oTarget, i), 0, 3); break;
				case VARIABLE_TYPE_STRING:   sType = "string";   sValue = GetVariableValueString(oTarget, i); break;
				case VARIABLE_TYPE_DWORD:    sType = "object";   sValue = NWBash_ObjectToString(GetVariableValueObject(oTarget, i)) + " (" + GetName(GetVariableValueObject(oTarget, i)) + ")"; break;
				case VARIABLE_TYPE_LOCATION: sType = "location"; sValue = NWBash_LocationToString(GetVariableValueLocation(oTarget, i)); break;
			}
			NWBash_Info(IntToString(i) + ": " + sType + " " + GetVariableName(oTarget, i) + " = " + sValue);
		}
	}
	else if(nArgsLen == 2){
		// Get / delete value
		string sType = GetStringLowerCase(ArrayGetString(args, 0));
		string sName = ArrayGetString(args, 1);

		if(bDelete){
			if(sType == "int")
				DeleteLocalInt(oTarget, sName);
			else if(sType == "float")
				DeleteLocalFloat(oTarget, sName);
			else if(sType == "string")
				DeleteLocalString(oTarget, sName);
			else if(sType == "object")
				DeleteLocalObject(oTarget, sName);
			else if(sType == "location")
				DeleteLocalLocation(oTarget, sName);
			else{
				return 3;
			}
			NWBash_Info("Delete " + sType + " " + sName);
		}
		else{
			string sValue;
			if(sType == "int")
				sValue = IntToString(GetLocalInt(oTarget, sName));
			else if(sType == "float")
				sValue = FloatToString(GetLocalFloat(oTarget, sName), 0, 6);
			else if(sType == "string")
				sValue = GetLocalString(oTarget, sName);
			else if(sType == "object")
				sValue = NWBash_ObjectToString(GetLocalObject(oTarget, sName)) + " (" + GetName(GetLocalObject(oTarget, sName)) + ")";
			else if(sType == "location")
				sValue = NWBash_LocationToString(GetLocalLocation(oTarget, sName));
			else{
				NWBash_Err("Invalid variable type '" + sType + "'");
				return 3;
			}
			NWBash_Info("Get " + sType + " " + sName + " = " + sValue);
			if(sVar != "")
				NWBash_SetEnvVar(sVar, sValue);
		}
	}
	else if(nArgsLen == 3){
		// Set value
		string sType = GetStringLowerCase(ArrayGetString(args, 0));
		string sName = ArrayGetString(args, 1);
		string sValue = ArrayGetString(args, 2);
		if(sType == "int"){
			if(!NWBash_IsNumeric(sValue, FALSE)){
				NWBash_Err("Value '" + sValue + "' is not an int");
				return 3;
			}
			SetLocalInt(oTarget, sName, StringToInt(sValue));
			sValue = IntToString(GetLocalInt(oTarget, sName));
		}
		else if(sType == "float"){
			if(!NWBash_IsNumeric(sValue, TRUE)){
				NWBash_Err("Value '" + sValue + "' is not a float");
				return 3;
			}
			SetLocalFloat(oTarget, sName, StringToFloat(sValue));
			sValue = FloatToString(GetLocalFloat(oTarget, sName), 0, 6);
		}
		else if(sType == "string"){
			SetLocalString(oTarget, sName, sValue);
			sValue = GetLocalString(oTarget, sName);
		}
		else if(sType == "object"){
			SetLocalObject(oTarget, sName, NWBash_ResolveObjectString(sName));
			sValue = NWBash_ObjectToString(GetLocalObject(oTarget, sName));
		}
		else if(sType == "location"){
			SetLocalLocation(oTarget, sName, NWBash_ResolveLocationString(sValue));
			sValue = NWBash_LocationToString(GetLocalLocation(oTarget, sName));
		}
		else{
			NWBash_Err("Invalid variable type '" + sType + "'");
			return 3;
		}

		if(sType == "object")
			NWBash_Info("Set " + sType + " " + sName + " = " + sValue + " (" + GetName(GetLocalObject(oTarget, sName)) + ")");
		else
			NWBash_Info("Set " + sType + " " + sName + " = " + sValue);

		if(sVar != "")
			NWBash_SetEnvVar(sVar, sValue);
	}
	return 0;
}