#include "nwbash_inc"

void ResetAnim(object oTarget){
	PlayCustomAnimation(oTarget, "%", FALSE);
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: sound [options] <sound_name>\n"
			+ "Make the target play a sound by name\n"
			+ "Options can be:\n"
			+ "--2d        Play sound as 2D\n"
			+ "-h, --help  Show this help\n"
		);
		return 0;
	}
	int b2D = NWBash_GetBooleanArg(args, "2d");
	args = NWBash_RemoveBooleanArgs(args, Array("|2d|"));


	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen > 1){
		NWBash_Err("Bad number of arguments. See 'sound --help'");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 2;
	}

	string sSound = ArrayGetString(args, 0);
	AssignCommand(oTarget, PlaySound(sSound, b2D));
	NWBash_Info("Playing sound " + sSound + " on " + NWBash_ObjectToUserString(oTarget));
	return 0;
}