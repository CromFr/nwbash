#include "nwbash_inc"

int GetTrackByIDName(string sID){
	int nID;
	if(NWBash_IsNumeric(sID)){
		nID = StringToInt(sID);

		if(nID < 0){
			NWBash_Err("Invalid music ID '" + sID + "'");
			return -1;
		}
	}
	else{
		sID = GetStringLowerCase(sID);

		int nRows = GetNum2DARows("ambientmusic");
		int i;
		for(i = 0 ; i < nRows ; i++)
		{
			if(GetStringLowerCase(Get2DAString("ambientmusic", "Resource", i)) == sID){
				nID = i;
				break;
			}
		}
		if(nID < 0){
			NWBash_Err("No music found with the name '" + sID + "'");
			return -1;
		}
	}

	return nID;
}

int GetAmbientByIDName(string sID){
	int nID;
	if(NWBash_IsNumeric(sID)){
		nID = StringToInt(sID);

		if(nID < 0){
			NWBash_Err("Invalid ambient sound ID '" + sID + "'");
			return -1;
		}
	}
	else{
		sID = GetStringLowerCase(sID);

		int nRows = GetNum2DARows("ambientsound");
		int i;
		for(i = 0 ; i < nRows ; i++)
		{
			if(GetStringLowerCase(Get2DAString("ambientsound", "Resource", i)) == sID){
				nID = i;
				break;
			}
		}
		if(nID < 0){
			NWBash_Err("No ambient sound found with the name '" + sID + "'");
			return -1;
		}
	}

	return nID;
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen < 1){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music <subcommand> [options]\n"
				+ "Manage area background and battle musics and ambient sounds\n"
				+ "\n"
				+ "subcommand:\n"
				+ "get            Print currently configured musics\n"
				+ "bg-play        Play the background music\n"
				+ "bg-stop        Stop / pause the background music\n"
				+ "bg-delay       Set the delay between loops of the background music\n"
				+ "bg-day         Set the day background music\n"
				+ "bg-night       Set the night background music\n"
				+ "\n"
				+ "bat-play       Play the battle music\n"
				+ "bat-stop       Stop the battle music\n"
				+ "bat-set        Set the battle music track\n"
				+ "\n"
				+ "amb-play       Play ambient sounds\n"
				+ "amb-stop       Stop ambient sounds\n"
				+ "amb-day        Set the day ambient sounds\n"
				+ "amb-night      Set the night ambient sounds\n"
				+ "amb-day-vol    Set the day ambient sounds volume\n"
				+ "amb-night-vol  Set the night ambient sounds volume\n"
				+ "\n"
				+ "options:\n"
				+ "-h, --help     Display help text for a subcommand\n"
			);
			return 0;
		}

		NWBash_Err("No subcommand provided");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 1;
	}
	oTarget = GetArea(oTarget);
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target area");
		return 2;
	}


	string sSubcommand = ArrayGetString(args, 0);
	args = ArrayPopFront(args);
	if(sSubcommand == "get"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music get"
			);
			return 0;
		}

		int nMusBgDay = MusicBackgroundGetDayTrack(oTarget);
		int nMusBgNight = MusicBackgroundGetNightTrack(oTarget);
		int nMusBat = MusicBackgroundGetBattleTrack(oTarget);

		NWBash_Info(
			"Current musics for area " + NWBash_ObjectToUserString(oTarget) + "\n"
			+ "  Background - Day: " + IntToString(nMusBgDay) + " / '" + Get2DAString("ambientmusic", "Resource", nMusBgDay) + "'\n"
			+ "  Background - Night: " + IntToString(nMusBgNight) + " / '" + Get2DAString("ambientmusic", "Resource", nMusBgNight) + "'\n"
			+ "  Battle: " + IntToString(nMusBat) + " / '" + Get2DAString("ambientmusic", "Resource", nMusBat) + "'\n"
		);
	}
	else if(sSubcommand == "bg-play"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music bg-play"
			);
			return 0;
		}
		MusicBackgroundPlay(oTarget);
		NWBash_Info("Play background music in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "bg-stop"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music bg-stop"
			);
			return 0;
		}
		MusicBackgroundStop(oTarget);
		NWBash_Info("Stop background music in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "bg-delay"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music bg-delay <delay_ms>\n"
				+ "delay_ms: delay between two music loops in milliseconds"
			);
			return 0;
		}
		int nDelay = ArrayGetInt(args, 0);
		MusicBackgroundSetDelay(oTarget, nDelay);
		NWBash_Info("Set background music delay to " + IntToString(nDelay) + " seconds, in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "bg-day"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music bg-day <track_id>\n"
				+ "track_id: music track ID or Resource as defined in ambientmusic.2da"
			);
			return 0;
		}

		int nTrack = GetTrackByIDName(ArrayGetString(args, 0));
		if(nTrack < 0)
			return 10;

		MusicBackgroundChangeDay(oTarget, nTrack);
		NWBash_Info("Set background day music to " + IntToString(nTrack) + " / '" + Get2DAString("ambientmusic", "Resource", nTrack) + "' in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "bg-night"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music bg-night <track_id>\n"
				+ "track_id: music track ID or Resource as defined in ambientmusic.2da"
			);
			return 0;
		}

		int nTrack = GetTrackByIDName(ArrayGetString(args, 0));
		if(nTrack < 0)
			return 10;

		MusicBackgroundChangeNight(oTarget, nTrack);
		NWBash_Info("Set background night music to " + IntToString(nTrack) + " / '" + Get2DAString("ambientmusic", "Resource", nTrack) + "' in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "bat-play"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music bat-play"
			);
			return 0;
		}
		MusicBattlePlay(oTarget);
		NWBash_Info("Play battle music in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "bat-stop"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music bat-stop"
			);
			return 0;
		}
		MusicBattleStop(oTarget);
		NWBash_Info("Stop battle music in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "bat-set"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music bat-set <track_id>\n"
				+ "track_id: music track ID or Resource as defined in ambientmusic.2da"
			);
			return 0;
		}

		int nTrack = GetTrackByIDName(ArrayGetString(args, 0));
		if(nTrack < 0)
			return 10;

		MusicBattleChange(oTarget, nTrack);
		NWBash_Info("Set battle music to " + IntToString(nTrack) + " / '" + Get2DAString("ambientmusic", "Resource", nTrack) + "' in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "amb-play"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music amb-play"
			);
			return 0;
		}
		AmbientSoundPlay(oTarget);
		NWBash_Info("Play ambient sounds in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "amb-stop"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music amb-stop"
			);
			return 0;
		}
		AmbientSoundStop(oTarget);
		NWBash_Info("Stop ambient sounds in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "amb-day"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music amb-day <track_id>\n"
				+ "track_id: ambient sound ID or Resource as defined in ambientsound.2da"
			);
			return 0;
		}

		int nTrack = GetAmbientByIDName(ArrayGetString(args, 0));
		if(nTrack < 0)
			return 10;

		AmbientSoundChangeDay(oTarget, nTrack);
		NWBash_Info("Set day ambient sounds to " + IntToString(nTrack) + " / '" + Get2DAString("ambientsound", "Resource", nTrack) + "' in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "amb-night"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music amb-night <track_id>\n"
				+ "track_id: ambient sound ID or Resource as defined in ambientsound.2da"
			);
			return 0;
		}

		int nTrack = GetAmbientByIDName(ArrayGetString(args, 0));
		if(nTrack < 0)
			return 10;

		AmbientSoundChangeNight(oTarget, nTrack);
		NWBash_Info("Set night ambient sounds to " + IntToString(nTrack) + " / '" + Get2DAString("ambientsound", "Resource", nTrack) + "' in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "amb-day-vol"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music amb-day-vol <volume>\n"
				+ "volume: ambient sound volume"
			);
			return 0;
		}

		int nVol = ArrayGetInt(args, 0);
		AmbientSoundSetDayVolume(oTarget, nVol);
		NWBash_Info("Set day ambient sounds volume to " + IntToString(nVol) + " in area " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sSubcommand == "amb-night-vol"){
		if(bNeedHelp){
			NWBash_Info(
				"Usage: music amb-night-vol <volume>\n"
				+ "volume: ambient sound volume"
			);
			return 0;
		}

		int nVol = ArrayGetInt(args, 0);
		AmbientSoundSetNightVolume(oTarget, nVol);
		NWBash_Info("Set night ambient sounds volume to " + IntToString(nVol) + " in area " + NWBash_ObjectToUserString(oTarget));
	}
	else{
		NWBash_Err("Unknown command '" + sSubcommand + "'");
		return 3;
	}
	return 0;
}