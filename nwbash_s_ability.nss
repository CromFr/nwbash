#include "nwbash_inc"

int GetAbilityID(string sAbility){
	if(NWBash_IsNumeric(sAbility)){
		return StringToInt(sAbility);
	} else{
		sAbility = GetStringLowerCase(sAbility);

		if(sAbility == "str" || sAbility == "strength")
			return ABILITY_STRENGTH;
		else if(sAbility == "dex" || sAbility == "dexterity")
			return ABILITY_DEXTERITY;
		else if(sAbility == "con" || sAbility == "constitution")
			return ABILITY_CONSTITUTION;
		else if(sAbility == "int" || sAbility == "intelligence")
			return ABILITY_INTELLIGENCE;
		else if(sAbility == "wis" || sAbility == "wisdom")
			return ABILITY_WISDOM;
		else if(sAbility == "cha" || sAbility == "charisma")
			return ABILITY_CHARISMA;
		return -1;
	}
}
string GetAbilityName(int nAbility){
	switch(nAbility){
		case ABILITY_STRENGTH: return "Strength";
		case ABILITY_DEXTERITY: return "Dexterity";
		case ABILITY_CONSTITUTION: return "Constitution";
		case ABILITY_INTELLIGENCE: return "Intelligence";
		case ABILITY_WISDOM: return "Wisdom";
		case ABILITY_CHARISMA: return "Charisma";
	}
	return "INVALID";
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: ability [options] <ability> [...]\n"
			+"       ability [options] <ability>=<value> [...]\n"
			+ "The first form prints ability values, while the second change ability values.\n"
			+ "Multiple abilities can be provided, in either forms.\n"
			+ "\n"
			+ "Arguments:\n"
			+ "ability  Ability ID or name (3 letters or full name)\n"
			+ "value    Ability value. Start with + or - to modify current values\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help  show this help"
		);
		return 0;
	}
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));

	object oTarget = NWBash_GetTarget();

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		NWBash_Err("Bad number of arguments. See 'ability --help'");
		return 1;
	}

	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 3;
	}
	if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE){
		NWBash_Err("Target is not a creature");
		return 4;
	}

	int i;
	for(i = 0 ; i < nArgsLen ; i++)
	{
		string sArg = ArrayGetString(args, i);
		int nEqPos = FindSubString(sArg, "=");
		string sAbility = GetSubString(sArg, 0, nEqPos);

		int nAbility = GetAbilityID(sAbility);
		if(nAbility < 0){
			NWBash_Err("Invalid ability '" + sAbility + "'");
			continue;
		}

		string sAbilityName = GetAbilityName(nAbility);

		if(nEqPos < 0){
			// Print Ability info
			int nBase = GetAbilityScore(oTarget, nAbility, TRUE);
			int nTotal = GetAbilityScore(oTarget, nAbility, FALSE);
			int nMod = GetAbilityModifier(nAbility, oTarget);
			NWBash_Info("Ability " + sAbilityName + ": base=" + IntToString(nBase) + " with bonuses=" + IntToString(nTotal) + " (mod " + (nMod >= 0 ? "+" : "") + IntToString(nMod) + ")" + " (on " + NWBash_ObjectToUserString(oTarget) + ")");
		}
		else{
			// Set the Ability
			string sValue = GetSubString(sArg, nEqPos + 1, -1);
			string sMod = GetStringLeft(sValue, 1);
			if(sMod == "+" || sMod == "-"){
				sValue = GetSubString(sValue, 1, -1);
			} else{
				sMod = "";
			}

			if(!NWBash_IsNumeric(sValue)){
				NWBash_Err("Invalid ability value: '" + sValue + "'");
				continue;
			}
			int nValue = StringToInt(sValue);

			int nPrevValue = GetAbilityScore(oTarget, nAbility, TRUE);

			if(sMod == "+")
				nValue = nPrevValue + nValue;
			else if(sMod == "-")
				nValue = nPrevValue - nValue;


			SetBaseAbilityScore(oTarget, nAbility, nValue);
			NWBash_Info("Ability " + sAbilityName + ": Changed from " + IntToString(nPrevValue) + " to " + IntToString(nValue) + " (on " + NWBash_ObjectToUserString(oTarget) + ")");
		}
	}

	return 0;
}
