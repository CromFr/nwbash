#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: spellknown [options] <spell_id>=<known>\n"
			+ "       spellknown [options] <spell_id>\n"
			+ "spell_id must be an integer matching a spell ID in spells.2da\n"
			+ "The form with '=<known>' sets the spell to be known or unknown\n"
			+ "The other form prints if the spell is known or not\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help        Show this help\n"
			+ "--withlvl         Binds the spell with the current level\n"
			+ "--classpos=<int>  Class position (if the target has multiple caster classes)"
		);
		return 0;
	}

	int bWithLevel = NWBash_GetBooleanArg(args, "withlvl");
	string sClassPos = NWBash_GetStringArg(args, "classpos");

	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|withlvl|"));
	args = NWBash_RemoveStringArgs(args, Array("|classpos|"));

	object oTarget = NWBash_GetTarget();

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		NWBash_Err("Bad number of arguments. See 'spellknown --help'");
		return 1;
	}

	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 3;
	}
	if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE){
		NWBash_Err("Target is not a creature");
		return 4;
	}

	int nClassPos;
	if(sClassPos != "")
		nClassPos = StringToInt(sClassPos);
	else{
		for(nClassPos = 0 ; nClassPos < 4 ; nClassPos++){
			int nClass = GetClassByPosition(nClassPos, oTarget);
			if(Get2DAString("classes", "SpellGainTable", nClass) != "")
				break;
		}
	}

	int nClass = GetClassByPosition(nClassPos, oTarget);
	if(Get2DAString("classes", "SpellGainTable", nClass) == ""){
		NWBash_Err("Class " + Get2DAString("classes", "Label", nClass) + " is not a spellcasting class");
		return 5;
	}

	int i;
	for(i = 0 ; i < nArgsLen ; i++)
	{
		string sSpellArg = ArrayGetString(args, i);

		int nEqPos = FindSubString(sSpellArg, "=");
		if(nEqPos >= 0){
			int nSpellID = StringToInt(GetSubString(sSpellArg, 0, nEqPos));
			int bKnown = NWBash_StringToBool(GetSubString(sSpellArg, nEqPos + 1, -1));

			SetSpellKnown(oTarget, nClassPos, nSpellID, bKnown, bWithLevel);
			NWBash_Info((bKnown ? "Gave" : "Removed") + " spell " + GetStringByStrRef(StringToInt(Get2DAString("spells", "Name", nSpellID))) + " (ID=" + IntToString(nSpellID) + ") to " + NWBash_ObjectToUserString(oTarget));
		}
		else{
			int nSpellID = StringToInt(sSpellArg);
			int bKnown = GetSpellKnown(oTarget, nSpellID);
			NWBash_Info("Spell '" + GetStringByStrRef(StringToInt(Get2DAString("spells", "Name", nSpellID))) + "' (ID=" + IntToString(nSpellID) + "): " + (bKnown ? "Known" : "Unknown"));
		}
	}

	return 0;
}
