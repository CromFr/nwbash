#include "nwbash_inc"

int GetSubraceECL(int nSubrace){
	return StringToInt(Get2DAString("racialsubtypes", "ECL", nSubrace));
}

// Returns the amount of XP needed to reach a given level from level 1
int GetXPForLevel(int nRawLevel){
	return StringToInt(Get2DAString("exptable", "XP", nRawLevel - 1));
}

// Returns the amount of XP needed to reach a given level with the given ECL from level 1.
int GetXPForAdjustedLevel(int nECL, int nRawLevel){
	return GetXPForLevel(nRawLevel + nECL);
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: !level [options] <amount>\n"
			+ "<amount>: amount of levels to give or remove. Can be negative.\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help: show this help"
			+ "--set:      Set to this level"
		);
		return 0;
	}
	int bSet = NWBash_GetBooleanArg(args, "set");
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|set|"));

	int argsLen = ArrayGetLength(args);
	if(argsLen != 1){
		NWBash_Err("Bad number of arguments. See 'level --help'");
		return 1;
	}

	int nAmount = ArrayGetInt(args, 0);
	if(nAmount == 0 || bSet && nAmount <= 0){
		NWBash_Err("Bad level amount");
		return 2;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 3;
	}
	if(GetObjectType(oTarget) != OBJECT_TYPE_CREATURE){
		NWBash_Err("Target is not a creature");
		return 4;
	}

	int nLvl = GetTotalLevels(oTarget, FALSE);

	int nTargetLvl;
	if(bSet)
		nTargetLvl = nAmount;
	else
		nTargetLvl = nLvl + nAmount;

	int nCurrentXP = GetXP(oTarget);
	int nNewXP = GetXPForAdjustedLevel(GetSubraceECL(GetLocalInt(oTarget, "player_subrace")), nTargetLvl);

	SetXP(oTarget, nNewXP);
	NWBash_Info("Set " + NWBash_ObjectToUserString(oTarget) + " level to " + IntToString(nTargetLvl) + " (" + (nCurrentXP < nNewXP ? "+" : "") + IntToString(nCurrentXP - nNewXP) + " xp)");
	return 0;
}




