#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	int bClear = NWBash_GetBooleanArg(args, "clear");

	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|clear|"));
	int nArgsLen = ArrayGetLength(args);

	if(bNeedHelp && nArgsLen == 0){
		NWBash_Info(
			  "Usage: action [options] <action_function> [<args> ...]\n"
			+ "       action [options]\n"
			+ "The first form allows to set the object action, the second only prints the object action.\n"
			+ "action_* parameters can be '-' to keep the current action on a specific axis\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-h, --help Show this help\n"
			+ "--clear    Clear all actions before\n"
			+ "\n"
			+ "Actions list:\n"
			+ "- ActionAttack\n"
			+ "- ActionCastFakeSpellAtLocation\n"
			+ "- ActionCastFakeSpellAtObject\n"
			+ "- ActionCastSpellAtLocation\n"
			+ "- ActionCastSpellAtObject\n"
			+ "- ActionCloseDoor\n"
			+ "- ActionCounterSpell\n"
			+ "- ActionEquipItem\n"
			+ "- ActionEquipMostDamagingMelee\n"
			+ "- ActionEquipMostDamagingRanged\n"
			+ "- ActionEquipMostEffectiveArmor\n"
			+ "- ActionExamine\n"
			+ "- ActionForceFollowObject\n"
			+ "- ActionForceMoveToLocation\n"
			+ "- ActionForceMoveToObject\n"
			+ "- ActionGiveItem\n"
			+ "- ActionInteractObject\n"
			+ "- ActionJumpToLocation\n"
			+ "- ActionJumpToObject\n"
			+ "- ActionLockObject\n"
			+ "- ActionMoveAwayFromLocation\n"
			+ "- ActionMoveAwayFromObject\n"
			+ "- ActionMoveToLocation\n"
			+ "- ActionMoveToObject\n"
			+ "- ActionOpenDoor\n"
			+ "- ActionPauseConversation\n"
			+ "- ActionPickUpItem\n"
			+ "- ActionPlayAnimation\n"
			+ "- ActionPutDownItem\n"
			+ "- ActionRandomWalk\n"
			+ "- ActionRest\n"
			+ "- ActionResumeConversation\n"
			+ "- ActionSit\n"
			+ "- ActionSpeakString\n"
			+ "- ActionSpeakStringByStrRef\n"
			+ "- ActionStartConversation\n"
			+ "- ActionTakeItem\n"
			+ "- ActionUnequipItem\n"
			+ "- ActionUnlockObject\n"
			+ "- ActionUseFeat\n"
			+ "- ActionUseTalentAtLocation\n"
			+ "- ActionUseTalentOnObject\n"
			+ "- ActionWait\n"
			+ "- JumpToLocation\n"
			+ "- JumpToObject\n"

			+ "- SetFacing\n"
			+ "- SetCameraFacing\n"
			+ "- SetFacingPoint\n"
			+ "- PlaySound\n"
			+ "- SpeakString\n"
			+ "- PlayAnimation\n"
			+ "- GiveGoldToCreature\n"
			+ "- SpeakOneLinerConversation\n"
			+ "- TakeGoldFromCreature\n"
			+ "- SurrenderToEnemies\n"
			+ "- StoreCameraFacing\n"
			+ "- RestoreCameraFacing\n"
			+ "- DoWhirlwindAttack\n"
			+ "- PlaySoundByStrRef\n"

		);
		return 0;
	}

	if(nArgsLen == 0){
		NWBash_Err("No actions to play. See action --help");
		return 1;
	}

	object oActionSubject = NWBash_GetTarget();
	if(!GetIsObjectValid(oActionSubject)){
		NWBash_Err("Invalid target");
		return 2;
	}

	if(bClear)
		AssignCommand(oActionSubject, ClearAllActions());

	string sAct = ArrayGetString(args, 0);
	args = ArrayPopFront(args);
	nArgsLen--;

	if(sAct == "ActionRandomWalk"){
		if(bNeedHelp || nArgsLen != 0){
			NWBash_Err("Bad parameters for: ActionRandomWalk()");
			return 10;
		}
		AssignCommand(oActionSubject, ActionRandomWalk());
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => walk randomly");
	}
	else if(sAct == "ActionMoveToLocation"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: ActionMoveToLocation(location lDestination, int bRun=FALSE)");
			return 10;
		}
		location lDestination = NWBash_ResolveLocationString(ArrayGetString(args, 0));
		int bRun = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : FALSE;

		AssignCommand(oActionSubject, ActionMoveToLocation(lDestination, bRun));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => " + (bRun ? "run" : "walk") + " towards location " + NWBash_LocationToString(lDestination));
	}
	else if(sAct == "ActionMoveToObject"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: ActionMoveToObject(object oMoveTo, int bRun=FALSE, float fRange=1.0f)");
			return 10;
		}
		object oMoveTo = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int bRun = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : FALSE;
		float fRange = nArgsLen > 2 ? ArrayGetFloat(args, 2) : 1.0;

		AssignCommand(oActionSubject, ActionMoveToObject(oMoveTo, bRun, fRange));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => " + (bRun ? "run" : "walk") + " towards object " + NWBash_ObjectToUserString(oMoveTo) + " with min dist " + FloatToString(fRange, 0, 0));
	}
	else if(sAct == "ActionMoveAwayFromObject"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: ActionMoveAwayFromObject(object oFleeFrom, int bRun=FALSE, float fMoveAwayRange=40.0f)");
			return 10;
		}
		object oFleeFrom = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int bRun = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : FALSE;
		float fMoveAwayRange = nArgsLen > 2 ? ArrayGetFloat(args, 2) : 40.0;

		AssignCommand(oActionSubject, ActionMoveAwayFromObject(oFleeFrom, bRun, fMoveAwayRange));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => " + (bRun ? "run" : "walk") + " away from object " + NWBash_ObjectToUserString(oFleeFrom) + " with max dist " + FloatToString(fMoveAwayRange, 0, 0));
	}
	else if(sAct == "ActionEquipItem"){
		if(bNeedHelp || nArgsLen != 2){
			NWBash_Err("Bad parameters for: ActionEquipItem(object oItem, int nInventorySlot)");
			return 10;
		}
		object oItem = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int nInventorySlot = ArrayGetInt(args, 1);

		AssignCommand(oActionSubject, ActionEquipItem(oItem, nInventorySlot));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => equip item " + NWBash_ObjectToUserString(oItem) + " in slot " + IntToString(nInventorySlot));
	}
	else if(sAct == "ActionUnequipItem"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionUnequipItem(object oItem)");
			return 10;
		}
		object oItem = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int nInventorySlot = ArrayGetInt(args, 1);

		AssignCommand(oActionSubject, ActionUnequipItem(oItem));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => unequip item " + NWBash_ObjectToUserString(oItem));
	}
	else if(sAct == "ActionPickUpItem"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionPickUpItem(object oItem)");
			return 10;
		}
		object oItem = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int nInventorySlot = ArrayGetInt(args, 1);

		AssignCommand(oActionSubject, ActionPickUpItem(oItem));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => pick up item " + NWBash_ObjectToUserString(oItem));
	}
	else if(sAct == "ActionPutDownItem"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionPutDownItem(object oItem)");
			return 10;
		}
		object oItem = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int nInventorySlot = ArrayGetInt(args, 1);

		AssignCommand(oActionSubject, ActionPutDownItem(oItem));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => put down item " + NWBash_ObjectToUserString(oItem));
	}
	else if(sAct == "ActionAttack"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: ActionAttack(object oAttackee, int bPassive=FALSE)");
			return 10;
		}
		object oAttackee = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int bPassive = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : FALSE;

		AssignCommand(oActionSubject, ActionAttack(oAttackee, bPassive));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => attack " + NWBash_ObjectToUserString(oAttackee) + " (passive=" + IntToString(bPassive) + ")");
	}
	else if(sAct == "ActionSpeakString"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: ActionSpeakString(string sStringToSpeak, int nTalkVolume=TALKVOLUME_TALK)");
			return 10;
		}
		string sStringToSpeak = ArrayGetString(args, 0);
		int nTalkVolume = nArgsLen > 1 ? ArrayGetInt(args, 1) : TALKVOLUME_TALK;

		AssignCommand(oActionSubject, ActionSpeakString(sStringToSpeak, nTalkVolume));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => speak on channel " + IntToString(nTalkVolume));
	}
	else if(sAct == "PlayAnimation"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: PlayAnimation(int nAnimation, float fSpeed=1.0, float fSeconds=0.0)");
			return 10;
		}
		int nAnimation = ArrayGetInt(args, 0);
		float fSpeed = nArgsLen > 1 ? ArrayGetFloat(args, 1) : 1.0;
		float fSeconds = nArgsLen > 2 ? ArrayGetFloat(args, 2) : 0.0;

		AssignCommand(oActionSubject, PlayAnimation(nAnimation, fSpeed, fSeconds));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => play animation " + IntToString(nAnimation) + " for " + FloatToString(fSeconds, 0, 0) + " seconds");
	}
	else if(sAct == "ActionOpenDoor"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionOpenDoor(object oDoor)");
			return 10;
		}
		object oDoor = NWBash_ResolveObjectString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, ActionOpenDoor(oDoor));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => open door " + NWBash_ObjectToUserString(oDoor));
	}
	else if(sAct == "ActionCloseDoor"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionCloseDoor(object oDoor)");
			return 10;
		}
		object oDoor = NWBash_ResolveObjectString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, ActionCloseDoor(oDoor));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => close door " + NWBash_ObjectToUserString(oDoor));
	}
	else if(sAct == "ActionCastSpellAtObject"){
		if(bNeedHelp || nArgsLen < 2 || nArgsLen > 7){
			NWBash_Err("Bad parameters for: ActionCastSpellAtObject(int nSpell, object oTarget, int nMetaMagic=METAMAGIC_ANY, int bCheat=FALSE, int nDomainLevel=0, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE)");
			return 10;
		}
		int nSpell              = ArrayGetInt(args, 0);
		object oTarget          = NWBash_ResolveObjectString(ArrayGetString(args, 1));
		int nMetaMagic          = nArgsLen > 2 ? ArrayGetInt(args, 2) : METAMAGIC_ANY;
		int bCheat              = nArgsLen > 3 ? NWBash_StringToBool(ArrayGetString(args, 3)) : FALSE;
		int nDomainLevel        = nArgsLen > 4 ? ArrayGetInt(args, 4) : 0;
		int nProjectilePathType = nArgsLen > 5 ? ArrayGetInt(args, 5) : PROJECTILE_PATH_TYPE_DEFAULT;
		int bInstantSpell       = nArgsLen > 6 ? NWBash_StringToBool(ArrayGetString(args, 6)) : FALSE;

		AssignCommand(oActionSubject, ActionCastSpellAtObject(nSpell, oTarget, nMetaMagic, bCheat, nDomainLevel, nProjectilePathType, bInstantSpell));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => cast spell " + Get2DAString("spells", "Label", nSpell) + " at object " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sAct == "ActionCastSpellAtLocation"){
		if(bNeedHelp || nArgsLen < 2 || nArgsLen > 7){
			NWBash_Err("Bad parameters for: ActionCastSpellAtLocation(int nSpell, location lTargetLocation, int nMetaMagic=METAMAGIC_ANY, int bCheat=FALSE, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE, int nDomainLevel=0)");
			return 10;
		}
		int nSpell               = ArrayGetInt(args, 0);
		location lTargetLocation = NWBash_ResolveLocationString(ArrayGetString(args, 1));
		int nMetaMagic           = nArgsLen > 2 ? ArrayGetInt(args, 2) : METAMAGIC_ANY;
		int bCheat               = nArgsLen > 3 ? NWBash_StringToBool(ArrayGetString(args, 3)) : FALSE;
		int nProjectilePathType  = nArgsLen > 4 ? ArrayGetInt(args, 4) : PROJECTILE_PATH_TYPE_DEFAULT;
		int bInstantSpell        = nArgsLen > 5 ? NWBash_StringToBool(ArrayGetString(args, 5)) : FALSE;
		int nDomainLevel         = nArgsLen > 6 ? ArrayGetInt(args, 6) : 0;

		AssignCommand(oActionSubject, ActionCastSpellAtLocation(nSpell, lTargetLocation, nMetaMagic, bCheat, nProjectilePathType, bInstantSpell, nDomainLevel));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => cast spell " + Get2DAString("spells", "Label", nSpell) + " at location " + NWBash_LocationToString(lTargetLocation));
	}
	else if(sAct == "ActionGiveItem"){
		if(bNeedHelp || nArgsLen < 2 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: ActionGiveItem(object oItem, object oGiveTo, int bDisplayFeedback=TRUE)");
			return 10;
		}
		object oItem = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		object oGiveTo = NWBash_ResolveObjectString(ArrayGetString(args, 1));
		int bDisplayFeedback = nArgsLen > 2 ? NWBash_StringToBool(ArrayGetString(args, 2)) : TRUE;

		AssignCommand(oActionSubject, ActionGiveItem(oItem, oGiveTo, bDisplayFeedback));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => give item " + NWBash_ObjectToUserString(oItem) + " to " + NWBash_ObjectToUserString(oGiveTo));
	}
	else if(sAct == "ActionTakeItem"){
		if(bNeedHelp || nArgsLen < 2 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: ActionTakeItem(object oItem, object oTakeFrom, int bDisplayFeedback=TRUE)");
			return 10;
		}
		object oItem = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		object oTakeFrom = NWBash_ResolveObjectString(ArrayGetString(args, 1));
		int bDisplayFeedback = nArgsLen > 2 ? NWBash_StringToBool(ArrayGetString(args, 2)) : TRUE;

		AssignCommand(oActionSubject, ActionTakeItem(oItem, oTakeFrom, bDisplayFeedback));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => take item " + NWBash_ObjectToUserString(oItem) + " from " + NWBash_ObjectToUserString(oTakeFrom));
	}
	else if(sAct == "ActionForceFollowObject"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: ActionForceFollowObject(object oFollow, float fFollowDistance=0.5f, int iFollowPosition=0)");
			return 10;
		}
		object oFollow = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		float fFollowDistance = nArgsLen > 1 ? ArrayGetFloat(args, 1) : 0.5;
		int iFollowPosition = nArgsLen > 2 ? ArrayGetInt(args, 2) : 0;

		AssignCommand(oActionSubject, ActionForceFollowObject(oFollow, fFollowDistance, iFollowPosition));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => force follow " + NWBash_ObjectToUserString(oFollow) + " at distance " + FloatToString(fFollowDistance, 0, 0) + " with position " + IntToString(iFollowPosition));
	}
	else if(sAct == "ActionSit"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionSit(object oChair)");
			return 10;
		}
		object oChair = NWBash_ResolveObjectString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, ActionSit(oChair));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => sit on " + NWBash_ObjectToUserString(oChair));
	}
	else if(sAct == "ActionJumpToObject"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: ActionJumpToObject(object oToJumpTo, int bWalkStraightLineToPoint=TRUE)");
			return 10;
		}
		object oToJumpTo = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int bWalkStraightLineToPoint = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : TRUE;

		AssignCommand(oActionSubject, ActionJumpToObject(oToJumpTo, bWalkStraightLineToPoint));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => jump to object " + NWBash_ObjectToUserString(oToJumpTo));
	}
	else if(sAct == "JumpToObject"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: JumpToObject(object oToJumpTo, int nWalkStraightLineToPoint=1)");
			return 10;
		}
		object oToJumpTo = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int bWalkStraightLineToPoint = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : TRUE;

		AssignCommand(oActionSubject, JumpToObject(oToJumpTo, bWalkStraightLineToPoint));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => jump immediately to object " + NWBash_ObjectToUserString(oToJumpTo));
	}
	else if(sAct == "ActionJumpToLocation"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionJumpToLocation(location lLocation)");
			return 10;
		}
		location lLocation = NWBash_ResolveLocationString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, ActionJumpToLocation(lLocation));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => jump to location " + NWBash_LocationToString(lLocation));
	}
	else if(sAct == "JumpToLocation"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: JumpToLocation(location lDestination)");
			return 10;
		}
		location lLocation = NWBash_ResolveLocationString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, JumpToLocation(lLocation));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => jump immediately to location " + NWBash_LocationToString(lLocation));
	}
	else if(sAct == "ActionWait"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionWait(float fSeconds)");
			return 10;
		}
		float fSeconds = ArrayGetFloat(args, 0);

		AssignCommand(oActionSubject, ActionWait(fSeconds));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => wait for " + FloatToString(fSeconds, 0, 0) + " seconds");
	}
	else if(sAct == "ActionStartConversation"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 6){
			NWBash_Err("Bad parameters for: ActionStartConversation(object oObjectToConverseWith, string sDialogResRef='', int bPrivateConversation=FALSE, int bPlayHello=TRUE, int bIgnoreStartDistance=FALSE, int bDisableCutsceneBars=FALSE)");
			return 10;
		}
		object oObjectToConverseWith = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		string sDialogResRef         = nArgsLen > 1 ? ArrayGetString(args, 1) : "";
		int bPrivateConversation     = nArgsLen > 2 ? NWBash_StringToBool(ArrayGetString(args, 2)) : FALSE;
		int bPlayHello               = nArgsLen > 3 ? NWBash_StringToBool(ArrayGetString(args, 3)) : TRUE;
		int bIgnoreStartDistance     = nArgsLen > 4 ? NWBash_StringToBool(ArrayGetString(args, 4)) : FALSE;
		int bDisableCutsceneBars     = nArgsLen > 5 ? NWBash_StringToBool(ArrayGetString(args, 5)) : FALSE;

		AssignCommand(oActionSubject, ActionStartConversation(oObjectToConverseWith, sDialogResRef, bPrivateConversation, bPlayHello, bIgnoreStartDistance, bDisableCutsceneBars));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => start conversation " + sDialogResRef + " with " + NWBash_ObjectToUserString(oObjectToConverseWith));
	}
	else if(sAct == "ActionPauseConversation"){
		if(bNeedHelp || nArgsLen != 0){
			NWBash_Err("Bad parameters for: ActionPauseConversation()");
			return 10;
		}
		AssignCommand(oActionSubject, ActionPauseConversation());
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => pause conversation");
	}
	else if(sAct == "ActionResumeConversation"){
		if(bNeedHelp || nArgsLen != 0){
			NWBash_Err("Bad parameters for: ActionResumeConversation()");
			return 10;
		}
		AssignCommand(oActionSubject, ActionResumeConversation());
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => resume conversation");
	}
	else if(sAct == "ActionSpeakStringByStrRef"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: ActionSpeakStringByStrRef(int nStrRef, int nTalkVolume=TALKVOLUME_TALK)");
			return 10;
		}
		int nStrRef = ArrayGetInt(args, 0);
		int nTalkVolume = nArgsLen > 1 ? ArrayGetInt(args, 1) : TALKVOLUME_TALK;

		AssignCommand(oActionSubject, ActionSpeakStringByStrRef(nStrRef, nTalkVolume));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => speak strref " + IntToString(nStrRef) + " on channel " + IntToString(nTalkVolume));
	}
	else if(sAct == "ActionUseFeat"){
		if(bNeedHelp || nArgsLen != 2){
			NWBash_Err("Bad parameters for: ActionUseFeat(int nFeat, object oTarget)");
			return 10;
		}
		int nFeat = ArrayGetInt(args, 0);
		object oTarget = NWBash_ResolveObjectString(ArrayGetString(args, 1));

		AssignCommand(oActionSubject, ActionUseFeat(nFeat, oTarget));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => use feat " + IntToString(nFeat) + " on object " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sAct == "ActionUseTalentOnObject"){
		if(bNeedHelp || nArgsLen != 2){
			NWBash_Err("Bad parameters for: ActionUseTalentOnObject(talent tChosenTalent, object oTarget)");
			return 10;
		}
		talent tChosenTalent;
		string sTalent = ArrayGetString(args, 0);
		if(GetStringLeft(sTalent, 3) == "sp:")
			tChosenTalent = TalentSpell(StringToInt(GetSubString(sTalent, 3, -1)));
		else if(GetStringLeft(sTalent, 3) == "fe:")
			tChosenTalent = TalentFeat(StringToInt(GetSubString(sTalent, 3, -1)));
		else if(GetStringLeft(sTalent, 3) == "sk:")
			tChosenTalent = TalentSkill(StringToInt(GetSubString(sTalent, 3, -1)));
		else{
			NWBash_Err("Unknown talent. Talent must be in the form 'sp:42', 'fe:42' or 'sk:42'");
			return 11;
		}
		object oTarget = NWBash_ResolveObjectString(ArrayGetString(args, 1));

		AssignCommand(oActionSubject, ActionUseTalentOnObject(tChosenTalent, oTarget));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => use talent " + sTalent + " on object " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sAct == "ActionUseTalentAtLocation"){
		if(bNeedHelp || nArgsLen != 2){
			NWBash_Err("Bad parameters for: ActionUseTalentAtLocation(talent tChosenTalent, location lTargetLocation)");
			return 10;
		}
		talent tChosenTalent;
		string sTalent = ArrayGetString(args, 0);
		if(GetStringLeft(sTalent, 3) == "sp:")
			tChosenTalent = TalentSpell(StringToInt(GetSubString(sTalent, 3, -1)));
		else if(GetStringLeft(sTalent, 3) == "fe:")
			tChosenTalent = TalentFeat(StringToInt(GetSubString(sTalent, 3, -1)));
		else if(GetStringLeft(sTalent, 3) == "sk:")
			tChosenTalent = TalentSkill(StringToInt(GetSubString(sTalent, 3, -1)));
		else{
			NWBash_Err("Unknown talent. Talent must be in the form 'sp:42', 'fe:42' or 'sk:42'");
			return 11;
		}
		location lTargetLocation = NWBash_ResolveLocationString(ArrayGetString(args, 1));

		AssignCommand(oActionSubject, ActionUseTalentAtLocation(tChosenTalent, lTargetLocation));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => use talent " + sTalent + " at location " + NWBash_LocationToString(lTargetLocation));
	}
	else if(sAct == "ActionInteractObject"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionInteractObject(object oPlaceable)");
			return 10;
		}
		object oPlaceable = NWBash_ResolveObjectString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, ActionInteractObject(oPlaceable));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => interact with object " + NWBash_ObjectToUserString(oPlaceable));
	}
	else if(sAct == "ActionMoveAwayFromLocation"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: ActionMoveAwayFromLocation(location lMoveAwayFrom, int bRun=FALSE, float fMoveAwayRange=40.0f)");
			return 10;
		}
		location lMoveAwayFrom = NWBash_ResolveLocationString(ArrayGetString(args, 0));
		int bRun = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : FALSE;
		float fMoveAwayRange = nArgsLen > 2 ? ArrayGetFloat(args, 2) : 40.0;

		AssignCommand(oActionSubject, ActionMoveAwayFromLocation(lMoveAwayFrom, bRun, fMoveAwayRange));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => " + (bRun ? "run" : "walk") + " away from location " + NWBash_LocationToString(lMoveAwayFrom) + " with max dist " + FloatToString(fMoveAwayRange, 0, 0));
	}
	else if(sAct == "ActionForceMoveToLocation"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: ActionForceMoveToLocation(location lDestination, int bRun=FALSE, float fTimeout=30.0f)");
			return 10;
		}
		location lDestination = NWBash_ResolveLocationString(ArrayGetString(args, 0));
		int bRun = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : FALSE;
		float fTimeout = nArgsLen > 2 ? ArrayGetFloat(args, 2) : 30.0f;

		AssignCommand(oActionSubject, ActionForceMoveToLocation(lDestination, bRun, fTimeout));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => force " + (bRun ? "run" : "walk") + " towards location " + NWBash_LocationToString(lDestination));
	}
	else if(sAct == "ActionForceMoveToObject"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 4 ){
			NWBash_Err("Bad parameters for: ActionForceMoveToObject(object oMoveTo, int bRun=FALSE, float fRange=1.0f, float fTimeout=30.0f)");
			return 10;
		}
		object oMoveTo = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int bRun = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : FALSE;
		float fRange = nArgsLen > 2 ? ArrayGetFloat(args, 2) : 1.0;
		float fTimeout = nArgsLen > 3 ? ArrayGetFloat(args, 3) : 30.0;

		AssignCommand(oActionSubject, ActionForceMoveToObject(oMoveTo, bRun, fRange, fTimeout));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => force " + (bRun ? "run" : "walk") + " towards object " + NWBash_ObjectToString(oMoveTo) + " with min dist " + FloatToString(fRange, 0, 0));
	}
	else if(sAct == "ActionEquipMostDamagingMelee"){
		if(bNeedHelp || nArgsLen > 2){
			NWBash_Err("Bad parameters for: ActionEquipMostDamagingMelee(object oVersus=OBJECT_INVALID, int bOffHand=FALSE)");
			return 10;
		}
		object oVersus = nArgsLen > 0 ? NWBash_ResolveObjectString(ArrayGetString(args, 0)) : OBJECT_INVALID;
		int bOffHand = nArgsLen > 1 ? NWBash_StringToBool(ArrayGetString(args, 1)) : FALSE;

		AssignCommand(oActionSubject, ActionEquipMostDamagingMelee(oVersus, bOffHand));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => equip most damaging melee weapon vs " + NWBash_ObjectToUserString(oVersus) + " on " + (bOffHand ? "off" : "main") + " hand");
	}
	else if(sAct == "ActionEquipMostDamagingRanged"){
		if(bNeedHelp || nArgsLen > 1){
			NWBash_Err("Bad parameters for: ActionEquipMostDamagingRanged(object oVersus=OBJECT_INVALID)");
			return 10;
		}
		object oVersus = nArgsLen > 0 ? NWBash_ResolveObjectString(ArrayGetString(args, 0)) : OBJECT_INVALID;

		AssignCommand(oActionSubject, ActionEquipMostDamagingRanged(oVersus));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => equip most damaging ranged weapon vs " + NWBash_ObjectToUserString(oVersus));
	}
	else if(sAct == "ActionRest"){
		if(bNeedHelp || nArgsLen > 1){
			NWBash_Err("Bad parameters for: ActionRest(int bIgnoreNoRestFlag=0)");
			return 10;
		}
		int bIgnoreNoRestFlag = nArgsLen > 0 ? NWBash_StringToBool(ArrayGetString(args, 0)) : 0;

		AssignCommand(oActionSubject, ActionRest(bIgnoreNoRestFlag));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => rest");
	}
	else if(sAct == "ActionEquipMostEffectiveArmor"){
		if(bNeedHelp || nArgsLen != 0){
			NWBash_Err("Bad parameters for: ActionEquipMostEffectiveArmor()");
			return 10;
		}

		AssignCommand(oActionSubject, ActionEquipMostEffectiveArmor());
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => equip most effective armor");
	}
	else if(sAct == "ActionUnlockObject"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionUnlockObject(object oTarget)");
			return 10;
		}
		object oTarget = NWBash_ResolveObjectString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, ActionUnlockObject(oTarget));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => unlock " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sAct == "ActionLockObject"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionLockObject(object oTarget)");
			return 10;
		}
		object oTarget = NWBash_ResolveObjectString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, ActionLockObject(oTarget));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => lock " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sAct == "ActionCastFakeSpellAtObject"){
		if(bNeedHelp || nArgsLen < 2 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: ActionCastFakeSpellAtObject(int nSpell, object oTarget, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT)");
			return 10;
		}
		int nSpell = ArrayGetInt(args, 0);
		object oTarget = NWBash_ResolveObjectString(ArrayGetString(args, 1));
		int nProjectilePathType = nArgsLen > 2 ? ArrayGetInt(args, 2) : PROJECTILE_PATH_TYPE_DEFAULT;

		AssignCommand(oActionSubject, ActionCastFakeSpellAtObject(nSpell, oTarget, nProjectilePathType));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => cast fake spell " + Get2DAString("spells", "Label", nSpell) + " at object " + NWBash_ObjectToUserString(oTarget));
	}
	else if(sAct == "ActionCastFakeSpellAtLocation"){
		if(bNeedHelp || nArgsLen < 2 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: ActionCastFakeSpellAtLocation(int nSpell, location lTarget, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT)");
			return 10;
		}
		int nSpell = ArrayGetInt(args, 0);
		location lTarget = NWBash_ResolveLocationString(ArrayGetString(args, 1));
		int nProjectilePathType = nArgsLen > 2 ? ArrayGetInt(args, 2) : PROJECTILE_PATH_TYPE_DEFAULT;

		AssignCommand(oActionSubject, ActionCastFakeSpellAtLocation(nSpell, lTarget, nProjectilePathType));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => cast fake spell " + Get2DAString("spells", "Label", nSpell) + " at location " + NWBash_LocationToString(lTarget));
	}
	else if(sAct == "ActionCounterSpell"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionCounterSpell(object oCounterSpellTarget)");
			return 10;
		}
		object oCounterSpellTarget = NWBash_ResolveObjectString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, ActionCounterSpell(oCounterSpellTarget));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => counter spell of object " + NWBash_ObjectToUserString(oCounterSpellTarget));
	}
	else if(sAct == "ActionExamine"){
		if(bNeedHelp || nArgsLen != 1){
			NWBash_Err("Bad parameters for: ActionExamine(object oExamine)");
			return 10;
		}
		object oExamine = NWBash_ResolveObjectString(ArrayGetString(args, 0));

		AssignCommand(oActionSubject, ActionExamine(oExamine));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => examine object " + NWBash_ObjectToUserString(oExamine));
	}
	else if(sAct == "SetFacing"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: SetFacing(float fDirection, int bLockToThisOrientation=FALSE)");
			return 10;
		}
		float fDirection = ArrayGetFloat(args, 0);
		int bLockToThisOrientation = nArgsLen > 1 ? ArrayGetInt(args, 1) : FALSE;

		AssignCommand(oActionSubject, SetFacing(fDirection, bLockToThisOrientation));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => face towards direction " + FloatToString(fDirection, 0, 2));
	}
	else if(sAct == "SetCameraFacing"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 4){
			NWBash_Err("Bad parameters for: SetCameraFacing(float fDirection, float fDistance=-1.0f, float fPitch=-1.0, int nTransitionType=CAMERA_TRANSITION_TYPE_SNAP)");
			return 10;
		}
		float fDirection = ArrayGetFloat(args, 0);
		float fDistance = nArgsLen > 1 ? ArrayGetFloat(args, 1) : -1.0;
		float fPitch = nArgsLen > 2 ? ArrayGetFloat(args, 2) : -1.0;
		int nTransitionType = nArgsLen > 3 ? ArrayGetInt(args, 3) : CAMERA_TRANSITION_TYPE_SNAP;

		AssignCommand(oActionSubject, SetCameraFacing(fDirection, fDistance, fPitch, nTransitionType));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => set camera facing dir=" + FloatToString(fDirection, 0, 0) + " dist=" + FloatToString(fDistance, 0, 0) + " pitch=" + FloatToString(fPitch, 0, 0) + " transition=" + IntToString(nTransitionType));
	}
	else if(sAct == "SetFacingPoint"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: SetFacingPoint(vector vTarget, int bLockToThisOrientation=FALSE)");
			return 10;
		}
		vector vTarget = NWBash_ResolveVectorString(ArrayGetString(args, 0));
		int bLockToThisOrientation = ArrayGetInt(args, 1);

		AssignCommand(oActionSubject, SetFacingPoint(vTarget, bLockToThisOrientation));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => set facing point " + NWBash_VectorToString(vTarget));
	}
	else if(sAct == "PlaySound"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: PlaySound(string sSoundName, int bPlayAs2D=FALSE)");
			return 10;
		}
		string sSoundName = ArrayGetString(args, 0);
		int bPlayAs2D = nArgsLen > 1 ? ArrayGetInt(args, 1) : FALSE;

		AssignCommand(oActionSubject, PlaySound(sSoundName, bPlayAs2D));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => Play sound " + sSoundName);
	}
	else if(sAct == "PlayAnimation"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: PlayAnimation(int nAnimation, float fSpeed=1.0, float fSeconds=0.0)");
			return 10;
		}
		int nAnimation = ArrayGetInt(args, 0);
		float fSpeed = nArgsLen > 1 ? ArrayGetFloat(args, 1) : 1.0;
		float fDurationSeconds = nArgsLen > 2 ? ArrayGetFloat(args, 2) : 0.0;

		AssignCommand(oActionSubject, ActionPlayAnimation(nAnimation, fSpeed, fDurationSeconds));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => play animation immediately " + IntToString(nAnimation) + " for " + FloatToString(fDurationSeconds, 0, 0) + " seconds");
	}
	else if(sAct == "GiveGoldToCreature"){
		if(bNeedHelp || nArgsLen < 2 || nArgsLen > 3){
			NWBash_Err("Bad parameters for: GiveGoldToCreature(object oCreature, int nGP, int bDisplayFeedback=TRUE)");
			return 10;
		}
		object oCreature = NWBash_ResolveObjectString(ArrayGetString(args, 0));
		int nGP = ArrayGetInt(args, 1);
		int bDisplayFeedback = nArgsLen > 2 ? ArrayGetInt(args, 2) : TRUE;

		AssignCommand(oActionSubject, GiveGoldToCreature(oCreature, nGP, bDisplayFeedback));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => give " + IntToString(nGP) + " gp to " + NWBash_ObjectToUserString(oCreature));
	}
	else if(sAct == "SpeakOneLinerConversation"){
		if(bNeedHelp || nArgsLen > 3){
			NWBash_Err("Bad parameters for: SpeakOneLinerConversation(string sDialogResRef='', object oTokenTarget=OBJECT_INVALID, int nTalkVolume=TALKVOLUME_TALK)");
			return 10;
		}
		string sDialogResRef = nArgsLen > 0 ? ArrayGetString(args, 0) : "";
		object oTokenTarget = nArgsLen > 1 ? NWBash_ResolveObjectString(ArrayGetString(args, 1)) : OBJECT_INVALID;
		int nTalkVolume = nArgsLen > 2 ? ArrayGetInt(args, 2) : TALKVOLUME_TALK;

		AssignCommand(oActionSubject, SpeakOneLinerConversation(sDialogResRef, oTokenTarget, nTalkVolume));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => speak one-liner " + sDialogResRef);
	}
	else if(sAct == "TakeGoldFromCreature"){
		if(bNeedHelp || nArgsLen < 2 || nArgsLen > 4){
			NWBash_Err("Bad parameters for: TakeGoldFromCreature(int nAmount, object oCreatureToTakeFrom, int bDestroy=FALSE, int bDisplayFeedback=TRUE)");
			return 10;
		}
		int nAmount = ArrayGetInt(args, 0);
		object oCreatureToTakeFrom = NWBash_ResolveObjectString(ArrayGetString(args, 1));
		int bDestroy = nArgsLen > 2 ? ArrayGetInt(args, 2) : FALSE;
		int bDisplayFeedback = nArgsLen > 3 ? ArrayGetInt(args, 3) : TRUE;

		AssignCommand(oActionSubject, TakeGoldFromCreature(nAmount, oCreatureToTakeFrom, bDestroy, bDisplayFeedback));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => take " + IntToString(nAmount) + " gp from " + NWBash_ObjectToUserString(oCreatureToTakeFrom) + (bDestroy ? " (destroyed)" : ""));
	}
	else if(sAct == "SurrenderToEnemies"){
		if(bNeedHelp || nArgsLen != 0){
			NWBash_Err("Bad parameters for: SurrenderToEnemies()");
			return 10;
		}

		AssignCommand(oActionSubject, SurrenderToEnemies());
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => surrender");
	}
	else if(sAct == "DoWhirlwindAttack"){
		if(bNeedHelp || nArgsLen > 2){
			NWBash_Err("Bad parameters for: DoWhirlwindAttack(int bDisplayFeedback=TRUE, int bImproved=FALSE)");
			return 10;
		}
		int bDisplayFeedback = nArgsLen > 0 ? ArrayGetInt(args, 0) : FALSE;
		int bImproved = nArgsLen > 1 ? ArrayGetInt(args, 1) : FALSE;

		AssignCommand(oActionSubject, DoWhirlwindAttack(bDisplayFeedback, bImproved));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => whirlwind attack");
	}
	else if(sAct == "PlaySoundByStrRef"){
		if(bNeedHelp || nArgsLen < 1 || nArgsLen > 2){
			NWBash_Err("Bad parameters for: PlaySoundByStrRef(int nStrRef, int nRunAsAction=TRUE)");
			return 10;
		}
		int nStrRef = ArrayGetInt(args, 0);
		int nRunAsAction = nArgsLen > 1 ? ArrayGetInt(args, 1) : TRUE;

		AssignCommand(oActionSubject, PlaySoundByStrRef(nStrRef, nRunAsAction));
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => Play sound strref " + IntToString(nStrRef));
	}
	else if(sAct == "StoreCameraFacing"){
		if(bNeedHelp || nArgsLen != 0){
			NWBash_Err("Bad parameters for: StoreCameraFacing()");
			return 10;
		}

		AssignCommand(oActionSubject, StoreCameraFacing());
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => store camera facing");
	}
	else if(sAct == "RestoreCameraFacing"){
		if(bNeedHelp || nArgsLen != 0){
			NWBash_Err("Bad parameters for: RestoreCameraFacing()");
			return 10;
		}

		AssignCommand(oActionSubject, RestoreCameraFacing());
		NWBash_Info(NWBash_ObjectToUserString(oActionSubject) + " => restore camera facing");
	}
	else{
		NWBash_Err("Unknown action function " + sAct);
		return 4;
	}
	return 0;
}
