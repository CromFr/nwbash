#include "nwbash_inc"


int StringToObjectType(string sObjectType){
	sObjectType = GetStringUpperCase(sObjectType);
	if     (sObjectType == "CREATURE")       return OBJECT_TYPE_CREATURE;
	else if(sObjectType == "ITEM")           return OBJECT_TYPE_ITEM;
	else if(sObjectType == "TRIGGER")        return OBJECT_TYPE_TRIGGER;
	else if(sObjectType == "DOOR")           return OBJECT_TYPE_DOOR;
	else if(sObjectType == "AREA_OF_EFFECT") return OBJECT_TYPE_AREA_OF_EFFECT;
	else if(sObjectType == "WAYPOINT")       return OBJECT_TYPE_WAYPOINT;
	else if(sObjectType == "PLACEABLE")      return OBJECT_TYPE_PLACEABLE;
	else if(sObjectType == "STORE")          return OBJECT_TYPE_STORE;
	else if(sObjectType == "ENCOUNTER")      return OBJECT_TYPE_ENCOUNTER;
	else if(sObjectType == "LIGHT")          return OBJECT_TYPE_LIGHT;
	else if(sObjectType == "PLACED_EFFECT")  return OBJECT_TYPE_PLACED_EFFECT;
	else if(sObjectType == "ALL")            return OBJECT_TYPE_ALL;
	else if(sObjectType == "INVALID")        return OBJECT_TYPE_INVALID;
	return -1;
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: export-object <env_var> <subcommand> [options]\n"
			+ "Stores an object reference into a NWBash environment variable\n"
			+ "\n"
			+ "env_var     Environment variable to store the object into\n"
			+ "subcommand  See below\n"
			+ "\n"
			+ "Options can be:\n"
			+ "-f, --from  Start from this object. If not provided, start from the TARGET object.\n"
			+ "-h, --help  show this help\n"
			+ "\n"
			+ "Subcommands:\n"
			+ "\n"
			+ "target: Select the current target\n"
			+ "\n"
			+ "inventory: Select an item in the inventory\n"
			+ "  -t, --tag=TAG  Item tag\n"
			+ "  -n, --nth=INT  Nth item in the inventory\n"
			+ "\n"
			+ "owner: Select the item owner\n"
			+ "\n"
			+ "area: Select the area\n"
			+ "\n"
			+ "module: Select the current module object\n"
			+ "\n"
			+ "tag: Select the an item by its tag\n"
			+ "  -n, --nth=INT  Nth object matching the tag\n"
			+ "\n"
			+ "near: Select a nearby object\n"
			+ "  --type     Object type. Can be creature, item, trigger, door, area_of_effect, waypoint, placeable, store, encounter, light, placed_effect, all\n"
			+ "  --tag      Object tag\n"
			+ "  -n, --nth  Nth object matching the other flags\n"
		);
		return 0;
	}
	string sFrom = NWBash_GetStringArg(args, "f|from");
	args = NWBash_RemoveStringArgs(args, Array("|f|from|"));
	args = NWBash_RemoveBooleanArgs(args, Array("|h|help|"));


	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen < 2){
		NWBash_Err("Bad number of arguments. See 'export-object --help'");
		return 1;
	}

	object oFrom;
	if(sFrom != "")
		oFrom = NWBash_ResolveObjectString(sFrom);
	else
		oFrom = NWBash_GetTarget();
	if(!GetIsObjectValid(oFrom)){
		NWBash_Err("Invalid 'from' object");
		return 2;
	}

	string sEnvVar = ArrayGetString(args, 0);
	string sSubcommand = ArrayGetString(args, 1);

	object oValue = OBJECT_INVALID;

	if(sSubcommand == "target"){
		oValue = NWBash_GetTarget();
	}
	else if(sSubcommand == "inventory"){
		if(nArgsLen < 3){
			NWBash_Err("Bad number of arguments. See 'export-object --help'");
			return 10;
		}

		string sTag = NWBash_GetStringArg(args, "t|tag");
		string sNth = NWBash_GetStringArg(args, "n|nth");

		if(sTag != "" && sNth != ""){
			NWBash_Err("You must provide either --tag or --nth, not both.");
			return 11;
		}
		if(sTag != ""){
			oValue = GetItemPossessedBy(oFrom, sTag);
		}
		else if(sNth != ""){
			int nNth = StringToInt(sNth);

			int i;
			oValue = GetFirstItemInInventory(oFrom);
			for(i = 0 ; i < nNth ; i++)
				oValue = GetNextItemInInventory(oFrom);
		}
		else{
			NWBash_Err("You must provide either --tag or --nth");
			return 12;
		}

	}
	else if(sSubcommand == "owner"){
		if(GetObjectType(oFrom) != OBJECT_TYPE_ITEM){
			NWBash_Err(GetName(oFrom) + " is not an item");
			return 10;
		}

		oValue = GetItemPossessor(oFrom);
	}
	else if(sSubcommand == "area"){
		oValue = GetArea(oFrom);
	}
	else if(sSubcommand == "module"){
		oValue = GetModule();
	}
	else if(sSubcommand == "tag"){
		int nNth = StringToInt(NWBash_GetStringArg(args, "n|nth"));
		args = NWBash_RemoveStringArgs(args, Array("|n|nth|"));
		string sTag = ArrayGetString(args, 2);

		oValue = GetObjectByTag(sTag, nNth);
	}
	else if(sSubcommand == "near"){
		string sTag = NWBash_GetStringArg(args, "tag");
		int nType = StringToObjectType(NWBash_GetStringArg(args, "type"));
		int nNth = StringToObjectType(NWBash_GetStringArg(args, "n|nth"));

		if(sTag != ""){
			oValue = GetNearestObjectByTag(sTag, oFrom, nNth);
		}
		else{
			oValue = GetNearestObject(nType >= 0 ? nType : OBJECT_TYPE_ALL, oFrom, nNth);
		}
	}
	else{
		NWBash_Err("Unknown subcommand " + sSubcommand);
		return 3;
	}



	if(!GetIsObjectValid(oValue)){
		NWBash_Err("Could not find object to export");
		return 1;
	}
	SetLocalString(OBJECT_SELF, "nwbash_env_" + sEnvVar, "0x" + ObjectToString(oValue));
	NWBash_Info("export " + sEnvVar + "=" + NWBash_ObjectToString(oValue) + " # " + GetName(oValue));
	return 0;
}
