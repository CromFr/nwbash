#include "nwbash_inc"


int StringToFaction(string sFaction){
	sFaction = GetStringLowerCase(sFaction);
	if     (sFaction == "hostile")        return STANDARD_FACTION_HOSTILE;
	else if(sFaction == "commoner")       return STANDARD_FACTION_COMMONER;
	else if(sFaction == "merchant")       return STANDARD_FACTION_MERCHANT;
	else if(sFaction == "defender")       return STANDARD_FACTION_DEFENDER;
	return -1;
}

void ClearReputation(object oCreature, int bCancelActions){
	if(!GetIsObjectValid(oCreature))
		return;
	location lSelf = GetLocation(oCreature);
	object oNear = GetFirstObjectInShape(SHAPE_CUBE, 10000.0, lSelf);
	while(GetIsObjectValid(oNear))
	{
		ClearPersonalReputation(oCreature, oNear);
		ClearPersonalReputation(oNear, oCreature);

		if(bCancelActions){
			AssignCommand(oNear, ClearAllActions());
		}

		oNear = GetNextObjectInShape(SHAPE_CUBE, 10000.0, lSelf);
	}
}

int DetectStandardFaction(object oCreature){
	int nHostileReput = GetStandardFactionReputation(STANDARD_FACTION_HOSTILE, oCreature);
	int nCommonerReput = GetStandardFactionReputation(STANDARD_FACTION_COMMONER, oCreature);
	int nMerchantReput = GetStandardFactionReputation(STANDARD_FACTION_MERCHANT, oCreature);
	int nDefenderReput = GetStandardFactionReputation(STANDARD_FACTION_DEFENDER, oCreature);

	if(nHostileReput == 100 && nCommonerReput == 0 && nMerchantReput == 0 && nDefenderReput == 0)
		return STANDARD_FACTION_HOSTILE;
	if(nHostileReput == 0 && nCommonerReput == 100 && nMerchantReput == 50 && nDefenderReput == 100)
		return STANDARD_FACTION_COMMONER;
	if(nHostileReput == 0 && nCommonerReput == 50 && nMerchantReput == 100 && nDefenderReput == 100)
		return 42;
	// 	return STANDARD_FACTION_MERCHANT;
	// if(nHostileReput == 0 && nCommonerReput == 50 && nMerchantReput == 100 && nDefenderReput == 100)
	// 	return STANDARD_FACTION_DEFENDER;
	return -1;
}

int GetFromMatch(object oCreature, int nFaction, object oFactMember){
	if(nFaction >= 0){
		int nRes = DetectStandardFaction(oCreature);
		if(nFaction == STANDARD_FACTION_MERCHANT || nFaction == STANDARD_FACTION_DEFENDER)
			return nRes == 42;
		return nRes == nFaction;
	}
	return GetFactionEqual(oCreature, oFactMember);
}

void ChangeToFaction(object oTarget, int nFaction, object oFactMember, int bKeepReput, int bClearAct){
	if(nFaction >= 0){
		ChangeToStandardFaction(oTarget, nFaction);
		NWBash_Info("Changed faction of " + NWBash_ObjectToUserString(oTarget) + " to standard faction ID=" + IntToString(nFaction));
	}
	else{
		ChangeFaction(oTarget, oFactMember);
		NWBash_Info("Changed faction of " + NWBash_ObjectToUserString(oTarget) + " to custom faction master " + NWBash_ObjectToUserString(oFactMember));
	}

	AssignCommand(oTarget, ClearAllActions());

	// Trigger combat round end script, to attack if needed
	DelayCommand(0.5, ExecuteScript("nw_c2_default3", oTarget));

	if(!bKeepReput){
		ClearReputation(oTarget, bClearAct);
	}
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: faction [options] <target_faction>\n"
			+ "target_faction: Either hostile, commoner, merchant, defender, or an object specification to join a custom faction\n"
			+ "\n"
			+ "Options can be:\n"
			+ "--keep-reput      Keep the current character reputation (can make the creature sill appear as hostile)\n"
			+ "--clear-act       Clear action of all creatures around (so creatures stop attacking)\n"
			+ "-r, --radius=RAD  Change the faction of all NPC in this radius around the target\n"
			+ "--from=FAC        Only change the faction of creature matching this faction\n"
			+ "                  Note: Merchant and Defender factions are not differentiated, i.e. --from=defender will also change merchant faction members.\n"
			+ "-h, --help        Show this help\n"
		);
		return 0;
	}

	int bKeepReput = NWBash_GetBooleanArg(args, "keep-reput");
	int bClearAct = NWBash_GetBooleanArg(args, "clear-act");
	string sFrom = NWBash_GetStringArg(args, "from");
	string sRadius = NWBash_GetStringArg(args, "r|radius");
	args = NWBash_RemoveBooleanArgs(args, Array("|keep-reput|clear-act|"));
	args = NWBash_RemoveStringArgs(args, Array("|from|"));


	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen != 1){
		NWBash_Err("Bad number of arguments. See 'faction --help'");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 2;
	}


	string sFaction = ArrayGetString(args, 0);
	int nFaction = StringToFaction(sFaction);
	object oFactionMember;
	if(nFaction < 0){
		oFactionMember = NWBash_ResolveObjectString(sFaction);
		if(!GetIsObjectValid(oFactionMember)){
			NWBash_Err("Could not find the faction master '" + sFaction + "'");
			return 3;
		}
	}


	int bFrom = sFrom != "";
	int nFacFrom;
	object oFacFrom;
	if(bFrom){
		nFacFrom = StringToFaction(sFrom);
		if(nFacFrom < 0){
			oFacFrom = NWBash_ResolveObjectString(sFrom);
			if(!GetIsObjectValid(oFactionMember)){
				NWBash_Err("Could not find the from faction master '" + sFrom + "'");
				return 4;
			}
		}
	}


	if(sRadius != ""){
		location lCenter = GetLocation(oTarget);
		float fRadius = StringToFloat(sRadius);

		object oNear = GetFirstObjectInShape(SHAPE_SPHERE, fRadius, lCenter);
		while(GetIsObjectValid(oNear)){
			if(!bFrom || GetFromMatch(oNear, nFacFrom, oFacFrom)){
				ChangeToFaction(oNear, nFaction, oFactionMember, bKeepReput, bClearAct);
			}

			oNear = GetNextObjectInShape(SHAPE_SPHERE, fRadius, lCenter);
		}
	}
	else{

		if(GetIsPC(oTarget)){
			NWBash_Err("Cannot change the faction of a PC");
			return 5;
		}

		if(bFrom && !GetFromMatch(oTarget, nFacFrom, oFacFrom)){
			NWBash_Err("Target is not from faction " + sFrom);
			return 3;
		}

		ChangeToFaction(oTarget, nFaction, oFactionMember, bKeepReput, bClearAct);
	}




	return 0;
}

/* Testing script:
!create c_zombie --export=1
!sleep 0.5
!TARGET=$1 faction --from hostile commoner
!sleep 0.5
!TARGET=$1 faction --from commoner merchant
!sleep 0.5
!TARGET=$1 faction --from merchant defender
!sleep 0.5
!TARGET=$1 faction --from defender hostile
!sleep 0.5
*/

