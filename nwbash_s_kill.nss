#include "nwbash_inc"

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);

	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: kill [options]\n"
			+ "Options can be:\n"
			+ "-r, --radius RANGE: Kill everything in RANGE meters around the target\n"
			+ "--players: Kill players too\n"
			+ "--force: Kill immortals and plot creatures\n"
			+ "--help: show this help");
		return 0;
	}
	string sRadius = NWBash_GetStringArg(args, "r|radius");
	int bKillPlayers = NWBash_GetBooleanArg(args, "p|players");
	int bForceKill = NWBash_GetBooleanArg(args, "f|force");
	args = NWBash_RemoveBooleanArgs(args, Array("|help|f|force|p|players|"));
	args = NWBash_RemoveStringArgs(args, Array("|r|radius|"));

	int argsLen = ArrayGetLength(args);
	if(argsLen != 0){
		NWBash_Err("Bad number of arguments / unknown options. See kill --help");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(sRadius != ""){
		float fRadius = StringToFloat(sRadius);
		location lTarget = GetLocation(oTarget);

		object oNear = GetFirstObjectInShape(SHAPE_SPHERE, fRadius, lTarget);
		while(GetIsObjectValid(oNear)){
			if(oNear != OBJECT_SELF && (bKillPlayers || !GetIsPC(oNear))){
				if(bForceKill){
					SetPlotFlag(oNear, FALSE);
					SetImmortal(oNear, FALSE);
				}
				AssignCommand(OBJECT_SELF, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDeath(TRUE, TRUE, TRUE, TRUE), oNear));
			}
			oNear = GetNextObjectInShape(SHAPE_SPHERE, fRadius, lTarget);
		}
	}
	else{
		if(!GetIsObjectValid(oTarget)){
			NWBash_Err("Invalid target");
			return 2;
		}

		if(GetIsPC(oTarget) && !bKillPlayers){
			NWBash_Err("Target is a PC, use --players to kill players");
			return 3;
		}

		if(bForceKill){
			SetPlotFlag(oTarget, FALSE);
			SetImmortal(oTarget, FALSE);
		}
		else if(GetPlotFlag(oTarget) || GetImmortal(oTarget)){
			NWBash_Err("Cannot kill target, plot="+IntToString(GetPlotFlag(oTarget))+", immortal="+IntToString(GetImmortal(oTarget))+". Use --force to bypass");
			return 4;
		}
		ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDeath(TRUE, TRUE, TRUE, TRUE), oTarget);
		NWBash_Info("Killed " + NWBash_ObjectToUserString(oTarget));
	}
	return 0;
}