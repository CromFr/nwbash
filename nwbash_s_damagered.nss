#include "nwbash_inc"


int StringToDRType(string sDRType, int bRaiseBug = TRUE){
	sDRType = GetStringUpperCase(sDRType);
	if     (sDRType == "NONE")       return DR_TYPE_NONE;
	else if(sDRType == "DMGTYPE")    return DR_TYPE_DMGTYPE;
	else if(sDRType == "MAGICBONUS") return DR_TYPE_MAGICBONUS;
	else if(sDRType == "EPIC")       return DR_TYPE_EPIC;
	else if(sDRType == "GMATERIAL")  return DR_TYPE_GMATERIAL;
	else if(sDRType == "ALIGNMENT")  return DR_TYPE_ALIGNMENT;
	else if(sDRType == "NON_RANGED") return DR_TYPE_NON_RANGED;
	return -1;
}

int StartingConditional(string sArgsData, int nChannel){
	struct array args = Array(sArgsData);
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Usage: !damagered amount dr_type dr_subtype [duration]\n"
			+"dr_type: none, dmgtype, magicbonus, epic, gmaterial, alignment, non_ranged\n"
			+"dr_subtype: integer that depends on dr_type\n"
			+"duration: -1.0 for permanent. 60sec by default"
		);
		return 0;
	}

	int argsLen = ArrayGetLength(args);
	if(argsLen != 3 && argsLen != 4){
		NWBash_Err("Bad number of arguments. See 'damagered --help'");
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 2;
	}

	int nAmount = ArrayGetInt(args, 0);
	int nDRType = StringToDRType(ArrayGetString(args, 1), FALSE);
	if(nDRType < 0){
		NWBash_Err("Invalid DR type");
		return 3;
	}
	int nDRSubType = ArrayGetInt(args, 2);
	float fDur = 60.0;
	if(argsLen > 3)
		fDur = ArrayGetFloat(args, 3);

	effect eDR = EffectDamageReduction(nAmount, nDRSubType, 0, nDRType);
	ApplyEffectToObject(fDur >= 0.0 ? DURATION_TYPE_TEMPORARY : DURATION_TYPE_PERMANENT, eDR, oTarget, fDur);
	NWBash_Info("Applied damage resistance to " + NWBash_ObjectToUserString(oTarget));
	return 0;
}




