#include "x0_i0_position"
#include "nwbash_inc_array"
#include "nwbash_custom"


// void DebugMsg(string sMsg){
// 	SendMessageToPC(GetFirstPC(), sMsg);
// }



// Print an error
void NWBash_Err(string sMsg){
	SendMessageToPC(OBJECT_SELF, "<c=red>    " + sMsg);
}
// Print a warning
void NWBash_Warn(string sMsg){
	SendMessageToPC(OBJECT_SELF, "<c=orange> " + sMsg);
}
// Print an informational message
void NWBash_Info(string sMsg){
	SendMessageToPC(OBJECT_SELF, "<c=lime>   " + sMsg);
}

// Converts an hexadecimal number string to int
int NWBash_HexToInt(string sHex){
	if(GetStringLeft(sHex, 2) == "0x")
		sHex = GetSubString(sHex, 2, -1);

	int nValue = 0;

	int nLen = GetStringLength(sHex);
	int i;
	for(i = 0 ; i < nLen ; i++){
		int nChar = CharToASCII(GetSubString(sHex, i, 1));

		if(nChar >= 0x30 && nChar <= 0x39)// 0-9
			nValue = (nValue << 4) + (nChar - 0x30);
		else if(nChar >= 0x41 && nChar <= 0x46)// A-F
			nValue = (nValue << 4) + 0xA + (nChar - 0x41);
		else if(nChar >= 0x61 && nChar <= 0x66)// a-f
			nValue = (nValue << 4) + 0xA + (nChar - 0x61);
		else
			return -1;
	}
	return nValue;
}

// Converts an object to a NWBash object value specification
string NWBash_ObjectToString(object o){
	return "0x" + ObjectToString(o);
}
// Converts a vector to a NWBash vector value specification
string NWBash_VectorToString(vector vVec, int bPrefix = TRUE){
	return (bPrefix ? "v:" : "") + FloatToString(vVec.x, 0, 6) + ";"+FloatToString(vVec.y, 0, 6) + ";"+FloatToString(vVec.y, 0, 6);
}
// Converts a location to a NWBash location value specification
string NWBash_LocationToString(location lLoc, int bUseObjectID = FALSE){
	return "l:" + (bUseObjectID ? NWBash_ObjectToString(GetAreaFromLocation(lLoc)) : ("t:" + GetTag(GetAreaFromLocation(lLoc))))
		+ "@" + NWBash_VectorToString(GetPositionFromLocation(lLoc), FALSE)
		+ "@" + FloatToString(GetFacingFromLocation(lLoc), 0, 6);
}
// Converts a NWBash vector value specification to a vector
vector NWBash_StringToVector(string sVec){
	if(GetStringLeft(sVec, 2) == "v:")
		sVec = GetSubString(sVec, 2, -1);

	int nY = FindSubString(sVec, ";", 0) + 1;
	int nZ = FindSubString(sVec, ";", nY) + 1;

	return Vector(
		StringToFloat(GetSubString(sVec, 0, nY - 1)),
		StringToFloat(GetSubString(sVec, nY, nZ - 1 - nY)),
		StringToFloat(GetSubString(sVec, nZ, -1))
	);
}
// Converts a NWBash location value specification to a location
location NWBash_StringToLocation(string sLoc){
	if(GetStringLeft(sLoc, 2) == "l:")
		sLoc = GetSubString(sLoc, 2, -1);

	int nPos = FindSubString(sLoc, "@", 0) + 1;
	int nRot = FindSubString(sLoc, "@", nPos) + 1;

	return Location(
		GetObjectByTag(GetSubString(sLoc, 0, nPos - 1)),
		NWBash_StringToVector(GetSubString(sLoc, nPos, nRot - 1 - nPos)),
		StringToFloat(GetSubString(sLoc, nRot, -1))
	);
}
// Checks if a string only contains numeric characters
// bAllowFloat: allow one single dot character for floating points
int NWBash_IsNumeric(string s, int bAllowFloat = FALSE){
	int i;
	int nLen = GetStringLength(s);
	for(i = 0 ; i < nLen ; i++)
	{
		switch(CharToASCII(GetSubString(s, i, 1))){
			case 0x30:
			case 0x31:
			case 0x32:
			case 0x33:
			case 0x34:
			case 0x35:
			case 0x36:
			case 0x37:
			case 0x38:
			case 0x39:
				continue;
			case 0x2e: // '.'
				if(bAllowFloat){
					bAllowFloat = FALSE;
					continue;
				}
				// fallthrough
			default:
				return FALSE;
		}
	}
	return nLen > 0;
}
// Converts an object to a user-friendly string (with name and value spec)
string NWBash_ObjectToUserString(object o){
	return GetName(o) + " (0x" + ObjectToString(o) + ")";
}
// Converts a string to a boolean value (accepts TRUE, FALSE, or numeric values)
int NWBash_StringToBool(string sBool){
	sBool = GetStringLowerCase(sBool);
	if(sBool == "true")
		return TRUE;
	else if(sBool == "false")
		return FALSE;
	return StringToInt(sBool);
}


// Converts an object value specification to an object
object NWBash_ResolveObjectString(string sObject){
	string sType = GetStringLeft(sObject, 2);
	if(sType == "0d")
		return StringToObject(GetSubString(sObject, 2, -1));
	else if(sType == "0x")
		return IntToObject(NWBash_HexToInt(GetSubString(sObject, 2, -1)));
	else if(sType == "t:"){
		// Tag
		int nColPos = FindSubString(sObject, ":", 2);
		int nTh = 0;
		if(nColPos >= 0)
			nTh = StringToInt(GetSubString(sObject, 2, -1));
		object o = GetObjectByTag(GetSubString(sObject, 2, nColPos), nTh);
		if(!GetIsObjectValid(o))
			NWBash_Warn("Object not found with tag '" + GetSubString(sObject, 2, nColPos) + "'");
		return o;
	}
	else if(sType == "n:"){
		// Nearby object name
		string sName = GetSubString(sObject, 2, -1);
		int nNameLen = GetStringLength(sName);

		float fTargetDist = 99999.0;
		object o;

		object oArea = GetArea(OBJECT_SELF);
		object oNear = GetFirstObjectInArea(oArea);
		while(GetIsObjectValid(oNear)){
			if(GetStringLowerCase(GetStringLeft(GetName(oNear), nNameLen)) == sName){
				float fDist = GetDistanceBetween(oNear, OBJECT_SELF);
				if(fDist < fTargetDist){
					o = oNear;
					fTargetDist = fDist;
				}
			}
			oNear = GetNextObjectInArea(oArea);
		}
		if(!GetIsObjectValid(o))
			NWBash_Warn("Object not found with name '" + sName + "'");
		return o;
	}
	else if(sType == "p:"){
		// PC name
		string sName = GetStringLowerCase(GetSubString(sObject, 2, -1));
		int nNameLen = GetStringLength(sName);

		object oPC = GetFirstPC();
		while(GetIsObjectValid(oPC)){
			string sPCName = GetStringLowerCase(GetName(oPC));
			if(GetStringLeft(sPCName, nNameLen) == sName){
				return oPC;
			}
			oPC = GetNextPC();
		}
		return OBJECT_INVALID;
	}
	else if(sObject == "__SELECT__")
		return GetPlayerCurrentTarget(OBJECT_SELF);
	else if(sObject == "__MODULE__")
		return GetModule();
	else if(sObject == "__AREA__")
		return GetArea(OBJECT_SELF);
	else if(sObject == "__SELF__")
		return OBJECT_SELF;
	return OBJECT_INVALID;
}

// Converts a vector value specification to a vector
vector NWBash_ResolveVectorString(string sVec){
	string sType = GetStringLeft(sVec, 2);
	if(sType == "v:")
		return NWBash_StringToVector(GetSubString(sVec, 2, -1));

	object o = NWBash_ResolveObjectString(sVec);
	if(GetIsObjectValid(o)){
		if(GetArea(o) != o)
			return GetPosition(NWBash_ResolveObjectString(sVec));
	}

	return Vector();
}

// Converts a location value specification to a location
location NWBash_ResolveLocationString(string sLoc){
	string sType = GetStringLeft(sLoc, 2);
	if(sType == "l:")
		return NWBash_StringToLocation(GetSubString(sLoc, 2, -1));
	else if(sType == "v:")
		return Location(GetArea(OBJECT_SELF), NWBash_StringToVector(GetSubString(sLoc, 2, -1)), 0.0);
	else if(sLoc == "__ENTRY__")
		return GetStartingLocation();

	object o = NWBash_ResolveObjectString(sLoc);
	if(GetIsObjectValid(o)){
		if(GetArea(o) == o){
			// oDest is an area
			int nHeight = GetAreaSize(AREA_HEIGHT, o);
			int nWidth = GetAreaSize(AREA_WIDTH, o);
			vector vCenter = Vector(nHeight * 5.0, nWidth * 5.0);
			location lLoc = Location(o, vCenter, 0.0);
			if(!GetIsLocationValid(lLoc))
				lLoc = CalcSafeLocation(OBJECT_SELF, lLoc, 30.0, FALSE, FALSE);
			if(!GetIsLocationValid(lLoc)){
				object oCreature = GetNearestObjectToLocation(OBJECT_TYPE_CREATURE, lLoc);
				if(GetIsObjectValid(oCreature))
					lLoc = GetLocation(oCreature);
			}
			return lLoc;
		}
		else
			return GetLocation(NWBash_ResolveObjectString(sLoc));
	}

	return Location(OBJECT_INVALID, Vector(), 0.0);
}


// Set a NWBash environment variable
void NWBash_SetEnvVar(string sVarName, string sValue){
	SetLocalString(OBJECT_SELF, "nwbash_env_" + sVarName, sValue);
}


// Returns the NWBash environment variable value
string NWBash_GetEnvVar(string sName, object oOwner = OBJECT_SELF){
	string sVal = GetLocalString(oOwner, "nwbash_tenv_" + sName);
	if(sVal != "")
		return sVal;
	return GetLocalString(oOwner, "nwbash_env_" + sName);
}

// Get the current command line target object
object NWBash_GetTarget(){
	string sEnvTarget = NWBash_GetEnvVar("TARGET");
	if(sEnvTarget != "")
		return NWBash_ResolveObjectString(sEnvTarget);

	object oSelected = GetPlayerCurrentTarget(OBJECT_SELF);
	if(GetIsObjectValid(oSelected))
		return oSelected;
	return OBJECT_SELF;
}


// Returns the value of a string argument
// sFlag: the flag to get. Can contain a | to separate short and long arguments.
// ex sFlag values:
// "flag"   => --flag value --flag=value
// "f|flag" => -f value -fvalue --flag value --flag=value
// "f|"     => -f value -fvalue
string NWBash_GetStringArg(struct array args, string sFlag){
	int nPos;
	int nFlagSplit = FindSubString(sFlag, "|");
	if(nFlagSplit >= 0){
		string sCharFlag = GetStringLeft(sFlag, nFlagSplit);
		// Short option
		// -f value
		nPos = FindSubString(args.data, ArraySeparator + "-" + sCharFlag + ArraySeparator);
		if(nPos >= 0){
			// Value is in next cell
			int nValStart = nPos + nFlagSplit + 3;
			int nValEnd = FindSubString(args.data, ArraySeparator, nValStart);
			return GetSubString(args.data, nValStart, nValEnd - nValStart);
		}
		// -fvalue
		nPos = FindSubString(args.data, ArraySeparator + "-" + sCharFlag);
		if(nPos >= 0){
			int nValStart = nPos + 2 + nFlagSplit;
			int nValEnd = FindSubString(args.data, ArraySeparator, nValStart);
			return GetSubString(args.data, nValStart, nValEnd - nValStart);
		}

		// Remove short option
		sFlag = GetSubString(sFlag, nFlagSplit + 1, -1);
	}

	// Long option
	if(GetStringLength(sFlag) > 0){
		// --flag=value
		nPos = FindSubString(args.data, ArraySeparator + "--" + sFlag + "=");
		if(nPos >= 0){
			// Value is after =
			int nValStart = nPos + 4 + GetStringLength(sFlag);
			int nValEnd = FindSubString(args.data, ArraySeparator, nValStart);
			return GetSubString(args.data, nValStart, nValEnd - nValStart);
		}
		// --flag value
		nPos = FindSubString(args.data, ArraySeparator + "--" + sFlag + ArraySeparator);
		if(nPos >= 0){
			// Value is in next cell
			int nValStart = nPos + GetStringLength(sFlag) + 4;
			int nValEnd = FindSubString(args.data, ArraySeparator, nValStart);
			return GetSubString(args.data, nValStart, nValEnd - nValStart);
		}
	}

	return "";
}

// Returns TRUE if the flag is present
// See NWBash_GetStringArg
int NWBash_GetBooleanArg(struct array args, string sFlag){
	int nFlagSplit = FindSubString(sFlag, "|");
	if(nFlagSplit >= 0){
		// Short option
		int nPos = FindSubString(args.data, ArraySeparator + "-" + GetStringLeft(sFlag, nFlagSplit));
		if(nPos >= 0)
			return TRUE;
		sFlag = GetSubString(sFlag, nFlagSplit + 1, -1);
	}
	if(ArrayGetHasValueString(args, "--" + sFlag))
		return TRUE;

	string sValue = NWBash_GetStringArg(args, sFlag);
	return sValue == "1" || GetStringLowerCase(sValue) == "true";
}

// Remove the given string arguments from the given arguments
struct array NWBash_RemoveStringArgs(struct array args, struct array flags){
	int nPos;
	int nLen = ArrayGetLength(flags);
	int i;
	for(i = 0 ; i < nLen ; i++)
	{
		string sFlag = ArrayGetString(flags, 0);
		int nFlagLen = GetStringLength(sFlag);
		string sDash = nFlagLen == 1 ? "-" : "--";
		int nDashLen = nFlagLen == 1 ? 1 : 2;

		// Remove --flag value or -f value
		nPos = FindSubString(args.data, ArraySeparator + sDash + sFlag + ArraySeparator);
		if(nPos >= 0){
			int nEnd = FindSubString(args.data, ArraySeparator, nPos + 1 + nDashLen + nFlagLen + 1);

			args = Array(GetStringLeft(args.data, nPos) + GetSubString(args.data, nEnd, -1));
		}

		// Remove --flag=value or -fvalue
		nPos = FindSubString(args.data, ArraySeparator + sDash + sFlag + (nFlagLen > 1 ? "=" : ""));
		if(nPos >= 0){
			int nEnd = FindSubString(args.data, ArraySeparator, nPos + 1);

			args = Array(GetStringLeft(args.data, nPos) + GetSubString(args.data, nEnd, -1));
		}

		flags = ArrayPopFront(flags);
	}
	return args;
}

// Remove the given boolean arguments from the given arguments
struct array NWBash_RemoveBooleanArgs(struct array args, struct array flags){
	int nLen = ArrayGetLength(flags);
	int i;
	for(i = 0 ; i < nLen ; i++)
	{
		string sFlag = ArrayGetString(flags, 0);
		int nFlagLen = GetStringLength(sFlag);

		// Remove --flag or -f
		args = ArrayRemoveValueString(args, (GetStringLength(sFlag) == 1 ? "-" : "--") + sFlag);
		flags = ArrayPopFront(flags);
	}
	return args;
}




// ==============================================================================
// Advanced functions
// ==============================================================================




struct NWBash_PreprocessedLine {
	string envArray;
	string cmd;
	string argsArray;
};

struct NWBash_PreprocessedLine NWBash_PreprocessLine(string sLine){
	struct NWBash_PreprocessedLine ret;
	ret.envArray = "|";
	ret.argsArray = "|";

	// DebugMsg("NWBash_PreprocessLine " + sLine);

	// 0: command
	// 1: args
	int nParsePart = 0;

	// Variable replacement
	int nCurrentQuote = 0; // 0: default, 1: double quoted, 2: single quoted
	int bEscape = FALSE;

	int nVarStart = -1;
	int bVarBrackets = FALSE;
	string sVarName;

	int nLen = GetStringLength(sLine);
	int i;
	for(i = 0 ; i < nLen + 1 ; i++)
	{
		string c = GetSubString(sLine, i, 1);
		int nChar = CharToASCII(c);

		// DebugMsg("c=" + c + " char=0x" + IntToHexString(nChar) + " nCurrentQuote=" + IntToString(nCurrentQuote) + " bEscape=" + IntToString(bEscape));

		// Escape sequences
		if(bEscape){
			if(c == "n")
				c = "\n";
			else if(c == "r")
				c = "\r";
			else if(c == "t")
				c = "	";
			nChar = CharToASCII(c);

			bEscape = FALSE;

			switch(nParsePart){
				case 0: ret.cmd += c; break;
				case 1: ret.argsArray += c; break;
			}
			continue;
		}
		else if(nChar == 0x5c){ // Backslash
			if(nCurrentQuote == 0 || nCurrentQuote == 1){
				bEscape = TRUE;
				continue;
			}
		}

		// Env var replacements
		if(nVarStart >= 0){
			if(sVarName == "" && c == "{"){
				bVarBrackets = TRUE;
				continue;
			}
			if(bVarBrackets && c != "}"){
				sVarName += c;
				continue;
			}
			if((nChar >= 0x30 && nChar <= 0x39)
			|| (nChar >= 0x41 && nChar <= 0x5A)
			|| (nChar >= 0x61 && nChar <= 0x7A)
			|| nChar == 0x5F){
				// [0-9a-zA-Z_]
				sVarName += c;
				continue;
			}

			switch(nParsePart){
				case 0: ret.cmd += NWBash_GetEnvVar(sVarName); break;
				case 1: ret.argsArray += NWBash_GetEnvVar(sVarName); break;
			}
			nVarStart = -1;
			sVarName = "";
			if(bVarBrackets){
				bVarBrackets = FALSE;
				continue;
			}
		}


		switch(nChar){
			case 0x09:// Tab
			case 0x20:// Space
			case 0xa0:// nbsp
			case 0x00:// end
				if(nCurrentQuote == 0){
					if(nParsePart == 0){
						int nEqPos = FindSubString(ret.cmd, "=");
						if(nEqPos >= 0){
							ret.envArray += ret.cmd + "|";
							ret.cmd = "";
						}
						else{
							string sAlias = GetLocalString(OBJECT_SELF, "nwbash_alias_" + ret.cmd);
							if(sAlias != ""){
								int nCmdEnd = FindSubString(sAlias, "|", 1);
								ret.cmd = GetSubString(sAlias, 1, nCmdEnd - 1);
								ret.argsArray += GetSubString(sAlias, nCmdEnd + 1, -1);
							}
							nParsePart = 1;
						}
					}
					else if(nParsePart == 1){
						if(GetStringRight(ret.argsArray, 1) != "|")
							ret.argsArray += "|";
					}
					continue;
				}
				break;

			case 0x22:// "
				if(nCurrentQuote == 0){
					nCurrentQuote = 1;
					continue;
				}
				else if(nCurrentQuote == 1){
					nCurrentQuote = 0;
					continue;
				}
				break;
			case 0x27:// '
				if(nCurrentQuote == 0){
					nCurrentQuote = 2;
					continue;
				}
				else if(nCurrentQuote == 2){
					nCurrentQuote = 0;
					continue;
				}
				break;

			case 0x24:// $
				if(nCurrentQuote == 0 || nCurrentQuote == 1){
					nVarStart = i + 1;
					sVarName = "";
					continue;
				}
				break;

		}

		// Append c to currently parsed field
		switch(nParsePart){
			case 0: ret.cmd += c; break;
			case 1: ret.argsArray += c; break;
		}
	}

	if(GetStringRight(ret.argsArray, 1) != "|")
		ret.argsArray += "|";



	return ret;
}

// Remove spaces around the string
string NWBash_TrimString(string s, int bRemoveLineEndings=FALSE)
{
	int nLen = GetStringLength(s);
	int nTrimStart = 0, nTrimEnd = nLen;

	for(nTrimStart = 0 ; nTrimStart < nLen ; nTrimStart++){
		switch(CharToASCII(GetSubString(s, nTrimStart, 1))){
			case 0x09:// Tab
			case 0x20:// Space
			case 0xa0:// nbsp
				continue;
			case 0x0a:// LF
			case 0x0d:// CR
				if(bRemoveLineEndings)
					continue;
		}
		break;
	}
	for(nTrimEnd = nLen ; nTrimEnd > 0 ; nTrimEnd--){
		switch(CharToASCII(GetSubString(s, nTrimEnd - 1, 1))){
			case 0x09:// Tab
			case 0x20:// Space
			case 0xa0:// nbsp
				continue;
			case 0x0a:// LF
			case 0x0d:// CR
				if(bRemoveLineEndings)
					continue;
		}
		break;
	}

	return GetSubString(s, nTrimStart, nTrimEnd - nTrimStart);
}

// Run a single command
int NWBash_RunCmd(string sCmd, struct array args, int nChatChannel=CHAT_MODE_TALK);

// Run a multi-line script
void NWBash_RunScript(string sScript, int nChatChannel=CHAT_MODE_TALK){
	float fDelay = 0.0;

	int nLineEnd = FindSubString(sScript, "\n");
	string sLine = NWBash_TrimString(GetSubString(sScript, 0, nLineEnd), TRUE);

	// Remove comments
	int nCommentStart = FindSubString(sLine, " #");
	if(nCommentStart < 0)
		nCommentStart = GetStringLeft(sLine, 1) == "#" ? 0 : -1;
	if(nCommentStart >= 0)
		sLine = GetStringLeft(sLine, nCommentStart);

	// Ignore empty lines
	if(sLine != ""){
		if(GetSubString(sLine, 0, 1) == NWBASH_CMD_PREFIX_CHAR)
			sLine = GetSubString(sLine, 1, -1);

		NWBash_Info("#> " + sLine);

		struct NWBash_PreprocessedLine line = NWBash_PreprocessLine(sLine);
		struct array args = Array(line.argsArray);
		struct array env = Array(line.envArray);

		if(line.cmd == "sleep"){
			if(ArrayGetLength(args) != 1){
				NWBash_Err("Wrong number of arguments. Usage: sleep (duration)");
				return;
			}
			fDelay = ArrayGetFloat(args, 0);
		}
		else{
			int nEnvLen = ArrayGetLength(env);
			int i;
			for(i = 0 ; i < nEnvLen ; i++){
				string sVar = ArrayGetString(env, i);
				int nEq = FindSubString(sVar, "=");
				SetLocalString(
					OBJECT_SELF,
					"nwbash_tenv_" + GetSubString(sVar, 0, nEq),
					GetSubString(sVar, nEq + 1, -1)
				);
			}

			int nRes = NWBash_RunCmd(line.cmd, args, nChatChannel);

			// int i;
			int nCount = GetVariableCount(OBJECT_SELF);
			for(i = 0 ; i < nCount ; i++){
				string sVarName = GetVariableName(OBJECT_SELF, i);
				if(GetStringLeft(sVarName, 12) == "nwbash_tenv_"){
					DeleteLocalString(OBJECT_SELF, sVarName);
					i--;
				}
			}

			if(nRes != 0){
				NWBash_Warn("!> Command exited with code " + IntToString(nRes));
				return;
			}
		}

	}
	if(nLineEnd >= 0)
		DelayCommand(fDelay, NWBash_RunScript(GetSubString(sScript, nLineEnd + 1, -1), nChatChannel));

}

int _NWBash_CLIExportVar(struct array args){
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Set shell variables\n"
			+"Usage: export VAR=VALUE\n"
		);
		return 0;
	}
	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 1){
		string sVar = ArrayGetString(args, 0);
		int nEqPos = FindSubString(sVar, "=", 0);
		if(nEqPos < 0){
			NWBash_Err("Usage: export VAR_NAME=VAR_VALUE");
			return 1;
		}
		string sVarName = NWBash_TrimString(GetStringLeft(sVar, nEqPos));
		string sVarValue = NWBash_TrimString(GetSubString(sVar, nEqPos + 1, -1));

		NWBash_SetEnvVar(sVarName, sVarValue);
	}
	else{
		NWBash_Err("Export command can only take 1 argument. See 'export --help'.");
		return 2;
	}

	return 0;
}
int _NWBash_CLIPrintEnv(struct array args){
	int nVars = GetVariableCount(OBJECT_SELF);
	int i;
	for(i = 0 ; i < nVars ; i++)
	{
		string sName = GetVariableName(OBJECT_SELF, i);
		if(GetStringLeft(sName, 11) == "nwbash_env_"){
			NWBash_Info(GetSubString(sName, 11, -1) + "=" + GetVariableValueString(OBJECT_SELF, i));
		}
	}
	return 0;
}
int _NWBash_CLIAlias(struct array args){
	int bNeedHelp = NWBash_GetBooleanArg(args, "h|help");
	if(bNeedHelp){
		NWBash_Info(
			"Print or register command aliases\n"
			+"Examples:\n"
			+"alias  Print all registered aliases\n"
			+"alias crush='dmg -t bludgeoning 5000'  Register the 'crush' alias to execute the 'dmg' command with additional arguments\n"
		);
		return 0;
	}

	int nArgsLen = ArrayGetLength(args);
	if(nArgsLen == 0){
		int nVars = GetVariableCount(OBJECT_SELF);
		int i;
		for(i = 0 ; i < nVars ; i++){
			string sName = GetVariableName(OBJECT_SELF, i);
			if(GetStringLeft(sName, 13) == "nwbash_alias_"){
				NWBash_Info(GetSubString(sName, 13, -1) + "=" + GetVariableValueString(OBJECT_SELF, i));
			}
		}
	}
	else if(nArgsLen == 1){
		string sAlias = ArrayGetString(args, 0);
		int nEqPos = FindSubString(sAlias, "=", 0);
		if(nEqPos < 0){
			NWBash_Err("Usage: alias cmd='resulting command'");
			return 1;
		}
		string sAliasName = NWBash_TrimString(GetStringLeft(sAlias, nEqPos));
		string sAliasValue = NWBash_TrimString(GetSubString(sAlias, nEqPos + 1, -1));

		struct NWBash_PreprocessedLine line = NWBash_PreprocessLine(sAliasValue);

		SetLocalString(OBJECT_SELF, "nwbash_alias_" + sAliasName, "|" + line.cmd + line.argsArray);
	}
	else{
		NWBash_Err("Alias command only takes 1 or 0 arguments. See 'alias --help'.");
		return 2;
	}

	return 0;
}

int _NWBash_CLIExec(struct array args){
	int bNeedHelp = NWBash_GetBooleanArg(args, "help");

	int argsLen = ArrayGetLength(args);
	if(argsLen == 0 || bNeedHelp){
		NWBash_Err("Usage: exec <script> [script_args...]\n"
			+ "If script_args is provided, the script will be executed with ExecuteScriptEnhanced().\n"
			+ "script_args type can be set using (s|i|f|o):<value> syntax. Ex: 'i:42', 'o:__SELF__'\n"
			+ "Object script_args follow the rules as the TARGET environment variable.\n"
		);
		return 1;
	}

	object oTarget = NWBash_GetTarget();
	if(!GetIsObjectValid(oTarget)){
		NWBash_Err("Invalid target");
		return 3;
	}

	int nArgsLen = ArrayGetLength(args);
	string sScript = ArrayGetString(args, 0);

	if(nArgsLen > 1){
		ClearScriptParams();
		int i;
		for(i = 1 ; i < nArgsLen ; i++){
			string sArg = ArrayGetString(args, i);
			int nTypeSep = FindSubString(sArg, ":");

			if(nTypeSep == 1){
				string sTypeID = GetStringLeft(sArg, 1);
				string sValue = GetSubString(sArg, 2, -1);
				if(sTypeID == "s")
					AddScriptParameterString(sValue);
				else if(sTypeID == "i")
					AddScriptParameterInt(StringToInt(sValue));
				else if(sTypeID == "f")
					AddScriptParameterFloat(StringToFloat(sValue));
				else if(sTypeID == "o")
					AddScriptParameterObject(NWBash_ResolveObjectString(sValue));
			}
			else
				AddScriptParameterString(sArg);
		}

		int nRes = ExecuteScriptEnhanced(sScript, oTarget);
		NWBash_Info("Executed '" + sScript + "': Returned int " + IntToString(nRes));
	}
	else{
		ExecuteScript(sScript, oTarget);
		NWBash_Info("Executed '" + sScript + "'");
	}


	return 0;
}

int _NWBash_PrintHelp(struct array args){
	string sSub = ArrayGetLength(args) > 0 ? ArrayGetString(args, 0) : "";
	if(sSub == ""){
		NWBash_Info("Bash-like command line for NWN2:\n"
			+"\n"
			+"Type 'help builtin' to get information on built-in commands\n"
			+"Type 'help values' to get information on value specifications (for setting objects and locations)\n"
			+"\n"
			+"Available commands:\n"
			+NWBash_HelpCommandList()
		);
		return 0;
	}
	else if(sSub == "builtin"){
		NWBash_Info("Built-in commands:\n"
			+"- <b>!!</b>       Enter command-line only mode. In this mode, each chat line will be treated as a command, and you don't need to type '!' before each command\n"
			+"- <b>exit</b>     Exit command-line only mode\n"
			+"\n"
			+"- <b>info</b>     Print target information\n"
			+"- <b>export</b>   Set a environment variable\n"
			+"- <b>unexport</b> Reset a environment variable\n"
			+"- <b>env</b>      Print all environment variables\n"
			+"- <b>alias</b>    Setup or print command aliases\n"
			+"- <b>unalias</b>  Remove a command alias\n"
			+"- <b>sleep</b>    Wait X seconds\n"
			+"- <b>exec</b>     Execute a script on the target\n"
			+"\n"
			+"Environment variables:\n"
			+"Environment can be used in command lines in two ways: $VARNAME or ${VARNAME}\n"
			+"Before a command line is processed, each env var that is not in a single quoted string is replaced by its value, even if the env var is not defined.\n"
			+"You can write a dollar sign by typing: \\$ or '$'\n"
			+"\n"
			+"Special environment variables:\n"
			+"- <b>TARGET</b>: Default target for many commands. If undefined, the command target will be the object currently selected (right clicked), or else the DM himself.\n"
		);
		return 0;
	}
	else if(sSub == "values"){
		NWBash_Info(
			"Most commands that takes objects or locations as parameters can be provided with a value specification."
			+"\n"
			+"Object value specification:\n"
			+"- 0xXXX         An object ID as an hexadecimal number\n"
			+"- 0dXXX         An object ID as a decimal number\n"
			+"- t:tag_name:N  The Nth object by tag. Last :N is optional\n"
			+"- n:name        The name of a nearby creature, placeable, or door\n"
			+"- p:name        PC name\n"
			+"- __SELECT__    The object selected (can be invalid)\n"
			+"- __MODULE__    The loaded module object\n"
			+"- __AREA__      The area where the speaker is\n"
			+"- __SELF__      The DM\n"
			+"\n"
			+"Vector value specification:\n"
			+"- v:x;y;z                Position defined by the x/y/z values\n"
			+"- Any object value spec  Position of the pointed object\n"
			+"\n"
			+"Location value specification:\n"
			+"- l:map_tag@x;y;z@facing  Location defined by the area, position and facing\n"
			+"- v:x;y;z                 Position in the DM current area\n"
			+"- __ENTRY__               Module entry location\n"
			+"- Any object value spec   Location of the pointed object\n"
		);
		return 0;
	}
	NWBash_Err("Unknown category " + sSub);
	return 1;
}


int NWBash_RunCmd(string sCmd, struct array args, int nChatChannel=CHAT_MODE_TALK){
	// DebugMsg("NWBash_RunCmd " + sCmd + ", " + args.data + ", " + IntToString(nChatChannel));
	if(sCmd == "help"){
		return _NWBash_PrintHelp(args);
	}
	else if(sCmd == NWBASH_CMD_PREFIX_CHAR)        { SetLocalInt(OBJECT_SELF, "nwbash_cmdmode", TRUE); return 0; }
	else if(sCmd == "exit")     { DeleteLocalInt(OBJECT_SELF, "nwbash_cmdmode"); return 0; }

	else if(sCmd == "export")   return _NWBash_CLIExportVar(args);
	else if(sCmd == "unexport") { DeleteLocalString(OBJECT_SELF, "nwbash_env_" + ArrayGetString(args, 0)); return 0; }
	else if(sCmd == "env")      return _NWBash_CLIPrintEnv(args);

	else if(sCmd == "alias")    return _NWBash_CLIAlias(args);
	else if(sCmd == "unalias")  { DeleteLocalString(OBJECT_SELF, "nwbash_alias_" + ArrayGetString(args, 0)); return 0; }

	// else if(sCmd == "sleep")    return -1;// Implemented in RunScript
	else if(sCmd == "info"){
		object oTrg = NWBash_GetTarget();
		if(!GetIsObjectValid(oTrg)){
			NWBash_Err("Invalid target");
			return 1;
		}

		string sObjType;
		switch(GetObjectType(oTrg)){
			case OBJECT_TYPE_CREATURE:       sObjType = "creature";       break;
			case OBJECT_TYPE_ITEM:           sObjType = "item";           break;
			case OBJECT_TYPE_TRIGGER:        sObjType = "trigger";        break;
			case OBJECT_TYPE_DOOR:           sObjType = "door";           break;
			case OBJECT_TYPE_AREA_OF_EFFECT: sObjType = "area_of_effect"; break;
			case OBJECT_TYPE_WAYPOINT:       sObjType = "waypoint";       break;
			case OBJECT_TYPE_PLACEABLE:      sObjType = "placeable";      break;
			case OBJECT_TYPE_STORE:          sObjType = "store";          break;
			case OBJECT_TYPE_ENCOUNTER:      sObjType = "encounter";      break;
			case OBJECT_TYPE_LIGHT:          sObjType = "light";          break;
			case OBJECT_TYPE_PLACED_EFFECT:  sObjType = "placed_effect";  break;
			default:
				if(GetArea(oTrg) == oTrg)
					sObjType = "placed_effect";
				else
					sObjType = "unknown";
		}

		string sText = "ID: 0x" + ObjectToString(oTrg) + " / 0d" + IntToString(ObjectToInt(oTrg)) + "\n";
		if(GetIsObjectValid(oTrg)){
			sText += "Name: " + GetName(oTrg) + "\n"
			        +"Tag: " + GetTag(oTrg) + "\n"
			        +"Resref: " + GetResRef(oTrg) + "\n"
			        +"ObjType: " + sObjType + "\n"
			        +"Area: " + NWBash_ObjectToUserString(GetArea(oTrg)) + "\n"
			        +"Position: " + NWBash_VectorToString(GetPosition(oTrg)) + "\n"
			        +"Location-obj: " + NWBash_LocationToString(GetLocation(oTrg), TRUE) + "\n"
			        +"Location-tag: " + NWBash_LocationToString(GetLocation(oTrg), FALSE) + "\n";

			if(GetObjectType(oTrg) == OBJECT_TYPE_CREATURE){
				sText += "IsPC: " + IntToString(GetIsPC(oTrg)) + "\n"
					   + "IsDM: " + IntToString(GetIsDM(oTrg) || GetIsDMPossessed(oTrg)) + "\n";
				if(GetIsPC(oTrg)){
					sText += "Account: " + GetPCPlayerName(oTrg) + "\n"
						   + "CDKey: " + GetPCPublicCDKey(oTrg) + "\n";
				}
			}
		}
		else{
			sText += "Invalid object\n";
		}
		NWBash_Info(sText);
		return 0;
	}

	else if(sCmd == "exec")     return _NWBash_CLIExec(args);
	else{
		ClearScriptParams();
		AddScriptParameterString(args.data);
		AddScriptParameterInt(nChatChannel);
		int nRes = ExecuteScriptEnhanced("nwbash_s_" + sCmd, OBJECT_SELF, TRUE);
		if(nRes == -1)
			NWBash_Err("Unknown command '"+sCmd+"'");
		return nRes;
	}

	return -1;
}

